Cut set,Expected,Observed
0,139.6 $\pm$ 11.3,Blinded
1,40.5 $\pm$ 5.1,Blinded
2,96.0 $\pm$ 17.8,Blinded
3,294.4 $\pm$ 49.8,Blinded
4,31.1 $\pm$ 7.7,Blinded
5,65.5 $\pm$ 1.2,Blinded
6,22.3 $\pm$ 2.5,Blinded
7,99.5 $\pm$ 10.5,Blinded
8,49.7 $\pm$ 6.9,Blinded
9,28.6 $\pm$ 3.2,Blinded
10,31.1 $\pm$ 6.1,Blinded
