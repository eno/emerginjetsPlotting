from rootpy import ROOT as rt
from rootpy.io import root_open
from rootpy.plotting import Canvas, Graph, Hist1D, Hist2D, Pad
from rootpy.plotting.utils import draw
from collections import OrderedDict
from rootutils import saveAllCanvases, updateAllCanvases, deleteAllCanvases
from histoutils import printBinContents, addOverflowUnderflow
from ratioplot import ratioPlot
import time
from math import sqrt
from ctypes import *

import tdrstyle
#set the tdr style
tdrstyle.setTDRStyle()
rt.gStyle.SetPalette(1)

def defaultCanvas(cname=""):
    # canvas = rt.TCanvas(cname, cname, W_ref, H_ref)
    canvas = Canvas(name=cname, title=cname, width=800, height=600)
    canvas.SetTopMargin(0.08)
    canvas.SetLeftMargin(0.16)
    canvas.SetRightMargin(0.08)
    canvas.cd()
    return canvas

def splitCanvas(canvas_in, top_pad_size=0.7):
    """Split input canvas into two pads, top and bottom"""
    canvas_in.cd()
    leftmargin  = canvas_in.GetLeftMargin()
    rightmargin = canvas_in.GetRightMargin()
    toppad = Pad(0.0, (1.0-top_pad_size), 1.0, 1.0, color=10, bordersize=5, bordermode=0, name=cname+'_num', title='' )
    toppad.SetLeftMargin(leftmargin)
    toppad.SetRightMargin(rightmargin)
    toppad.SetCanvas(canvas_in)
    toppad.SetNumber(1)
    botpad = Pad(0.0, 0.0, 1.0, (1.0-top_pad_size), color=10, bordersize=5, bordermode=0, name=cname+'_den', title='' )
    botpad.SetLeftMargin(leftmargin)
    botpad.SetRightMargin(rightmargin)
    botpad.SetCanvas(canvas_in)
    botpad.SetNumber(2)
    botpad.SetBottomMargin(0.2)
    return (toppad, botpad)

def createProfile(hist_in, func, name="", title=""):
    """Create 1D histogram val vs x from input 2D histogram, where val is computed from an x-slice of the input histogram.
    func must be a function that takes 1D histogram and returns (val, valError)
    """
    if not hasattr(func, '__call__'): raise TypeError('createProfile','func must be a function that takes 1D histogram and returns (val, valError)')
    hist = Hist1D([x for x in hist_in.xedges()])
    if name  : hist.SetName  ( name  )
    if title : hist.SetTitle ( title )
    nBinsX = hist.GetNbinsX()
    # for i in xrange(-1, nBinsX+1+1):
    for i in xrange(0, nBinsX+1+1):
        hi = hist_in.ProjectionY("_py_"+str(i), i, i, "")
        (val, valError) = func(hi)
        hist.SetBinContent ( i, val      )
        hist.SetBinError   ( i, valError )
    return hist

def getMeanMeanError(hist_in):
    if hist_in.Integral():
        mean      = hist_in.GetMean()
        meanError = hist_in.GetMeanError()
    else:
        mean      = 0
        meanError = 0
    return (mean, meanError)

def getMeanMeanError2(hist_in):
    if hist_in.Integral():
        mean      = hist_in.GetMean()
        meanError = sqrt( mean*(1-mean)/hist_in.Integral() )
    else:
        mean      = 0
        meanError = 0
    return (mean, meanError)

def getMeanZeroError(hist_in):
    if hist_in.Integral():
        mean      = hist_in.GetMean()
        meanError = 0.0001
    else:
        mean      = 0
        meanError = 0
    return (mean, meanError)

def histToGraph(hist_in, name="", title=""):
    """Convert 1D histogram to graph. Ignores underflow/overflow bins"""
    nBins = hist_in.GetNbinsX()
    graph = Graph( nBins )
    for i in xrange( nBins ):
        graph.SetPoint      ( i+1, hist_in.GetBinCenter(i+1), hist_in.GetBinContent(i+1) )
        graph.SetPointError ( i+1, 0, 0, hist_in.GetBinError(i+1) , hist_in.GetBinError(i+1) )
    return graph

def draw_cms_header(canvas=None):
    t = canvas.GetTopMargin()
    l = canvas.GetLeftMargin()
    r = canvas.GetRightMargin()
    if canvas: canvas.cd()
    latex = rt.TLatex()
    latex.SetTextAlign(11) # Left bottom aligned
    latex.DrawLatexNDC(l, (1-t)+0.2*t, '#scale[1.2]{#font[62]{CMS}} #font[52]{#scale[1.0]{Preliminary}}')
    latex.SetTextAlign(31) # Right bottom aligned
    latex.DrawLatexNDC(1-r, (1-t)+0.2*t, '#font[42]{16.1 fb^{-1} (13 TeV)}')

def draw_cms_header(canvas=None):
    t = canvas.GetTopMargin()
    l = canvas.GetLeftMargin()
    r = canvas.GetRightMargin()
    if canvas: canvas.cd()
    latex = rt.TLatex()
    latex.SetTextAlign(11) # Left bottom aligned
    latex.DrawLatexNDC(l, (1-t)+0.2*t, '#scale[1.2]{#font[62]{CMS}} #font[52]{#scale[1.0]{Preliminary}}')
    latex.SetTextAlign(31) # Right bottom aligned
    latex.DrawLatexNDC(1-r, (1-t)+0.2*t, '#font[42]{16.1 fb^{-1} (13 TeV)}')

unitNormalize = 1

import argparse
parser = argparse.ArgumentParser()
# parser.add_argument('-f', '--force', action='store_true')
# parser.add_argument('sampleset', type=int)
args = parser.parse_args()
# sampleset = args.sampleset


samples = ['QCD', 'Clone']

# Open files
file_prefix = '/mnt/hepcms/home/yhshin/data/condor_output/2018-05-23/histos-clone-v0-'
files = OrderedDict()
files['QCD'  ]  = root_open(file_prefix+'/histo-QCD_HT1500to2000.root', 'read')
files['Clone']  = root_open(file_prefix+'/histo-QCD_HT1500to2000clone.root', 'read')

# Descriptive label
label = OrderedDict()
label['QCD'  ] = 'Official sample'
label['Clone'] = 'Private sample'

# Color histograms
colors = OrderedDict()
colors['QCD'  ] = 'black'
colors['Clone'] = 'blue'

# Create dummy decorator objects for each sample
# All objects will copy decorators from this object
decos = OrderedDict()
s = 'QCD'   ; h = rt.Hist(1,0,1, title=label[s]) ; h.color = colors[s] ; h.markerstyle = 21 ; h.markersize = 0.7 ; h.linesize = 5 ; h.drawstyle = 'e' ; h.legendstyle = 'pl' ; decos[s] = h ;
s = 'Clone' ; h = rt.Hist(1,0,1, title=label[s]) ; h.color = colors[s] ; h.markerstyle = 25 ; h.markersize = 0.7 ; h.linesize = 5 ; h.drawstyle = 'e' ; h.legendstyle = 'pl' ; decos[s] = h ;

# Make legend that can be copied
x1_l = 0.92
y1_l = 0.60
dx_l = 0.30
dy_l = 0.18
x0_l = x1_l-dx_l
y0_l = y1_l-dy_l
defaultCanvas()
legend =  rt.Legend(decos.values(), topmargin=0.02, leftmargin=0.10, rightmargin=0.45, textfont=42)
legend.SetBorderSize(0)

# Get histogram objects from file
histonames = [
    "jet_a3dsigM",
    "jet_theta2D",
    "jet_medianIP",
    "jet_medianIP2",
]
histos = OrderedDict()
for s in samples:
    histo_list = list(files[s].objects())
    histo_dict={}
    for h in histo_list:
        if h.InheritsFrom("TH1") and h.GetName() in histonames:
            h.SetLineWidth(2)
            h.GetXaxis().SetTitleOffset(1.0)
            # h.GetYaxis().SetTitleOffset(1.0)
            h.GetXaxis().SetNoExponent()
            h.GetYaxis().SetTitleSize(20)
            h.GetYaxis().SetTitleFont(43)
            h.GetYaxis().SetTitleOffset(1.55)
            if unitNormalize: h.Scale(1./h.Integral())
            histo_dict[h.GetName()] = h
            # histo_dict[h.GetName()] = addOverflowUnderflow(h)
    histos[s] = histo_dict

# Rebin some histograms
switch_rebin = 0
if switch_rebin:
    # mbins = [(41,100)]
    mbins = [(i*4+1, i*4+4) for i in range(25)]
    # mbins = [(i*4+1+1, i*4+4+1) for i in range(25)]
    # mbins = [ (17, 20), (57, 60), (61, 100)]
    for s in samples:
        for hname in histonames:
        # for hname in ['overlap_jet_a3dsigM', 'overlap_jet_a3dsigM_no']:
            print 'Merge %s' % (hname)
            h = histos[s][hname].merge_bins(mbins, axis=0)
            h.decorate(**histos[s][hname].decorators)
            histos[s][hname] = h
            # histos[s][hname] = addOverflowUnderflow(h)

############################################################
# Plots
############################################################
canvases = OrderedDict()
toppads = OrderedDict()
botpads = OrderedDict()
toppad_axes = OrderedDict()
botpad_axes = OrderedDict()
legends = OrderedDict()
ratios = OrderedDict()
stuff = OrderedDict()

toppadsize=0.7

if 0:
    hdict = OrderedDict()
    cname = 'jet_medianIP'
    canvases[cname] = defaultCanvas(cname)
    hdict['QCD'   ] = histos['QCD'   ]['jet_medianIP' ]
    hdict['Clone' ] = histos['Clone' ]['jet_medianIP' ]
    hdict['QCD'   ] .decorate(decos['QCD'   ])
    hdict['Clone' ] .decorate(decos['Clone' ])
    stuff[cname] = ratioPlot(hdict['Clone'], hdict['QCD'], canvases[cname])
    (ratios[cname], toppads[cname], botpads[cname], toppad_axes[cname], botpad_axes[cname]) = stuff[cname]
    toppad_axes[cname][0][1].SetTitle("Median IP")
    botpad_axes[cname][0][1].SetTitle("Ratio")
    # toppad_axes[cname][0][1].SetRangeUser(1.2e-3, 1.2e3)
    ratios[cname].color = 'black'
    # Clone and draw legend
    legends[cname] = legend.Clone()
    # toppads[cname].SetLogy()
    toppads[cname].SetLogx()
    botpads[cname].SetLogx()
    toppads[cname].cd()
    legends[cname].Draw()
    draw_cms_header(toppads[cname])
    # updateAllCanvases()
    # canvases[cname].Print('%s.png' % (cname))

if 1:
    hdict = OrderedDict()
    cname = 'jet_medianIP2'
    canvases[cname] = defaultCanvas(cname)
    hdict['QCD'   ] = histos['QCD'   ]['jet_medianIP2' ]
    hdict['Clone' ] = histos['Clone' ]['jet_medianIP2' ]
    hdict['QCD'   ] .decorate(decos['QCD'   ])
    hdict['Clone' ] .decorate(decos['Clone' ])
    stuff[cname] = ratioPlot(hdict['Clone'], hdict['QCD'], canvases[cname])
    (ratios[cname], toppads[cname], botpads[cname], toppad_axes[cname], botpad_axes[cname]) = stuff[cname]
    toppad_axes[cname][0][1].SetTitle("# of jets")
    botpad_axes[cname][0][0].SetTitle("Median IP [cm]")
    botpad_axes[cname][0][1].SetTitle("Ratio")
    botpad_axes[cname][0][0].SetTitleOffset(3.0)
    # toppad_axes[cname][0][1].SetRangeUser(1.2e-3, 1.2e3)
    ratios[cname].color = 'black'
    # Clone and draw legend
    legends[cname] = legend.Clone()
    # toppads[cname].SetLogy()
    toppads[cname].cd()
    legends[cname].Draw()
    draw_cms_header(toppads[cname])
    # updateAllCanvases()
    # canvases[cname].Print('%s.png' % (cname))

if 1:
    hdict = OrderedDict()
    cname = 'jet_a3dsigM'
    canvases[cname] = defaultCanvas(cname)
    hdict['QCD'   ] = histos['QCD'   ]['jet_a3dsigM' ]
    hdict['Clone' ] = histos['Clone' ]['jet_a3dsigM' ]
    hdict['QCD'   ] .decorate(decos['QCD'   ])
    hdict['Clone' ] .decorate(decos['Clone' ])
    stuff[cname] = ratioPlot(hdict['Clone'], hdict['QCD'], canvases[cname])
    (ratios[cname], toppads[cname], botpads[cname], toppad_axes[cname], botpad_axes[cname]) = stuff[cname]
    toppad_axes[cname][0][1].SetTitle("# of jets")
    botpad_axes[cname][0][0].SetTitle("#alpha_{3D}")
    botpad_axes[cname][0][1].SetTitle("Ratio")
    botpad_axes[cname][0][0].SetTitleOffset(3.0)
    # toppad_axes[cname][0][1].SetRangeUser(1.2e-3, 1.2e3)
    ratios[cname].color = 'black'
    # Clone and draw legend
    legends[cname] = legend.Clone()
    # toppads[cname].SetLogy()
    toppads[cname].cd()
    legends[cname].Draw()
    draw_cms_header(toppads[cname])
    # updateAllCanvases()
    # canvases[cname].Print('%s.png' % (cname))


###################################
# Must save plots manually
###################################
