from rootpy import ROOT as rt
from rootpy.io import root_open
from rootpy.plotting import Canvas, Hist1D, Hist2D
from rootpy.plotting.utils import draw
from collections import OrderedDict
from rootutils import saveAllCanvases, updateAllCanvases, deleteAllCanvases
from histoutils import printBinContentsWithLabels
import time

import CMS_lumi, tdrstyle
#set the tdr style
tdrstyle.setTDRStyle()
rt.gStyle.SetPalette(1)

#change the CMS_lumi variables (see CMS_lumi.py)
CMS_lumi.lumi_7TeV = "4.8 fb^{-1}"
CMS_lumi.lumi_8TeV = "18.3 fb^{-1}"
CMS_lumi.writeExtraText = 1
CMS_lumi.extraText = "Preliminary"
CMS_lumi.lumi_sqrtS = "13 TeV" # used with iPeriod = 0, e.g. for simulation-only plots (default is an empty string)

intlumi = 20.

# iPos = 11 # "CMS Preliminary" inside border
iPos = 0 # "CMS Preliminary" above border
if( iPos==0 ): CMS_lumi.relPosX = 0.08

H_ref =  600;
W_ref =  800;
# H_ref =  800;
# W_ref = 1200;
W = W_ref
H  = H_ref

#
# Simple example of macro: plot with CMS name and lumi text
#  (this script does not pretend to work in all configurations)
# iPeriod = 1*(0/1 7 TeV) + 2*(0/1 8 TeV)  + 4*(0/1 13 TeV)
# For instance:
#               iPeriod = 3 means: 7 TeV + 8 TeV
#               iPeriod = 7 means: 7 TeV + 8 TeV + 13 TeV
#               iPeriod = 0 means: free form (uses lumi_sqrtS)
# Initiated by: Gautier Hamel de Monchenault (Saclay)
# Translated in Python by: Joshua Hardenbrook (Princeton)
# Updated by:   Dinko Ferencek (Rutgers)
#

iPeriod = 0

# references for T, B, L, R
T = 0.08*H_ref
B = 0.12*H_ref
L = 0.12*W_ref
R = 0.04*W_ref


def defaultCanvas(cname=""):
    # canvas = rt.TCanvas(cname, cname, W_ref, H_ref)
    canvas = Canvas(name=cname, title=cname, width=W_ref, height=H_ref)
    canvas.SetLeftMargin(0.16)
    canvas.SetRightMargin(0.08)
    canvas.cd()
    return canvas

def createProfile(hist_in, func, name="", title=""):
    """Create 1D histogram val vs x from input 2D histogram, where val is computed from an x-slice of the input histogram.
    func must be a function that takes 1D histogram and returns (val, valError)
    """
    if not hasattr(func, '__call__'): raise TypeError('createProfile','func must be a function that takes 1D histogram and returns (val, valError)')
    hist = Hist1D(hist_in.bounds())
    if name  : hist.SetName  ( name  )
    if title : hist.SetTitle ( title )
    nBinsX = hist.GetNbinsX()
    for i in xrange(-1, nBinsX+1+1):
        hi = hist_in.ProjectionY("_py_"+str(i), i, i, "e")
        (val, valError) = func(hi)
        hist.SetBinContent ( i, val      )
        hist.SetBinError   ( i, valError )
    return hist


samples = ['QCD', 'tau_pi_d_1', 'tau_pi_d_5', 'tau_pi_d_25', 'tau_pi_d_60', 'tau_pi_d_100', 'tau_pi_d_150']
# samples = ['QCD', 'tau_pi_d_1', 'tau_pi_d_5', 'tau_pi_d_25']
samples = ['QCD', 'ModelA', 'ModelB']
# samples = ['QCD', 'ModelA', 'ModelB', 'QCD_HT500to700', 'QCD_HT700to1000', 'QCD_HT1000to1500', 'QCD_HT1500to2000', 'QCD_HT2000toInf', ]
# samples = ['QCD_HT500to700', 'QCD_HT700to1000', 'QCD_HT1000to1500', 'QCD_HT1500to2000', 'QCD_HT2000toInf', ]

# Open files
file_prefix = '/mnt/hepcms'
files = OrderedDict()
files['QCD']          = root_open(file_prefix+'/home/yhshin/data/condor_output/2017-03-13/histos-v0/histo-QCD.root', 'read')
files['tau_pi_d_1']   = root_open(file_prefix+'/home/yhshin/data/condor_output/2017-03-13/histos-v0/histo-mass_pi_d_2_tau_pi_d_1.root', 'read')
files['tau_pi_d_5']   = root_open(file_prefix+'/home/yhshin/data/condor_output/2017-03-13/histos-v0/histo-mass_pi_d_2_tau_pi_d_5.root', 'read')
files['tau_pi_d_25']  = root_open(file_prefix+'/home/yhshin/data/condor_output/2017-03-13/histos-v0/histo-mass_pi_d_2_tau_pi_d_25.root', 'read')
files['tau_pi_d_60']  = root_open(file_prefix+'/home/yhshin/data/condor_output/2017-03-13/histos-v0/histo-mass_pi_d_2_tau_pi_d_60.root', 'read')
files['tau_pi_d_100'] = root_open(file_prefix+'/home/yhshin/data/condor_output/2017-03-13/histos-v0/histo-mass_pi_d_2_tau_pi_d_100.root', 'read')
files['tau_pi_d_150'] = root_open(file_prefix+'/home/yhshin/data/condor_output/2017-03-13/histos-v0/histo-mass_pi_d_2_tau_pi_d_150.root', 'read')
# files['ModelA'] = root_open(file_prefix+'/home/yhshin/data/condor_output/2017-02-15/histos-v1/histo-ModelA.root', 'read')
# files['ModelB'] = root_open(file_prefix+'/home/yhshin/data/condor_output/2017-02-15/histos-v1/histo-ModelB.root', 'read')
files['QCD']              = root_open(file_prefix+'/home/yhshin/data/condor_output/2017-03-20/histos-v1/histo-QCD.root', 'read')
files['ModelA']           = root_open(file_prefix+'/home/yhshin/data/condor_output/2017-03-20/histos-v1/histo-mass_pi_d_5_tau_pi_d_150.root', 'read')
files['ModelB']           = root_open(file_prefix+'/home/yhshin/data/condor_output/2017-03-20/histos-v1/histo-mass_pi_d_2_tau_pi_d_5.root', 'read')
# files['QCD_HT500to700']   = root_open(file_prefix+'/home/yhshin/data/condor_output/2017-03-15/histos-test6/histo-QCD_HT500to700.root', 'read')
# files['QCD_HT700to1000']  = root_open(file_prefix+'/home/yhshin/data/condor_output/2017-03-15/histos-test6/histo-QCD_HT700to1000.root', 'read')
# files['QCD_HT1000to1500'] = root_open(file_prefix+'/home/yhshin/data/condor_output/2017-03-15/histos-test6/histo-QCD_HT1000to1500.root', 'read')
# files['QCD_HT1500to2000'] = root_open(file_prefix+'/home/yhshin/data/condor_output/2017-03-15/histos-test6/histo-QCD_HT1500to2000.root', 'read')
# files['QCD_HT2000toInf']  = root_open(file_prefix+'/home/yhshin/data/condor_output/2017-03-15/histos-test6/histo-QCD_HT2000toInf.root', 'read')


# Descriptive label
label = OrderedDict()
label['QCD']    = 'QCD MC'
label['tau_pi_d_1']   = 'tau_pi_d_1'
label['tau_pi_d_5']   = 'tau_pi_d_5'
label['tau_pi_d_25']  = 'tau_pi_d_25'
label['tau_pi_d_60']  = 'tau_pi_d_60'
label['tau_pi_d_100'] = 'tau_pi_d_100'
label['tau_pi_d_150'] = 'tau_pi_d_150'

# Color histograms
colors = OrderedDict()
colors['QCD']      = 'black'
colors['ModelA']   = 'red'
colors['ModelB']   = 'blue'
colors['tau_pi_d_1']   = (255 , 0   , 0   )
colors['tau_pi_d_5']   = (120 , 120 , 0   )
colors['tau_pi_d_25']  = (0   , 255 , 0   )
colors['tau_pi_d_60']  = (0   , 120 , 120 )
colors['tau_pi_d_100'] = (0   , 0   , 255 )
colors['tau_pi_d_150'] = (120 , 0   , 120 )
colors['QCD_HT500to700']   = 'black'
colors['QCD_HT700to1000']  = 'black'
colors['QCD_HT1000to1500'] = 'black'
colors['QCD_HT1500to2000'] = 'black'
colors['QCD_HT2000toInf']  = 'black'
for s in samples:
    for o in list(files[s].objects()):
        o.color = colors[s]
        # print o.GetName()

# Create dummy decorator objects for each sample
# All objects will copy decorators from this object
decos = OrderedDict()
s = 'QCD'          ; h = rt.Hist(1,0,1, title=label[s]) ; h.color = colors[s] ; h.legendstyle = 'l' ; decos[s] = h ;
s = 'tau_pi_d_1'   ; h = rt.Hist(1,0,1, title=label[s]) ; h.color = colors[s] ; h.legendstyle = 'l' ; decos[s] = h ;
s = 'tau_pi_d_5'   ; h = rt.Hist(1,0,1, title=label[s]) ; h.color = colors[s] ; h.legendstyle = 'l' ; decos[s] = h ;
s = 'tau_pi_d_25'  ; h = rt.Hist(1,0,1, title=label[s]) ; h.color = colors[s] ; h.legendstyle = 'l' ; decos[s] = h ;
s = 'tau_pi_d_60'  ; h = rt.Hist(1,0,1, title=label[s]) ; h.color = colors[s] ; h.legendstyle = 'l' ; decos[s] = h ;
s = 'tau_pi_d_100' ; h = rt.Hist(1,0,1, title=label[s]) ; h.color = colors[s] ; h.legendstyle = 'l' ; decos[s] = h ;
s = 'tau_pi_d_150' ; h = rt.Hist(1,0,1, title=label[s]) ; h.color = colors[s] ; h.legendstyle = 'l' ; decos[s] = h ;
# s = 'ModelA' ; h = rt.Hist(1,0,1, title=label[s]) ; h.color = colors[s] ; h.legendstyle = 'l' ; decos[s] = h ;
# s = 'ModelB' ; h = rt.Hist(1,0,1, title=label[s]) ; h.color = colors[s] ; h.legendstyle = 'l' ; decos[s] = h ;

# Make legend that can be copied
x1_l = 0.92
y1_l = 0.60
dx_l = 0.30
dy_l = 0.18
x0_l = x1_l-dx_l
y0_l = y1_l-dy_l
defaultCanvas()
legend =  rt.Legend(decos.values())
legend.SetBorderSize(0)

histos = OrderedDict()
# for s in samples:
#     histo_list = list(files[s].objects())
#     histo_dict={}
#     for h in histo_list:
#         h.color = colors[s]
#         h.GetXaxis().SetTitleOffset(1.0)
#         h.GetYaxis().SetTitleOffset(1.0)
#         h.GetXaxis().SetNoExponent()
#         histo_dict[h.GetName()] = h
#         # histo_dict[hist.GetName()] = addOverflowUnderflow(hist)
#     histos[s] = histo_dict

# Rebin some histograms
switch_rebin_jetpt = 1
if switch_rebin_jetpt:
    # mbins = [(41,100)]
    mbins = [(31,35),(36,40),(41,60),(61,80),(81,100)]
    for s in samples:
        if s == 'WJetData' or s == 'WJetMC':
            for hname in ['jet_pt__JTbasic', 'jet_pt__JTemerging', 'jet_pt__JTipcut']:
                h = histos[s][hname].merge_bins(mbins)
                h.decorate(**histos[s][hname].decorators)
                histos[s][hname] = h

############################################################
# Plots
############################################################
canvases = OrderedDict()
if 0:
    hdict = OrderedDict()
    for i, s in enumerate(samples):
        cname = 'medianIP'
        canvases[cname] = defaultCanvas(cname)
        print i, s
        h = histos[s]['jet_medianIP__JTbasic__EVTkinematic']
        # h.GetXaxis().SetRangeUser(0., 1.)
        # h.SetXTitle('#alpha_{max}')
        # h.GetYaxis().SetRangeUser(0., 100.)
        h.SetYTitle('median(IP^{2D}) [cm]')
        hdict[s] = h.Clone(cname+"_"+s)
        if unitNormalize: hdict[s].Scale(1./hdict[s].Integral())
        # hdict[s].Draw("colz")
    draw(hdict.values(), logx=1)
    CMS_lumi.CMS_lumi(canvases[cname], iPeriod, 0)
    legend.Clone().Draw()
if 0:
    cname = 'fakerate_alphaMaxCut_vs_pt'
    canvases[cname] = defaultCanvas(cname)
    fakerate_alphaMax = OrderedDict()
    for s in samples:
        h = histos[s]['jet_pt__JTemerging']
        h.Divide( histos[s]['jet_pt__JTbasic'] )
        fakerate_alphaMax[s] = h
    draw(fakerate_alphaMax.values(), xtitle='Jet p_{T} [GeV]', ytitle='Fake rate (#alpha_{max} < 0.04)', ylimits=[-0.005, 0.02])
    CMS_lumi.CMS_lumi(canvases[cname], iPeriod, 0)
    legend.Clone().Draw()

if 0:
    cname = 'fakerate_maxIpCut_vs_pt'
    canvases[cname] = defaultCanvas(cname)
    fakerate_maxIpCut = OrderedDict()
    for s in samples:
        h = histos[s]['jet_pt__JTipcut']
        h.Divide( histos[s]['jet_pt__JTbasic'] )
        fakerate_maxIpCut[s] = h
    draw(fakerate_maxIpCut.values(), xtitle='Jet p_{T} [GeV]', ytitle='Fake rate (#alpha_{max} < 0.04, IP^{2D}_{max} > 0.4 cm)', ylimits=[-0.005, 0.02])
    CMS_lumi.CMS_lumi(canvases[cname], iPeriod, 0)
    legend.Clone().Draw()

if 0:
    cname = 'alphaMax_vs_ht'
    canvases[cname] = defaultCanvas(cname)
    alphaMax_vs_ht = OrderedDict()
    for i, s in enumerate(samples):
        print i, s
        h = histos[s]['jet_alphaMax_VS_ht'].ProfileX("_pfx", 2)
        h.GetXaxis().SetRangeUser(0., 2000.)
        h.SetXTitle('H_{T} [GeV]')
        h.GetYaxis().SetRangeUser(0., 1.)
        h.SetYTitle('#alpha_{max}')
        alphaMax_vs_ht[s] = h.Clone(cname+"_"+s)
        if i == 0 : alphaMax_vs_ht[s].Draw()
        else      : alphaMax_vs_ht[s].Draw('SAME')
    CMS_lumi.CMS_lumi(canvases[cname], iPeriod, 0)
    legend.Clone().Draw()

if 0:
    cname = 'alphaMax_vs_nVtx'
    canvases[cname] = defaultCanvas(cname)
    alphaMax_vs_nVtx = OrderedDict()
    for i, s in enumerate(samples):
        print i, s
        h = histos[s]['jet_alphaMax_VS_nVtx'].ProfileX("_pfx", 2)
        h.GetXaxis().SetRangeUser(0., 2000.)
        h.SetXTitle('N_{vertex}')
        h.GetYaxis().SetRangeUser(0., 1.)
        h.SetYTitle('#alpha_{max}')
        alphaMax_vs_nVtx[s] = h.Clone(cname+"_"+s)
        if i == 0 : alphaMax_vs_nVtx[s].Draw()
        else      : alphaMax_vs_nVtx[s].Draw('SAME')
    CMS_lumi.CMS_lumi(canvases[cname], iPeriod, 0)
    legend.Clone().Draw()

if 0:
    cname = 'maxIP_vs_alphaMax'
    canvases[cname] = defaultCanvas(cname)
    hdict = OrderedDict()
    for i, s in enumerate(samples):
        print i, s
        h = histos[s]['jet_maxIP_VS_jet_alphaMax__JTbasic__EVTkinematic'].ProfileX("_pfx", 2)
        h.GetXaxis().SetRangeUser(0., 1.)
        h.SetXTitle('#alpha_{max}')
        h.GetYaxis().SetRangeUser(0., 10.)
        h.SetYTitle('max(IP^{2D}) [cm]')
        hdict[s] = h.Clone(cname+"_"+s)
        if i == 0 : hdict[s].Draw()
        else      : hdict[s].Draw('SAME')
    CMS_lumi.CMS_lumi(canvases[cname], iPeriod, 0)
    legend.Clone().Draw()

if 0:
    hdict = OrderedDict()
    for i, s in enumerate(samples):
        cname = 'maxIP_vs_alphaMax' + '_' + s
        canvases[cname] = defaultCanvas(cname)
        print i, s
        h = histos[s]['jet_maxIP_VS_jet_alphaMax__JTbasic__EVTkinematic']
        h.GetXaxis().SetRangeUser(0., 1.)
        h.SetXTitle('#alpha_{max}')
        h.GetYaxis().SetRangeUser(0., 100.)
        h.SetYTitle('max(IP^{2D}) [cm]')
        hdict[s] = h.Clone(cname+"_"+s)
        hdict[s].Draw("colz")
    CMS_lumi.CMS_lumi(canvases[cname], iPeriod, 0)
    legend.Clone().Draw()


for i, s in enumerate(samples):
    print s
    printBinContentsWithLabels(files[s].cutflow2 * intlumi)
    # printBinContents(histos[s]['cutflow'])

# for c in canvases.values():
#     # c.Update()
#     c.cd()
#     c.Paint()
#     c.ExecuteEvent(rt.kButton1Down, 500, 100)
#     c.Modified()
#     c.ForceUpdate()
#     c.ExecuteEvent(rt.kButton1Up, 500, 101)
#     # canvas.ExecuteEventAxis(rt.kButton1Up, 500, 100)

# raw_input("Press Enter to end")




