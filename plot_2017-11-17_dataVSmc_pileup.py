from rootpy import ROOT as rt
from rootpy.io import root_open
from rootpy.plotting import Canvas, Graph, Hist1D, Hist2D, Pad, Legend
from rootpy.plotting.utils import draw
import colorsys
from collections import OrderedDict
import tdrstyle
#set the tdr style
tdrstyle.setTDRStyle()
rt.gStyle.SetPalette(1)
from ratioplot import ratioPlot

def buildColorPalette(number):
    """Build color palette by dividing the color spectrum into the required number of colors"""
    colors = []
    for i in xrange(number):
        rgb_tuple = colorsys.hls_to_rgb(i/float(number), 0.5, 1.0)
        scaled_rgb_tuple = tuple([256*x for x in rgb_tuple])
        colors.append(scaled_rgb_tuple)
    return colors

def defaultCanvas(cname=""):
    # canvas = rt.TCanvas(cname, cname, W_ref, H_ref)
    canvas = Canvas(name=cname, title=cname, width=800, height=800)
    canvas.SetTopMargin(0.08)
    canvas.SetLeftMargin(0.16)
    canvas.SetRightMargin(0.08)
    canvas.cd()
    return canvas

f = OrderedDict()
hdict = OrderedDict()
canvases = OrderedDict()
ratios = OrderedDict()
toppads = OrderedDict()
botpads = OrderedDict()

f['QCD'] = root_open('/mnt/hepcms/home/yhshin/data/condor_output/2017-11-17/histos-pileuptest/histo-QCD.root')
hdict['QCD'] = f['QCD'].nTrueInt_puweighted.clone()
hdict['QCD'].title = 'QCD'
hdict['QCD'].linecolor = 'blue'
# hdict['QCD'].linewidth = 3
hdict['QCD'].drawstyle = "l hist"
hdict['QCD'].legendstyle = "l"
hdict['QCD'].Scale(1./hdict['QCD'].Integral())


f['Data'] = root_open('/mnt/hepcms/data/users/yhshin/datajsonfiles_received/Analysis-20171103-v0/pileup_Analysis-20171103-v0_20171109_GH.root')
hdict['Data'] = f['Data'].pileup.clone()
hdict['Data'].title = 'Data'
hdict['Data'].color = 'black'
# hdict['Data'].linewidth = 3
# hdict['Data'].drawstyle = "pe"
hdict['Data'].legendstyle = "lpe"
hdict['Data'].Scale(1./hdict['Data'].Integral())

cname = "c1"
canvases[cname] = defaultCanvas(cname)
(ratios[cname], toppads[cname], botpads[cname]) = ratioPlot(hdict['Data'], hdict['QCD'], canvases[cname])
ratios[cname].markerstyle = 1
toppads[cname].cd()
legend =  rt.Legend(hdict.values(), leftmargin=0.50, topmargin=0.05, rightmargin=0.05, entryheight=0.06, textfont=42, textsize=0.06)
legend.SetBorderSize(0)
legend.Draw()


