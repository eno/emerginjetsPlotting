from rootpy import ROOT as rt
from rootpy.io import root_open
from rootpy.plotting import Canvas, Graph, Hist1D, Hist2D, Pad
from rootpy.plotting.utils import draw

from collections import OrderedDict

samples = ['BF', 'GH', ]
file_prefix = '/mnt/hepcms/data/users/yhshin/datajsonfiles_received/Analysis-20171103-v0/'
files = OrderedDict()
files['BF']         = root_open(file_prefix+'/pileup_Analysis-20171103-v0_20171109_BF.root', 'read')
files['GH']         = root_open(file_prefix+'/pileup_Analysis-20171103-v0_20171109_GH.root', 'read')

def defaultCanvas(cname=""):
    # canvas = rt.TCanvas(cname, cname, W_ref, H_ref)
    canvas = Canvas(name=cname, title=cname, width=800, height=800)
    canvas.SetTopMargin(0.08)
    canvas.SetLeftMargin(0.16)
    canvas.SetRightMargin(0.08)
    canvas.cd()
    return canvas

can = defaultCanvas("c1")
files['BF'].pileup.SetLineColor(rt.kBlack)
files['GH'].pileup.SetLineColor(rt.kBlue)
files['BF'].pileup.SetTitle('B-F')
files['GH'].pileup.SetTitle('G-H')
histolist = [
    files['BF'].pileup,
    files['GH'].pileup,
]
draw(histolist)
# legend =  rt.Legend(histolist, topmargin=0.6, rightmargin=0.45, textfont=42)
# legend.Draw()

