"""Plot track modeling systematic"""
import argparse
parser = argparse.ArgumentParser()
# parser.add_argument('-f', '--force', action='store_true')
# parser.add_argument('sampleset', type=int)
args = parser.parse_args()
# sampleset = args.sampleset

from rootpy import ROOT as rt
from rootpy.io import root_open

import os, os.path

import csv

from collections import OrderedDict

from emjet_helpers import sample_name_to_parameters # Returns (mass_X_d, mass_pi_d, tau_pi_d)
from emjet_helpers import signal_parameter_hist
from process_signal_model import get_pdf_uncertainty
from process_signal_model import get_trigger_uncertainty
from process_signal_model import get_modeling_uncertainty
from process_signal_model import get_jec_uncertainty
from process_signal_model import get_pileup_uncertainty
from process_signal_model import get_mcstat_uncertainty
from process_signal_model import get_cutflow

# Dictionary of cross sections for each mediator mass
cross_sections_fb = {
    400   : 5506.11   ,
    600   : 523.797   ,
    800   : 85.0014   ,
    1000  : 18.45402  ,
    1250  : 3.47490   ,
    1500  : 0.768744  ,
    2000  : 0.0487065 ,
}

# mainpath = '/mnt/hepcms/home/yhshin/data/condor_output/2017-11-04/histos-test/'
# mainpath = '/mnt/hepcms/home/yhshin/data/condor_output/2017-11-07/histos-pdftest0/'
# mainpath = '/mnt/hepcms/home/yhshin/data/condor_output/2017-11-21/histos-cuttest1/'
# mainpath = '/mnt/hepcms/home/yhshin/data/condor_output/2017-11-27/histos-acc1/'
# mainpath = '/mnt/hepcms/home/yhshin/data/condor_output/2017-12-06/histos-acc0/'
# mainpath = '/mnt/hepcms/home/yhshin/data/condor_output/2017-12-11/histos-acc0/'
# mainpath = '/mnt/hepcms/data/users/fengyb/condor_output/2017-12-11/histos-acc0/'
# mainpath = '/mnt/hepcms/home/yhshin/data/condor_output/2017-12-21/histos-acc0-ht/'
# mainpath = '/mnt/hepcms/home/yhshin/data/condor_output/2018-01-15/histos-acc0/'
# mainpath_temp = '/mnt/hepcms/home/yhshin/data/condor_output/2018-02-05/histos-acc2-%s/'
# mainpath_temp = '/mnt/hepcms/home/yhshin/data/condor_output/2018-02-13/histos-acc0-%s/'
# mainpath_temp = '/mnt/hepcms/home/yhshin/data/condor_output/2018-02-14/histos-acc1-%s/'
# mainpath_temp = '/mnt/hepcms/home/yhshin/data/condor_output/2018-02-15/histos-acc0-%s/'
# mainpath_temp = '/mnt/hepcms/home/yhshin/data/condor_output/2018-02-27/histos-acc0-%s/' # Add MET JEC-shift
# mainpath_temp = '/mnt/hepcms/home/yhshin/data/condor_output/2018-03-20/histos-acc0-%s/' # Add MET JEC-shift
# mainpath_temp = '/mnt/hepcms/home/yhshin/data/condor_output/2018-03-22/histos-acc0-%s/' # Add ipXY and Z smearing
# mainpath_temp = '/mnt/hepcms/home/yhshin/data/condor_output/2018-03-23/histos-acc1-%s/' # Fix pileup sys. histograms
# mainpath_temp = '/mnt/hepcms/home/yhshin/data/condor_output/2018-04-23/histos-acc1-%s/' # Add nEm == nEm_min to cutflow
# mainpath_temp = '/mnt/hepcms/home/yhshin/data/condor_output/2018-04-30/histos-acc0-%s/' # Back to nEm >= N
mainpath_temp = '/mnt/hepcms/home/yhshin/data/condor_output/2018-05-04/histos-acc0-%s/' # Add MC toy smearing for track systematic
cutnames = [
    'cut1',
    'cut2',
    'cut3',
    'cut4',
    'cut5',
    'cut6',
    'cut7',
    'cut8',
]

# # cutfilepath = '/mnt/hepcms/home/yhshin/workspace/EmJetHistoMaker/cuts/modelset_combined_1121.txt'
# # cutfilepath = '/mnt/hepcms/home/yhshin/workspace/EmJetHistoMaker/cuts/modelset_combined_1122.txt'
# cutfilepath = '/mnt/hepcms/home/yhshin/workspace/EmJetHistoMaker/cuts/modelset_0105_combined.txt'
# with open(cutfilepath) as csvfile:
#     r = csv.reader(csvfile, skipinitialspace=True)
#     cutdict = {row[0]: row[1] for row in r}

samples_to_run = [
    'mass_X_d_1500_mass_pi_d_10_tau_pi_d_1000',
    'mass_X_d_1500_mass_pi_d_1_tau_pi_d_1000',
    'mass_X_d_1500_mass_pi_d_2_tau_pi_d_1000',
    'mass_X_d_1500_mass_pi_d_5_tau_pi_d_1000',
    'mass_X_d_2000_mass_pi_d_10_tau_pi_d_1000',
    'mass_X_d_2000_mass_pi_d_1_tau_pi_d_1000',
    'mass_X_d_2000_mass_pi_d_2_tau_pi_d_1000',
    'mass_X_d_2000_mass_pi_d_5_tau_pi_d_1000',
]
signallist = []
for cutname in cutnames:
    mainpath = mainpath_temp % (cutname)
    samplelist = [x for x in os.listdir(mainpath) if os.path.isdir(x)]

    for root, dirs, files in os.walk(mainpath, topdown=False):
        if root == mainpath: # Only check one level
            for dirname in dirs: # For each directory in mainpath
                dircontents = os.listdir( os.path.join(root, dirname) )
                if 'mass_X_d' in dirname and len(dircontents): # If directory not empty
                # if 'mass_X_d' in dirname and len(dircontents) and dirname in samples_to_run: # If directory not empty
                    print 'Processing %s with %s' % (dirname, cutname)
                    s = OrderedDict()
                    s['name'] = dirname
                    s['cutname'] = cutname
                    signal_file_path =  os.path.join(root, dirname, dircontents[0])
                    # Get parameters
                    parameters = sample_name_to_parameters(dirname)
                    s['mass_X_d']  = parameters[0]
                    s['mass_pi_d'] = parameters[1]
                    s['tau_pi_d']  = parameters[2]
                    if s['mass_X_d']  != 2000 : continue
                    if s['mass_pi_d'] != 1    : continue
                    if s['tau_pi_d']  != 1    : continue
                    # Get cross sections
                    s['xsec_fb'] = cross_sections_fb[s['mass_X_d']]
                    # Get acceptance and systematics
                    fileh = root_open(signal_file_path, 'read')
                    try:
                        pdf_unc = get_pdf_uncertainty(fileh)
                        s['acceptance' ]             = pdf_unc[0]
                        s['acceptance_shift_PdfUp' ] = pdf_unc[1]
                        s['acceptance_shift_PdfDn' ] = pdf_unc[2]
                        # modeling_unc = get_modeling_uncertainty(fileh)
                        # s['acceptance_shift_ModelingUp'] = modeling_unc[1]
                        trigger_unc = get_trigger_uncertainty(fileh)
                        s['acceptance_shift_TriggerUp'] = trigger_unc[1]
                        jec_unc = get_jec_uncertainty(fileh)
                        s['acceptance_shift_JecUp' ] = jec_unc[1]
                        s['acceptance_shift_JecDn' ] = jec_unc[2]
                        pileup_unc = get_pileup_uncertainty(fileh)
                        s['acceptance_shift_PileupUp' ] = pileup_unc[1]
                        s['acceptance_shift_PileupDn' ] = pileup_unc[2]
                        mcstat_unc = get_mcstat_uncertainty(fileh)
                        s['acceptance_shift_MCStatUp' ] = mcstat_unc[1]
                        s['acceptance_shift_MCStatDn' ] = mcstat_unc[2]
                    except ZeroDivisionError:
                        print "Error: Empty cutflow on %s, %s" % (dirname, cutname)
                        fileh.Close()
                        continue
                    except AttributeError as e:
                        print "Error: AttributeError on %s, %s" % (dirname, cutname)
                        print e
                        fileh.Close()
                        continue
                    # Get cutflow
                    cutflow = get_cutflow(fileh)
                    for cut in cutflow:
                        # print cut[0], cut[1]
                        s['"%s"' % cut[0]] = cut[1]
                    fileh.Close()
                    signallist.append( s )

signallist.sort(key=lambda k: (k['mass_X_d'], k['mass_pi_d'], k['tau_pi_d'],))

fieldnames = {}
n_keys = 0
for s in signallist:
    k = s.keys()
    if len(k) > n_keys:
        n_keys = len(k)
        fieldnames = k

# Write results to CSV file
# output_signals_file = 'emjet_signals_allcuts_20180504.csv'
output_signals_file = 'emjet_signals_picked_20180509.csv'
with open(output_signals_file, 'w') as csvfile:
    writer = csv.DictWriter(csvfile, quotechar='"', fieldnames=fieldnames)
    writer.writeheader()
    for s in signallist:
        writer.writerow(s)

