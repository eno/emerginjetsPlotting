import ROOT as rt
rt.TH1.SetDefaultSumw2()
import math
from collections import OrderedDict
from histoutils import *
from rootutils import getHistos, getHistosFromCanvas,saveAllCanvases, updateAllCanvases, deleteAllCanvases
from tdrstyle import setTDRStyle
setTDRStyle()
from containerutils import deftuple
import time

rt.gROOT.ProcessLine('.x $HOME/rootlogon.C')
rt.gStyle.SetPadTickX(1)
# rt.gStyle.SetPadRightMargin(0.8)
# rt.gStyle.SetPadLeftMargin(0.8)
# rt.gStyle.SetTitleAlign(22)
# rt.gStyle.SetTitleSize(0.10, "xyz")
# rt.gStyle.SetLabelSize(0.08, "xyz")
rt.TGaxis.SetMaxDigits(2)

batch_mode = False
if batch_mode: rt.gROOT.SetBatch()
unitNormalize = 1
logPlots = True
plotEfficiency = False
adjustAxisRange = True
testing = False
draw_gluond = False
do_fakerate = 0

intLumi = 20000 # in pb^-1
# intLumi = 79.56 # in pb^-1
# intLumi = 200 # in fb^-1
xsec_ratio = 18619.777 # Ratio between effective xsec for QCD and signal
scale_signal_to_QCD = False

samples = ['QCD', 'tau_pi_d_1', 'tau_pi_d_5', 'tau_pi_d_25', 'tau_pi_d_60', 'tau_pi_d_100', 'tau_pi_d_150']
samples = ['QCD', 'tau_pi_d_1', 'tau_pi_d_5', 'tau_pi_d_25']

files={}
file_prefix = '/tmp'
files['QCD']          = rt.TFile(file_prefix+'/histo-QCD.root')
files['tau_pi_d_1']   = rt.TFile(file_prefix+'/histo-mass_pi_d_2_tau_pi_d_1.root')
files['tau_pi_d_5']   = rt.TFile(file_prefix+'/histo-mass_pi_d_2_tau_pi_d_5.root')
files['tau_pi_d_25']  = rt.TFile(file_prefix+'/histo-mass_pi_d_2_tau_pi_d_25.root')
# files['QCD_HT1500to2000']    = rt.TFile("2016-02-18/histo-QCD_HT1500to2000.root")
colors={}
colors['QCD']    = rt.kBlack
colors['tau_pi_d_1']   = rt.kRed
colors['tau_pi_d_5']   = rt.kGreen
colors['tau_pi_d_25']  = rt.kBlue
# colors['QCD_HT1500to2000']    = rt.kGreen
legoption={}
legoption['QCD']    = "F"
legoption['ModelA'] = "L"
legoption['ModelB'] = "L"
legoption['Data']    = "P"
legoption['WJetData']    = "P"
legoption['WJetMC']    = "L"
# legoption['QCD_HT1500to2000']    = "F"

legtext={}
legtext['QCD']      = 'QCD'
legtext['ModelA']   = 'Model A '
legtext['ModelB']   = 'Model B '
# legtext['ModelA']   = 'Model A (scaled to QCD)'
# legtext['ModelB']   = 'Model B (scaled to QCD)'
legtext['Data']     = 'Data'
legtext['WJetData'] = 'W+Jet Data'
legtext['WJetMC']   = 'W+Jet MC'

histos={}
# Organize histograms so that they are accessible by histos[sample][name]
for sample in samples:
    histo_list = getHistos( files[sample] )
    histo_dict={}
    for hist in histo_list:
        hist.SetLineColor( colors[sample] )
        hist.SetMarkerColor( colors[sample] )
        hist.GetXaxis().SetTitleOffset(1.0)
        # hist.GetYaxis().SetTitleOffset(0.3)
        hist.GetYaxis().SetTitleOffset(1.0)
        hist.GetXaxis().SetNoExponent()
        histo_dict[hist.GetName()] = hist
        # histo_dict[hist.GetName()] = addOverflowUnderflow(hist)
    histos[sample] = histo_dict
sample = samples[0]

canvases={}
toppads={}
bottompads={}
histos_ratios={}
paves={}
# Set default values for class Var
Var_default = {}
Var_default['solo']           = 0
Var_default['mute']           = 0
Var_default['histoname']      = -1 # Raise error if unspecified
Var_default['xAxisTitle']     = -1 # If unspecified, set to histoname
Var_default['Logy']           = 0
Var_default['draw_sig']       = 0 # Draw histoname+"_sig" as well
Var_default['Logx']           = 0
Var_default['binWidthNorm']   = 0
Var_default['canvas_prefix']  = ''
Var_default['canvas_postfix'] = ''
Var_default['xAxisRange']     = []
Var_default['yAxisTitle']     = -1 # If unspecified, set to histoname
Var_default['yAxisRange']     = []
Var_default['addOFUF']        = 1 # Add overflow underflow
Var = deftuple('Var', ['solo', 'mute', 'histoname', 'xAxisTitle', 'Logy', 'draw_sig', 'Logx', 'binWidthNorm', 'canvas_prefix', 'canvas_postfix', 'xAxisRange', 'yAxisTitle', 'yAxisRange', 'addOFUF'], Var_default)
vars_2d = [
    Var(0, 1, 'track_phi_VS_track_eta_nHits_low',  'track #eta', xAxisRange = [-3, 3], yAxisTitle='track #phi', yAxisRange =[-3.5, 3.5] ),
    Var(0, 1, 'track_phi_VS_track_eta_nHits_high', 'track #eta', xAxisRange = [-3, 3], yAxisTitle='track #phi', yAxisRange =[-3.5, 3.5] ),
    Var(0, 1, 'track_ipXY_VS_track_phi',  xAxisTitle='track #phi', xAxisRange =[-3.5, 3.5], yAxisTitle='track 2D IP', Logy=1),
    Var(0, 1, 'track_ipXY_VS_track_nHits',  xAxisTitle='track nHits', xAxisRange =[-0.5, 25.5], yAxisTitle='track 2D IP', Logy=1),
    Var(0, 1, 'track_ipSig_VS_track_nHits',  xAxisTitle='track nHits', xAxisRange =[-0.5, 25.5], yAxisTitle='track 2D IP significance', Logy=1),
    # Var(1, 1, 'track_ipSig_VS_track_ipXY',  xAxisTitle='track 2D IP', yAxisTitle='track 2D IP significance', Logy=1),
    # Var(1, 1, 'jet_prompt_frac_E_VS_jet_alphaMax',  xAxisTitle='#alpha_{max}', yAxisTitle='prompt frac.'),
    Var(0, 1, 'jet_displaced_frac_VS_jet_alphaMaxNeg',  xAxisTitle='1-#alpha_{max}', yAxisTitle='displaced frac.', yAxisRange=[0, 1]),
]
vars = [
    Var(0, 1, 'cutflow'                   , 'cutflow'                                , 1  , 0      , )               ,
    Var(0, 1, 'cutflow2'                   , 'cutflow'                                , 1  , 0      , )               ,
    Var(0, 1, 'nJet'                      , 'nJet'                                   , 1  , 0      , )               ,
    # Var(1, 1, 'nEmerging'                 , 'nEmerging'                              , 1  , 0      , )               ,
    # Var(1, 1, 'nJetPostCut'               , 'nJetPostCut'                            , 1  , 0      , )               ,
    Var(0, 1, 'nJet_alphaMax_low'         , 'nJet_alphaMax_low'                      , 1  , 0      , )               ,
    Var(0, 1, 'nJet_alphaMax_high'        , 'nJet_alphaMax_high'                     , 1  , 0      , )               ,
    Var(0, 1, 'ht'                        , 'H_{T}'                                  , 0  , 0      , )               ,
    Var(0, 1, 'ht4'                       , 'H_{T} of 4 leading jets'                , 0  , 0      , )               ,
    Var(0, 1, 'vertex_Lxy'                , 'vertex_Lxy'                             , 0  , Logx=1 , binWidthNorm=1) ,
    Var(0, 1, 'vertex_mass'               , 'vertex_mass'                            , 0  , Logx=1 , binWidthNorm=1) ,
    Var(0, 1, 'jet_eta'                   , '#eta'                                   , 0  , 1      , )               ,
    Var(0, 1, 'jet_pt'                    , 'p_{T} [GeV]'                            , 0  , 1      , )               ,
    Var(0, 1, 'jet_pt/JTemerging'         , 'p_{T} [GeV]'                            , 0  , 1      , )               ,
    Var(0, 1, 'jet_pt'                    , 'p_{T}'                                  , 0  , 0      , )               ,
    Var(0, 1, 'jet_pt0'                   , 'p_{T,0}'                                , 0  , 0      , )               ,
    Var(0, 1, 'jet_pt1'                   , 'p_{T,1}'                                , 0  , 0      , )               ,
    Var(0, 1, 'jet_cef'                   , 'Charged EM fraction'                    , 0  , 1      , )               ,
    Var(0, 1, 'jet_maxIP'                 , 'Maximum 2D IP'                          , 0  , 0      , Logx=0, xAxisRange=[1e-5, 1e2] )               ,
    Var(0, 1, 'jet_maxIP'                 , 'Maximum 2D IP'                          , 0  , 0      , Logx=1, xAxisRange=[1e-5, 1e2], canvas_postfix='logx' )               ,
    Var(1, 1, 'jet_medianIP'              , 'median 2D IP'                           , 0  , 0      , Logx=1, xAxisRange=[1e-5, 1e2] )               ,
    Var(0, 1, 'jet_medianIPPostCut'       , 'median 2D IP'                           , 0  , 0      , Logx=1, xAxisRange=[1e-5, 1e2] )               ,
    Var(0, 1, 'jet_medianLogIpSig'        , 'median log(#sigma_{IP})'                , 0  , 0      , )               ,
    # Var(1, 1, 'jet_medLogIpSig'           , 'median log(#sigma_{IP})'                , 1  , 1      , )               ,
    Var(0, 1, 'jet_alphaMax'              , '#alpha_{max}'                           , 1  , 1      , )               ,
    Var(0, 1, 'jet_alphaMax__EVTpvpass'    , '#alpha_{max}'                           , 1  , 0      , )               ,
    Var(0, 1, 'jet_alphaMax_lowntrack'    , '#alpha_{max}'                           , 1  , 0      , )               ,
    Var(0, 1, 'jet_alphaMax_highntrack'   , '#alpha_{max}'                           , 1  , 0      , )               ,
    Var(0, 1, 'jet_prompt_frac_E'         , 'prompt frac.'                           , 0  , 1      , )               ,
    Var(0, 1, 'jet_disp_frac'             , 'displaced frac.'                        , 1  , 1      , xAxisRange=[0, 1],  )               ,
    Var(0, 1, 'jet_displaced_frac_lowntrack', 'displaced frac.'                        , 0  , 0      , xAxisRange=[0, 1],  )               ,
    Var(0, 1, 'jet_displaced_frac_highntrack', 'displaced frac.'                        , 0  , 0      , xAxisRange=[0, 1],  )               ,
    Var(0, 1, 'jet_nDarkPions'            , '# of dark pions within jet cone'        , 1  , 1      , )               ,
    Var(0, 1, 'jet_missInnerHit_frac'     , 'missing hit energy frac.'               , 1  , 1      , xAxisRange=[-.1, 1.1], addOFUF=1  )               ,
    Var(0, 1, 'jet_missInnerHit_frac__EVTpvpass'     , 'missing hit energy frac.'               , 1  , 0      , xAxisRange=[-.1, 1.1], addOFUF=1  )               ,
    Var(0, 1, 'jet_nTrack'                , '# of tracks per jet'                    , 0  , 1      , xAxisRange=[0   , 20]             , )               ,
    Var(0, 1, 'jet_nTrackPostCut'         , '# of tracks per jet'                    , 0  , 1      , xAxisRange=[0   , 20]             , )               ,
    Var(0, 1, 'jet_nTracks_ipSig2_low'    , '# of tracks per jet'                    , 0  , 1      , xAxisRange=[0   , 50]             , )                              ,
    Var(0, 1, 'jet_nTracks_ipSig2_high'   , '# of tracks per jet'                    , 0  , 1      , xAxisRange=[0   , 50]             , )                              ,
    Var(0, 1, 'jet_nTracks_ipSig5_low'    , '# of tracks per jet'                    , 0  , 1      , xAxisRange=[0   , 50]             , )                              ,
    Var(0, 1, 'jet_nTracks_ipSig5_high'   , '# of tracks per jet'                    , 0  , 1      , xAxisRange=[0   , 50]             , )                              ,
    Var(0, 1, 'jet_nTracks_ipSig10_low'   , '# of tracks per jet'                    , 0  , 1      , xAxisRange=[0   , 50]             , )                              ,
    Var(0, 1, 'jet_nTracks_ipSig10_high'  , '# of tracks per jet'                    , 0  , 1      , xAxisRange=[0   , 50]             , )                              ,
    Var(0, 1, 'jet_nTracks_ipSig20_low'   , '# of tracks per jet'                    , 0  , 1      , xAxisRange=[0   , 50]             , )                              ,
    Var(0, 1, 'jet_nTracks_ipSig20_high'  , '# of tracks per jet'                    , 0  , 1      , xAxisRange=[0   , 50]             , )                              ,
    Var(0, 1, 'jet_nVertex'               , 'jet_nVertex'                            , 1  , 1      , xAxisRange=[0.  , 10])            ,
    Var(0, 1, 'vertex_Lxy'                , 'jet_vertex_Lxy '                        , 0  , 1      , Logx=1          , xAxisRange=[0.1 , 100]                           , )               ,
    Var(0, 1, 'vertex_mass'               , 'jet_vertex_mass '                       , 0  , 1      , Logx=1          , xAxisRange=[0.1 , 100]                           , )               ,
    Var(0, 1, 'vertex_Lxy'                , 'jet_vertex_Lxy (bin width normalized)'  , 0  , 1      , Logx=1          , binWidthNorm=1  , canvas_postfix='_binWidthNorm' , xAxisRange=[0.1 , 100] , ) ,
    Var(0, 1, 'vertex_mass'               , 'jet_vertex_mass (bin width normalized)' , 0  , 1      , Logx=1          , binWidthNorm=1  , canvas_postfix='_binWidthNorm' , xAxisRange=[0.1 , 100] , ) ,
    Var(0, 1, 'vertex_chi2'               , 'jet_vertex_chi2'                        , 0  , 0      , )               ,
    Var(0, 1, 'vertex_ndof'               , 'jet_vertex_ndof'                        , 0  , 0      , )               ,
    Var(0, 1, 'vertex_pt2sum'             , 'jet_vertex_pt2sum'                      , 0  , 1      , )               ,
    Var(0, 1, 'nJet'                      , '# of jets'                              , 1  , 0      , )               ,
    Var(0, 1, 'track_pt'                  , 'track pt'                               , 0  , 1      , )               ,
    Var(0, 1, 'track_eta'                 , 'track eta'                              , 0  , 1      , )               ,
    Var(0, 1, 'track_phi'                 , 'track phi'                              , 0  , 1      , )               ,
    Var(0, 1, 'track_quality'             , 'track quality'                          , 0  , 1      , xAxisRange=[0,200] , )               ,
    Var(0, 1, 'track_quality__TKprompt'    , 'track quality'                          , 0  , 1      , xAxisRange=[0,200] , )               ,
    Var(0, 1, 'track_quality__TKdisplaced' , 'track quality'                          , 0  , 1      , xAxisRange=[0,200] , )               ,
    Var(0, 1, 'track_algo'                , 'track algo'                             , 0  , 1      , )               ,
    Var(0, 1, 'track_algo__TKprompt'       , 'track quality'                          , 0  , 1      , )               ,
    Var(0, 1, 'track_algo__TKdisplaced'    , 'track quality'                          , 0  , 1      , )               ,
    Var(0, 1, 'track_originalAlgo'        , 'track originalAlgo'                     , 0  , 1      , )               ,
    Var(0, 1, 'track_nHits'               , 'track nHits'                            , 0  , 1      , xAxisRange=[0, 30] )               ,
    Var(0, 1, 'track_nMissInnerHits'      , 'track nMissInnerHits'                   , 0  , 1      , xAxisRange=[0,10] , )               ,
    Var(0, 1, 'track_nMissInnerHits__TKprompt'         , 'track nMissInnerHits'       , 0  , 1      , xAxisRange=[0,10] , )               ,
    Var(0, 1, 'track_nMissInnerHits__TKdisplaced'      , 'track nMissInnerHits'       , 0  , 1      , xAxisRange=[0,10] , )               ,
    Var(0, 1, 'track_missHitFrac'         , 'track missHitFrac'                      , 0  , 1      , )               ,
    Var(0, 1, 'track_nTrkLayers'          , 'track_nTrkLayers'                       , 0  , 1      , xAxisRange=[0   ,10]              , )                              ,
    Var(0, 1, 'track_nMissInnerTrkLayers' , 'track_nMissInnerTrkLayers'              , 0  , 1      , xAxisRange=[0   ,10]              , )                              ,
    Var(0, 1, 'track_nPxlLayers'          , 'track_nPxlLayers'                       , 0  , 1      , xAxisRange=[0   ,10]              , )                              ,
    Var(0, 1, 'track_nMissInnerPxlLayers' , 'track_nMissInnerPxlLayers'              , 0  , 1      , xAxisRange=[0   ,10]              , )                              ,
    Var(0, 1, 'track_nNetMissInnerLayers' , 'track nNetMissInnerLayers'              , 0  , 1      , xAxisRange=[0   ,10]              , )                              ,
    Var(0, 1, 'track_missLayerFrac'       , 'track missLayerFrac'                    , 0  , 1      , )               ,
    Var(0, 1, 'track_ipXY'                , 'track ipXY'                             , 0  , 1      , Logx=1, xAxisRange=[1e-5, 1e2] )        ,
    Var(0, 1, 'track_ipXYb'               , 'track ipXY'                             , 0  , 0      , xAxisRange=[0, 1] )        ,
    Var(0, 1, 'track_ipSig'               , 'track IP significance'                  , 0  , 1      , Logx=1, xAxisRange=[1e-5, 1e2] )        ,
    Var(0, 1, 'track_logIpSig'            , 'track log(#sigma_{IP})'                 , 0  , 1      , )               ,
    Var(0, 1, 'track_dRToJetAxis'         , 'track dRToJetAxis'                      , 0  , 1      , )               ,
    Var(0, 1, 'track_distanceToJet'       , 'track distanceToJet'                    , 0  , 1      , )               ,
    Var(0, 1, 'jet_track_logIpSig0'       , 'track log(#sigma_{IP})'                 , 0  , 0      , )               ,
    Var(0, 1, 'track_logIpSig'            , 'track log(#sigma_{IP})'                 , 0  , 1      , )               ,
]

# Plots for highpt versions of the histograms
# vars_highpt = []
# for var in vars:
#     if var.histoname[:6] == 'track_':
#         varcopy = var._replace(histoname=var.histoname+'_highpt', draw_sig=0)
#         vars_highpt.append(varcopy)
# vars.extend(vars_highpt)

# Temporary/testing histograms
vars_testing = []
if testing:
    for var in vars:
        if var.histoname == 'track_nHits':
            varcopy = var._replace(histoname=var.histoname+'_regionA', draw_sig=0)
            vars_testing.append(varcopy)
            varcopy = var._replace(histoname=var.histoname+'_regionB', draw_sig=0)
            vars_testing.append(varcopy)
    vars.extend(vars_testing)

# Are there any "solo" variables? If so, only plot those.
solo_variables_exist = False
solo_vars = []
for var in vars:
    if var.solo:
        solo_variables_exist = True
        solo_vars.append(var)
if solo_variables_exist: vars = solo_vars

# Plots for all jets in event
for var in vars:
    solo           = var.solo
    mute           = var.mute
    vname          = var.histoname
    label          = var.xAxisTitle
    logy           = var.Logy
    draw_sig       = var.draw_sig
    binWidthNorm   = var.binWidthNorm
    canvas_prefix  = var.canvas_prefix
    canvas_postfix = var.canvas_postfix
    xAxisRange     = var.xAxisRange
    if mute and not solo: continue

    # Set canvas properties
    cname        = canvas_prefix+vname+canvas_postfix
    canvases[cname] = rt.TCanvas(cname, cname, 1200, 800)
    canvases[cname].SetLeftMargin(0.08)
    canvases[cname].SetRightMargin(0.08)
    canvases[cname].SetLogy(logy)
    canvases[cname].SetLogx(var.Logx)
    canvases[cname].cd()

    for sname in samples:
        scale = 1.
        if binWidthNorm: histos[sname][vname] = binWidthNormalize(histos[sname][vname])
        if var.addOFUF: histos[sname][vname] = addOverflowUnderflow(histos[sname][vname])
        if vname[:12] == 'jet_alphaMax': histos[sname][vname].Rebin(2)
        if vname == 'jet_alphaMax__EVTpvpass': histos[sname][vname].Rebin(5)
        if sname == 'Data':
            scale=1.0
            histos[sname][vname].SetMarkerStyle(20)
            histos[sname][vname].SetMarkerSize(1.0)
            histos[sname][vname].GetXaxis().SetTitle(label)
            if xAxisRange: histos[sname][vname].GetXaxis().SetRangeUser(xAxisRange[0], xAxisRange[1])
            # histos[sname][vname].SetLineWidth(0.01)
            histos[sname][vname].Scale(scale)
            # if unitNormalize : histos[sname][vname].DrawNormalized("p0 e x0")
            # else             : histos[sname][vname].Draw("p0 e x0")
        if sname == 'QCD':
            scale = 1. * intLumi
            histos[sname][vname].Scale(scale)
            histos[sname][vname].SetFillStyle(3003)
            histos[sname][vname].SetFillColor( colors[sname] )
            histos[sname][vname].GetXaxis().SetTitle(label)
            if xAxisRange: histos[sname][vname].GetXaxis().SetRangeUser(xAxisRange[0], xAxisRange[1])
            # if unitNormalize : histos[sname][vname].DrawNormalized("hist same")
            # else             : histos[sname][vname].Draw("hist same")
            # (ratios, toppad, bottompad) = plotRatiosGeneric(histos['QCD'][vname], histos['Data'][vname], canvas=canvases[cname], style="d")
            # toppads[cname]    = toppad
            # bottompads[cname] = bottompad
            # bottompads[cname].cd()
            # histos_ratios[cname] = {sname: ratios[0], }
            # ratios[0].Draw("p0 e x0")

        if sname == 'ModelA' or sname == 'ModelB':
            # scale = 14.6 / 3458. * intLumi # 10 fb / 10k events * integrated lumi
            scale = 1. * intLumi * 18.45/14.6;
            if scale_signal_to_QCD: scale = xsec_ratio * intLumi
            histos[sname][vname].Scale(scale)
            histos[sname][vname].GetXaxis().SetTitle(label)
            if xAxisRange: histos[sname][vname].GetXaxis().SetRangeUser(xAxisRange[0], xAxisRange[1])
            # if unitNormalize : histos[sname][vname].DrawNormalized("hist e same")
            # else             : histos[sname][vname].Draw("hist e same")
            if draw_sig:
                if binWidthNorm: histos[sname][vname+"__sig"] = binWidthNormalize(histos[sname][vname+"__sig"])
                histos[sname][vname+"__sig"].Scale(scale*2.0)
                histos[sname][vname+"__sig"].SetLineStyle(2)
                histos[sname][vname+"__sig"].GetXaxis().SetTitle(label)
                if xAxisRange: histos[sname][vname+"__sig"].GetXaxis().SetRangeUser(xAxisRange[0], xAxisRange[1])
                if vname[:12] == 'jet_alphaMax': histos[sname][vname+"__sig"].Rebin(5)
                if unitNormalize : histos[sname][vname+"__sig"].DrawNormalized("hist e same")
                else             : histos[sname][vname+"__sig"].Draw("hist e same")
                if draw_gluond:
                    if binWidthNorm: histos[sname][vname+"_gluond"] = binWidthNormalize(histos[sname][vname+"_gluond"])
                    histos[sname][vname+"_gluond"].Scale(scale*2.0)
                    histos[sname][vname+"_gluond"].SetLineStyle(3)
                    if xAxisRange: histos[sname][vname+"_gluond"].GetXaxis().SetRangeUser(xAxisRange[0], xAxisRange[1])
                    if vname[:12] == 'jet_alphaMax': histos[sname][vname+"_gluond"].Rebin(5)
                    if unitNormalize : histos[sname][vname+"_gluond"].DrawNormalized("hist e same")
                    else             : histos[sname][vname+"_gluond"].Draw("hist e same")
        if sname == 'WJetData':
            histos[sname][vname].SetMarkerStyle(20)
            histos[sname][vname].SetMarkerSize(1.0)
            histos[sname][vname].GetXaxis().SetTitle(label)
            # histos[sname][vname].SetLineWidth(0.01)
            histos[sname][vname].Scale(scale)
            # if unitNormalize : histos[sname][vname].DrawNormalized("p0 e x0")
            # else             : histos[sname][vname].Draw("p0 e x0")
        if sname == 'WJetMC':
            histos[sname][vname].Scale(scale)
            # if unitNormalize : histos[sname][vname].DrawNormalized("hist e same")
            # else             : histos[sname][vname].Draw("hist e same")
    # toppads[cname].cd()
    canvases[cname].cd()
    # if unitNormalize : histos['Data'][vname].DrawNormalized("p0 e x0")
    # else             : histos['Data'][vname].Draw("p0 e x0")
    if unitNormalize : histos['QCD'][vname].DrawNormalized("hist e same")
    else             : histos['QCD'][vname].Draw("hist e same")
    # if unitNormalize : histos['QCD'][vname].DrawNormalized("hist same")
    # else             : histos['QCD'][vname].Draw("hist same")
    if unitNormalize : histos['ModelA'][vname].DrawNormalized("hist e same")
    else             : histos['ModelA'][vname].Draw("hist e same")
    if draw_sig:
        if unitNormalize : histos['ModelA'][vname+"__sig"].DrawNormalized("hist e same")
        else             : histos['ModelA'][vname+"__sig"].Draw("hist e same")
    if unitNormalize : histos['ModelB'][vname].DrawNormalized("hist e same")
    else             : histos['ModelB'][vname].Draw("hist e same")
    if draw_sig:
        if unitNormalize : histos['ModelB'][vname+"__sig"].DrawNormalized("hist e same")
        else             : histos['ModelB'][vname+"__sig"].Draw("hist e same")

if do_fakerate:
    cname = "fakerate"
    canvases[cname] = rt.TCanvas(cname, cname, 1200, 800)
    canvases[cname].SetLeftMargin(0.16)
    canvases[cname].SetRightMargin(0.08)
    canvases[cname].SetLogy(0)
    canvases[cname].SetLogx(0)
    canvases[cname].cd()
    hist = histos['QCD']['jet_pt/JTemerging'].Clone().Rebin(5)
    denominator = histos['QCD']['jet_pt'].Clone().Rebin(5)
    hist.Divide(denominator)
    hist.GetXaxis().SetTitleOffset(1.5)
    hist.GetXaxis().SetNoExponent()
    hist.GetXaxis().SetRangeUser(0, 1000.)
    hist.GetYaxis().SetTitleOffset(2.0)
    hist.GetYaxis().SetNoExponent()
    hist.GetYaxis().SetRangeUser(0, 0.05)
    hist.GetYaxis().SetTitle('Probability of QCD jet faking Emerging Jet')
    hist.Draw("hist e")


# jet_indices = {}
# jet_indices['WJetMC'] = [ 0, ]
# jet_indices['ModelA'] = [ 0, 1, 2, 3, ]
# jet_indices['ModelB'] = [ 0, 1, 2, 3, ]
# linestyles = [1, 2, 4, 3]
# linewidths = [3, 3, 2, 1]
# # Per-jet plots
# for var in vars:
#     name = var + '_per_jet'
#     canvases[name] = rt.TCanvas(name, name, 1200, 800)
#     canvases[name].SetRightMargin(0.08)
#     canvases[name].cd()
#     for sample in samples:
#         for jet_index in jet_indices[sample]:
#             if sample == 'WJetMC':
#                 histos[sample][var+str(jet_index)].DrawNormalized("p")
#             else:
#                 histos[sample][var+str(jet_index)].SetLineWidth(linewidths[jet_index])
#                 histos[sample][var+str(jet_index)].SetLineStyle(linestyles[jet_index])
#                 histos[sample][var+str(jet_index)].DrawNormalized("hist same")

# Set max/min range for histos
minMargin = 0.8
maxMargin = 1.5
for name, canvas in canvases.iteritems():
    if name == "fakerate": continue
    # histo_list = getHistosFromCanvas(toppads[name])
    histo_list = getHistosFromCanvas(canvases[name])
    if logPlots : (ymax, ymin) = getMaxMinPositive(*histo_list)
    else        : (ymax, ymin) = getMaxMin(*histo_list)
    for hist in histo_list:
        hist.GetYaxis().SetRangeUser(ymin*minMargin, ymax*maxMargin);
# for name, canvas in canvases.iteritems():
#     histo_list = getHistosFromCanvas(bottompads[name])
#     if logPlots : (ymax, ymin) = getMaxMinPositive(*histo_list)
#     else        : (ymax, ymin) = getMaxMin(*histo_list)
#     for hist in histo_list:
#         hist.GetYaxis().SetRangeUser(ymin*minMargin, ymax*maxMargin);

# legend = rt.TLegend(0, 200, 0, 200, "", "br")
legend = rt.TLegend(0.43, 0.7, 0.93, 0.9, "", "NB NDC" )
legend.SetBorderSize(0)
for sample in samples:
    legend.AddEntry(histos[sample][vars[0].histoname], legtext[sample], legoption[sample])
legend_sig = legend.Clone()
legend_sig_postfix = ""
if scale_signal_to_QCD: legend_sig_postfix = " (scaled to QCD #times 2)"
histos['ModelA']["jet_pt__sig"].SetLineStyle(2)
histos['ModelB']["jet_pt__sig"].SetLineStyle(2)
legend_sig.AddEntry(histos['ModelA']["jet_pt__sig"], "ModelA signal jets w/ dark pion"+legend_sig_postfix, legoption['ModelA'])
legend_sig.AddEntry(histos['ModelB']["jet_pt__sig"], "ModelB signal jets w/ dark pion"+legend_sig_postfix, legoption['ModelB'])
if draw_gluond:
    histos['ModelA']["jet_pt_gluond"].SetLineStyle(3)
    histos['ModelB']["jet_pt_gluond"].SetLineStyle(2)
    legend_sig.AddEntry(histos['ModelA']["jet_pt_gluond"], "ModelA signal jets w/ dark gluon"+legend_sig_postfix, legoption['ModelA'])
    legend_sig.AddEntry(histos['ModelB']["jet_pt_gluond"], "ModelB signal jets w/ dark gluon"+legend_sig_postfix, legoption['ModelB'])

# for name, canvas in canvases.iteritems():
# 	canvas.cd()
# 	legend.Draw()

legends = {}

for var in vars:
    solo           = var.solo
    mute           = var.mute
    vname          = var.histoname
    label          = var.xAxisTitle
    logy           = var.Logy
    draw_sig       = var.draw_sig
    binWidthNorm   = var.binWidthNorm
    canvas_prefix  = var.canvas_prefix
    canvas_postfix = var.canvas_postfix
    xAxisRange     = var.xAxisRange
    if mute and not solo: continue

    # Set canvas properties
    cname        = canvas_prefix+vname+canvas_postfix
    # toppads[cname].cd()
    canvases[cname].cd()
    if draw_sig: legend_sig.Draw()
    else:        legend.Draw()

# Draw 2d histograms
for var in vars_2d:
    solo           = var.solo
    mute           = var.mute
    vname          = var.histoname
    label          = var.xAxisTitle
    logy           = var.Logy
    draw_sig       = var.draw_sig
    binWidthNorm   = var.binWidthNorm
    canvas_prefix  = var.canvas_prefix
    canvas_postfix = var.canvas_postfix
    xAxisRange     = var.xAxisRange
    if mute and not solo: continue
    for sname in samples:
        # if sname == 'ModelA' or sname == 'ModelB': continue
        cname        = var.canvas_prefix + var.histoname + var.canvas_postfix + "_" + sname
        canvases[cname] = rt.TCanvas(cname, cname, 1200, 800)
        canvases[cname].SetLeftMargin(0.08)
        canvases[cname].SetRightMargin(0.16)
        canvases[cname].SetLogy(var.Logy)
        canvases[cname].SetLogx(var.Logx)
        canvases[cname].cd()
        if sname == 'QCD':
            scale = 1. * intLumi
            histos[sname][vname].Scale(scale)
        histos[sname][vname].GetXaxis().SetTitle(var.xAxisTitle)
        histos[sname][vname].GetYaxis().SetTitle(var.yAxisTitle)
        histos[sname][vname].Draw("colz")
        corr = histos[sname][vname].GetCorrelationFactor()
        print "Correlation for %s in sample %s: " % (vname, sname), corr
        if var.xAxisRange: histos[sname][vname].GetXaxis().SetRangeUser(var.xAxisRange[0], var.xAxisRange[1])
        if var.yAxisRange: histos[sname][vname].GetYaxis().SetRangeUser(var.yAxisRange[0], var.yAxisRange[1])
        paves[cname] = rt.TPaveText(.85,.02,.95,.07, "NDC")
        paves[cname].SetFillColor(rt.kWhite)
        paves[cname].AddText(sname)
        paves[cname].Draw()

# graphs = {}
# # vname = "jet_prompt_frac_E"; reverse=False
# # vname = "jet_alphaMax"; reverse=False
# vname = "jet_displaced_frac"; reverse=True
# cname = "roc_" + vname
# # skip
# # canvases[cname] = rt.TCanvas(cname, cname, 1200, 800)
# # canvases[cname].SetLeftMargin(0.08)
# # canvases[cname].SetRightMargin(0.08)
# sname = 'QCD'
# histo_bg = getCumulativeHistogram(histos[sname][vname], normalize=True)
# first = True
# for sname in samples: graphs[sname] = {}
# for sname in samples:
#     continue; # skip
#     if sname == 'QCD': continue
#     histo_sig = getCumulativeHistogram(histos[sname][vname], normalize=True, reverse=reverse)
#     graphs[sname][vname] = makeGraphFromHistos(histo_bg, histo_sig, reverse=reverse)
#     graphs[sname][vname].SetLineColor( colors[sname] )
#     graphs[sname][vname].GetXaxis().SetTitle("#epsilon_{bkg}")
#     graphs[sname][vname].GetYaxis().SetTitle("#epsilon_{sig}")
#     graphs[sname][vname].GetYaxis().SetTitleOffset(0.5)
#     if first:
#         graphs[sname][vname].Draw("AL")
#         first = False
#     else:
#         graphs[sname][vname].Draw("L")
#     histo_sig = getCumulativeHistogram(histos[sname][vname+"_sig"], normalize=True, reverse=reverse)
#     graphs[sname][vname+"_sig"] = makeGraphFromHistos(histo_bg, histo_sig, reverse=reverse)
#     graphs[sname][vname+"_sig"].SetLineStyle(2)
#     graphs[sname][vname+"_sig"].SetLineColor( colors[sname] )
#     graphs[sname][vname+"_sig"].Draw("L")


# # Set max/min range for histos
# minMargin = 0.8
# maxMargin = 1.2
# if logPlots:
# 	minMargin = math.pow(10, -minMargin)
# 	maxMargin = math.pow(10,  maxMargin)
# if adjustAxisRange:
# 	for histoname in histonames:
# 		histo_list = [ hist for (name, ifileName), hist in histos.iteritems() if name == histoname]
# 		if logPlots : (ymax, ymin) = getMaxMinPositive(*histo_list)
# 		else        : (ymax, ymin) = getMaxMin(*histo_list)
# 		# print '(ymax, ymin)'
# 		# print (ymax, ymin)
# 		for hist in histo_list:
# 			hist.GetYaxis().SetRangeUser(ymin*minMargin, ymax*maxMargin);

# for canvas in rt.gROOT.GetListOfCanvases():
# 	canvas.Update()
updateAllCanvases()
if batch_mode: saveAllCanvases(prefix="test_")
rt.gInterpreter.UpdateAllCanvases()

print "QCD"
printBinContentsWithLabels(histos['QCD']['cutflow2'])
print "ModelA"
printBinContentsWithLabels(histos['ModelA']['cutflow2'])
print "ModelB"
printBinContentsWithLabels(histos['ModelB']['cutflow2'])
