"""Plot track modeling systematic"""
import argparse
parser = argparse.ArgumentParser()
# parser.add_argument('-f', '--force', action='store_true')
# parser.add_argument('sampleset', type=int)
args = parser.parse_args()
# sampleset = args.sampleset

from rootpy import ROOT as rt
from rootpy.io import root_open

import os, os.path

import csv

from collections import OrderedDict

from emjet_helpers import sample_name_to_parameters # Returns (mass_X_d, mass_pi_d, tau_pi_d)
from emjet_helpers import signal_parameter_hist
from process_signal_model import get_pdf_uncertainty
from process_signal_model import get_trigger_uncertainty
from process_signal_model import get_modeling_uncertainty
from process_signal_model import get_modeling_toy_uncertainty
from process_signal_model import get_jec_uncertainty
from process_signal_model import get_met_uncertainty
from process_signal_model import get_cutflow
from process_signal_model import get_pileup_uncertainty
from process_signal_model import get_mcstat_uncertainty
from process_signal_model import get_cutflow

doMet = 1

# mainpath = '/mnt/hepcms/home/yhshin/data/condor_output/2017-11-04/histos-test/'
# mainpath = '/mnt/hepcms/home/yhshin/data/condor_output/2017-11-07/histos-pdftest0/'
# mainpath = '/mnt/hepcms/home/yhshin/data/condor_output/2017-11-21/histos-cuttest1/'
# mainpath = '/mnt/hepcms/home/yhshin/data/condor_output/2017-11-27/histos-acc1/'
# mainpath = '/mnt/hepcms/home/yhshin/data/condor_output/2017-12-06/histos-acc0/'
# mainpath = '/mnt/hepcms/home/yhshin/data/condor_output/2017-12-11/histos-acc0/'
# mainpath = '/mnt/hepcms/data/users/fengyb/condor_output/2017-12-11/histos-acc0/'
# mainpath = '/mnt/hepcms/home/yhshin/data/condor_output/2017-12-21/histos-acc0-ht/'
# mainpath = '/mnt/hepcms/home/yhshin/data/condor_output/2018-02-05/histos-acc1/'
# mainpath = '/mnt/hepcms/home/yhshin/data/condor_output/2018-02-12/histos-acc0/'
# mainpath = '/mnt/hepcms/home/yhshin/data/condor_output/2018-04-26/histos-acc0/'
cutfilepath = 'emjet_cutmap_bestcuts_20180607.txt'
mainpath = '/mnt/hepcms/home/yhshin/data/condor_output/2018-06-13/histos-nomet/'
# out_csvfile = 'emjet_signals_nomet_20180613.csv'
out_csvfile_temp = 'emjet_signals_%s_20180617.csv'
tag = 'nomet'
if doMet: mainpath = '/mnt/hepcms/home/yhshin/data/condor_output/2018-06-13/histos-met0/'
if doMet: tag = 'met'
out_csvfile = 'test_met.csv'
# Dictionary of cross sections for each mediator mass
cross_sections_fb = {
    400   : 5506.11   ,
    600   : 523.797   ,
    800   : 85.0014   ,
    1000  : 18.45402  ,
    1250  : 3.47490   ,
    1500  : 0.768744  ,
    2000  : 0.0487065 ,
}

# cutfilepath = '/mnt/hepcms/home/yhshin/workspace/EmJetHistoMaker/cuts/modelset_combined_1121.txt'
# cutfilepath = '/mnt/hepcms/home/yhshin/workspace/EmJetHistoMaker/cuts/modelset_combined_1122.txt'
# cutfilepath = '/mnt/hepcms/home/yhshin/workspace/EmJetHistoMaker/cuts/modelset_0105_combined.txt'
# cutfilepath = 'modelset_best_limits_20180212.txt'
with open(cutfilepath) as csvfile:
    r = csv.reader(csvfile, skipinitialspace=True)
    cutdict = {row[0]: row[1] for row in r}

samplelist = [x for x in os.listdir(mainpath) if os.path.isdir(x)]

sbin = 16 # Signal region bin in cutflow histogram
if doMet: sbin = 17

signallist = []
for root, dirs, files in os.walk(mainpath, topdown=False):
    if root == mainpath: # Only check one level
        for dirname in dirs: # For each directory in mainpath
            dircontents = os.listdir( os.path.join(root, dirname) )
            if 'mass_X_d' in dirname and len(dircontents): # If directory not empty
                print 'Processing %s' % (dirname)
                s = OrderedDict()
                s['name'] = dirname
                if dirname in cutdict.keys():
                    s['cutname'] = cutdict[dirname]
                else:
                    s['cutname'] = 'cut-1'
                signal_file_path =  os.path.join(root, dirname, dircontents[0])
                # Get parameters
                parameters = sample_name_to_parameters(dirname)
                s['mass_X_d']  = parameters[0]
                s['mass_pi_d'] = parameters[1]
                s['tau_pi_d']  = parameters[2]
                # Get cross sections
                s['xsec_fb'] = cross_sections_fb[s['mass_X_d']]
                # Get acceptance and systematics
                fileh = root_open(signal_file_path, 'read')
                try:
                    pdf_unc = get_pdf_uncertainty(fileh, signal_bin=sbin)
                    s['acceptance' ]             = pdf_unc[0]
                    s['acceptance_shift_PdfUp' ] = pdf_unc[1]
                    s['acceptance_shift_PdfDn' ] = pdf_unc[2]
                    modeling_unc = get_modeling_toy_uncertainty(fileh, signal_bin=sbin)
                    s['acceptance_shift_ModelingUp'] = modeling_unc[1]
                    print modeling_unc[1]
                    trigger_unc = get_trigger_uncertainty(fileh, signal_bin=sbin)
                    s['acceptance_shift_TriggerUp'] = trigger_unc[1]
                    jec_unc = get_jec_uncertainty(fileh, signal_bin=sbin)
                    s['acceptance_shift_JecUp' ] = jec_unc[1]
                    s['acceptance_shift_JecDn' ] = jec_unc[2]
                    pileup_unc = get_pileup_uncertainty(fileh, signal_bin=sbin)
                    s['acceptance_shift_PileupUp' ] = pileup_unc[1]
                    s['acceptance_shift_PileupDn' ] = pileup_unc[2]
                    mcstat_unc = get_mcstat_uncertainty(fileh, signal_bin=sbin)
                    s['acceptance_shift_MCStatUp' ] = mcstat_unc[1]
                    s['acceptance_shift_MCStatDn' ] = mcstat_unc[2]
                    if doMet:
                        met_unc = get_met_uncertainty(fileh, signal_bin=sbin)
                        s['acceptance_shift_MetUp' ] = met_unc[1]
                        s['acceptance_shift_MetDn' ] = met_unc[2]
                    else:
                        s['acceptance_shift_MetUp' ] = 0
                        s['acceptance_shift_MetDn' ] = 0
                except ZeroDivisionError:
                    print "Error: Empty cutflow on %s, %s" % (dirname, cutname)
                    fileh.Close()
                    continue

                    print "Error: AttributeError on %s, %s" % (dirname, cutname)
                    print e
                    fileh.Close()
                    continue
                # Get cutflow
                cutflow = get_cutflow(fileh, signal_bin=sbin)
                for cut in cutflow:
                    # print cut[0], cut[1]
                    s['"%s"' % cut[0]] = cut[1]
                fileh.Close()
                signallist.append( s )
            # dircontents = os.listdir( os.path.join(root, dirname) )
            # if 'mass_X_d' in dirname and len(dircontents): # If directory not empty
            #     samplename = dirname
            #     print 'Processing %s' % (samplename)
            #     s = OrderedDict()
            #     s['name'] = samplename
            #     if samplename in cutdict.keys():
            #         s['cutname'] = cutdict[samplename]
            #     else:
            #         s['cutname'] = 'cut-1'
            #     signal_file_path =  os.path.join(root, samplename, dircontents[0])
            #     # Get parameters
            #     parameters = sample_name_to_parameters(samplename)
            #     s['mass_X_d']  = parameters[0]
            #     s['mass_pi_d'] = parameters[1]
            #     s['tau_pi_d']  = parameters[2]
            #     # Get cross sections
            #     s['xsec_fb'] = cross_sections_fb[s['mass_X_d']]
            #     # Get acceptance and systematics
            #     fileh = root_open(signal_file_path, 'read')
            #     pdf_unc = get_pdf_uncertainty(fileh)
            #     s['acceptance' ]             = pdf_unc[0]
            #     s['acceptance_shift_PdfUp' ] = pdf_unc[1]
            #     s['acceptance_shift_PdfDn' ] = pdf_unc[2]
            #     modeling_unc = get_modeling_uncertainty(fileh)
            #     s['acceptance_shift_ModelingUp'] = modeling_unc[1]
            #     trigger_unc = get_trigger_uncertainty(fileh)
            #     s['acceptance_shift_TriggerUp'] = trigger_unc[1]
            #     jec_unc = get_jec_uncertainty(fileh)
            #     s['acceptance_shift_JecUp' ] = jec_unc[1]
            #     s['acceptance_shift_JecDn' ] = jec_unc[2]
            #     mcstat_unc = get_mcstat_uncertainty(fileh)
            #     s['acceptance_shift_MCStatUp' ] = mcstat_unc[1]
            #     s['acceptance_shift_MCStatDn' ] = mcstat_unc[2]
            #     # Get cutflow
            #     cutflow = get_cutflow(fileh)
            #     for cut in cutflow:
            #         # print cut[0], cut[1]
            #         s['"%s"' % cut[0]] = cut[1]
            #     signallist.append( s )

signallist.sort(key=lambda k: (k['mass_X_d'], k['mass_pi_d'], k['tau_pi_d'],))

# Write results to CSV file
writeToFile = 1
if writeToFile:
    with open(out_csvfile, 'w') as csvfile:
        fieldnames = signallist[0].keys()
        writer = csv.DictWriter(csvfile, quotechar='"', fieldnames=fieldnames)
        if not doMet: writer.writeheader()
        for s in signallist:
            writer.writerow(s)

