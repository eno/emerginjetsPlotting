from rootpy import ROOT as rt
from rootpy.io import root_open
signal_region_bin = 15

def calculate_pdf_uncertainty(filename, debug=0):
    """Calculate PDF uncertainty on signal acceptance. Returns fractional positive and negative shift as a tuple: (shift_positive, shift_negative)"""
    fileh = root_open(filename, 'read')
    if debug:
        print fileh.cutflow.GetBinContent(signal_region_bin) , fileh.cutflow.GetBinContent(1)
        print fileh.cutflow__PdfUp.GetBinContent(signal_region_bin) , fileh.cutflow__PdfUp.GetBinContent(1)
        print fileh.cutflow__PdfDn.GetBinContent(signal_region_bin) , fileh.cutflow__PdfDn.GetBinContent(1)

    accept = fileh.cutflow.GetBinContent(signal_region_bin) / fileh.cutflow.GetBinContent(1)
    accept_up = fileh.cutflow__PdfUp.GetBinContent(signal_region_bin) / fileh.cutflow__PdfUp.GetBinContent(1)
    accept_dn = fileh.cutflow__PdfDn.GetBinContent(signal_region_bin) / fileh.cutflow__PdfDn.GetBinContent(1)
    if accept != 0:
        shift_up = accept_up/accept - 1.0
        shift_dn = accept_dn/accept - 1.0
    else:
        shift_up = 0.0
        shift_dn = 0.0
    if debug:
        print 'accept    :' , accept
        print 'accept_up :' , accept_up
        print 'accept_dn :' , accept_dn
    sorted_shift = [shift_up, shift_dn]
    sorted_shift.sort(reverse=True) # Sort from high to low
    return (sorted_shift[0], sorted_shift[1])

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-debug', type=int, default=0)
    parser.add_argument('filename', type=str)
    args = parser.parse_args()
    filename = args.filename
    (shift_up, shift_dn) = calculate_pdf_uncertainty(filename, args.debug)
    print shift_up, shift_dn


