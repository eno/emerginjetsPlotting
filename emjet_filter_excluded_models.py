"""Process a limit file and produce a text file containing signal models that are expected to be excluded"""
import os
import csv
from collections import OrderedDict
from rootpy import ROOT as rt
from rootpy.io import root_open
from rootpy.plotting import Hist, Hist2D, Hist3D, HistStack, Legend, Canvas, Graph, Pad
from rootpy.plotting.utils import draw
import tdrstyle
tdrstyle.setTDRStyle()
rt.gStyle.SetPalette(1)
batch = 1 # SWITCH
if batch:
    # Turn off graphics
    rt.gROOT.SetBatch(True)
blinded = 0 # SWITCH
from rootutils import saveAllCanvases, updateAllCanvases, deleteAllCanvases
from math import sin
import colorsys

limitfile = os.environ['LIMITFILE_BEST']
output_excluded_file = 'emjet_excluded_models.txt'

import emjet_helpers as helper
# helper.outer.parameters['mass_X_d'].remove(1250)
helper.outer.parameters['mass_X_d'].remove(1500)
helper.outer.parameters['mass_X_d'].remove(2000)
helper.outer.parameters['tau_pi_d'].remove(0.001)
helper.outer.parameters['tau_pi_d'].remove(0.1)
# helper.outer.parameters['tau_pi_d'].remove(1)
# helper.outer.parameters['tau_pi_d'].remove(2)
# helper.outer.parameters['tau_pi_d'].remove(500)
helper.update_signal_parameter_dict()

xsec_fb = {
    400   : 5506.11   ,
    600   : 523.797   ,
    800   : 85.0014   ,
    1000  : 18.45402  ,
    1250  : 3.47490   ,
    1500  : 0.768744  ,
    2000  : 0.0487065 ,
}

# Read into list of OrderedDict
with open(limitfile) as csvfile:
    reader = csv.DictReader(csvfile)
    keys = reader.fieldnames
    r = csv.reader(csvfile)
    signal_list = [OrderedDict(zip(keys, row)) for row in r]

########################################################################
# Check if +/- 1 sigma expected limit is below theoretical cross section
########################################################################
# Define list of parameters
mass_X_d_list = list( set([float(s['mass_X_d']) for s in signal_list]) )
mass_X_d_list.sort()
mass_pi_d_list = list( set([float(s['mass_pi_d']) for s in signal_list]) )
mass_pi_d_list.sort()
tau_pi_d_list = list( set([float(s['tau_pi_d']) for s in signal_list]) )
tau_pi_d_list.sort()
# mass_X_d_list = [1000]
# mass_pi_d_list = [1, 10]

if 1:
    signal_list_filtered = []
    for s in signal_list:
        name      = (s['name'])
        xsec      = float(s['xsec_fb'])
        mass_X_d  = float(s['mass_X_d'])
        mass_pi_d = float(s['mass_pi_d'])
        tau_pi_d  = float(s['tau_pi_d'])
        expected  = float(s['sigma0'])
        sigma1up  = float(s['sigma1'])
        sigma1dn  = float(s['sigma-1'])
        observed  = float(s['observed'])
        if sigma1up < xsec or sigma1dn < xsec:
            signal_list_filtered.append(s)
    with open(output_excluded_file, 'w') as ofile:
        for s in signal_list_filtered:
            name      = (s['name'])
            ofile.write(name + '\n')

