import csv
from collections import OrderedDict
from rootpy import ROOT as rt
from rootpy.io import root_open
from rootpy.plotting import Hist, Hist2D, Hist3D, HistStack, Legend, Canvas, Graph, Pad
from rootpy.plotting.utils import draw
import tdrstyle
tdrstyle.setTDRStyle()
rt.gStyle.SetPalette(1)
batch = 0
if batch:
    # Turn off graphics
    rt.gROOT.SetBatch(True)
from rootutils import saveAllCanvases, updateAllCanvases, deleteAllCanvases
from math import sin
import colorsys

def defaultCanvas(cname=""):
    canvas = Canvas(name=cname, title=cname, width=800, height=600)
    canvas.SetTopMargin(0.08)
    canvas.SetLeftMargin(0.12)
    canvas.SetRightMargin(0.08)
    # canvas.SetTopMargin(0.0)
    # canvas.SetLeftMargin(0.0)
    # canvas.SetRightMargin(0.0)
    canvas.SetLogy()
    # canvas.SetGridx()
    # canvas.SetGridy()
    canvas.cd()
    return canvas

signal_list = []
with open('emjet_theoryxsec.txt') as csvfile:
    next(csvfile) # Skip first file
    r = csv.reader(csvfile, delimiter=' ')
    for row in r:
        mass             = float(row[0])
        xsec_pb          = float(row[1]) * 3.0
        percentage_error = float(row[2])
        signal_list.append((mass, xsec_pb, percentage_error))

canvas = defaultCanvas('emjet_theoryxsec')
gr_central = Graph()
gr_central.SetTitle('Cross section')
gr_central.color = rt.kMagenta
gr_central.legendstyle = 'l'
gr_central.linewidth = 2
gr_sigma_up = Graph()
gr_sigma_up.SetTitle('Uncertainty')
gr_sigma_up.color = rt.kMagenta
gr_sigma_up.legendstyle = 'l'
gr_sigma_up.linestyle = 2
gr_sigma_up.linewidth = 2
gr_sigma_dn = gr_sigma_up.Clone()
# gr_sigma_up.SetLineWidth(2)
for i, s in enumerate(signal_list):
    mass             = s[0]
    xsec_pb          = s[1]
    percentage_error = s[2]
    xsec_error = percentage_error/100 * xsec_pb
    gr_central.SetPoint(i, mass, xsec_pb)
    gr_sigma_up.SetPoint(i, mass, xsec_pb+xsec_error)
    gr_sigma_dn.SetPoint(i, mass, xsec_pb-xsec_error)
gr_central.GetXaxis().SetLabelSize(0.03)
gr_central.GetXaxis().SetTitleSize(0.06)
gr_central.GetXaxis().SetTitleOffset(0.9)
gr_central.GetXaxis().SetTitle('M_{X_{d}}  [GeV]')
gr_central.GetYaxis().SetLabelSize(0.03)
gr_central.GetYaxis().SetTitleSize(0.06)
gr_central.GetYaxis().SetTitleOffset(0.9)
gr_central.GetYaxis().SetTitle('#sigma_{theoretical}  [pb]')
gr_central.Draw("ac")
gr_sigma_up.Draw("c same")
gr_sigma_dn.Draw("c same")
latex = rt.TLatex()
latex.SetTextSize(0.04)
latex.SetTextAlign(12) # Middle left align
latex.SetTextFont(42)
latex.DrawLatexNDC(0.55, 0.85,'NLO+NLL @ 13 TeV')
latex.DrawLatexNDC(0.55, 0.80,'X_{d} pair production')
# latex.DrawLatexNDC(0.05, 0.25,'m_{#pi_{d}} = %g GeV' % mass_pi_d)
legend = Legend([gr_central, gr_sigma_up], topmargin=0.20, leftmargin=0.4, rightmargin=0.05, textfont=42, textsize=0.04)
legend.SetBorderSize(0)
legend.Draw("same")


