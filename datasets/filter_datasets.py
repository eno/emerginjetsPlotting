def sample_name_to_parameters(samplename):
    """Take sample name, e.g. mass_X_d_1000_mass_pi_d_5_tau_pi_d_1 and return parameter outer.parameters as tuple.
    Returns (mass_X_d, mass_pi_d, tau_pi_d)"""
    splitstring = samplename.split('_')
    mass_X_d_string = splitstring[3]
    mass_pi_d_string = splitstring[7]
    tau_pi_d_string = splitstring[11]
    mass_X_d = float(mass_X_d_string)
    mass_pi_d = float(mass_pi_d_string)
    tau_pi_d = float(tau_pi_d_string.replace('p','.'))
    return (mass_X_d, mass_pi_d, tau_pi_d)

with open('aodsim.txt') as f:
    for line in f:
        filter = False
        # sample line: /EmergingJets_mass_X_d_800_mass_pi_d_5_tau_pi_d_60_TuneCUETP8M1_13TeV_pythia8Mod/yoshin-RunIISummer16DR80Premix_private-AODSIM-v2017-09-11re-9b8a2f7f8fb796283f35935e0ffa8bb2/USER
        dataset_name = line.strip()
        a = dataset_name.split('/')
        dataset_parent = a[1]
        tag = a[2]
        # tag.split('yoshin-RunIISummer16DR80Premix_private-AODSIM-')[1]
        tags = tag.split('-')
        tag_production = '-'.join(tags[3:6])
        b = dataset_parent.split('EmergingJets_')[1].split('_TuneCUETP8M1_13TeV_pythia8Mod')[0]
        sample_name = b
        (mass_X_d, mass_pi_d, tau_pi_d) = sample_name_to_parameters(sample_name)
        if mass_pi_d == 1  and tau_pi_d >= 150: filter = True
        if mass_pi_d == 2  and tau_pi_d >= 225: filter = True
        if mass_pi_d == 5  and tau_pi_d >= 300: filter = True
        if mass_pi_d == 10 and tau_pi_d >= 500: filter = True
        if tau_pi_d >= 150: filter = True
        if filter:
            # print (mass_X_d, mass_pi_d, tau_pi_d), tag_production
            print dataset_name
