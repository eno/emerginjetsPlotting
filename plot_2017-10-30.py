"""Plot track modeling systematic"""
from rootpy import ROOT as rt
from rootpy.io import root_open
from rootpy.plotting import Canvas, Graph, Hist1D, Hist2D, Pad
from rootpy.plotting.utils import draw
from collections import OrderedDict
from rootutils import saveAllCanvases, updateAllCanvases, deleteAllCanvases
from histoutils import printBinContents, addOverflowUnderflow
from ratioplot import ratioPlot
import time
from math import sqrt

# import tdrstyle
import CMS_lumi, tdrstyle
#set the tdr style
tdrstyle.setTDRStyle()
rt.gStyle.SetPalette(1)

#change the CMS_lumi variables (see CMS_lumi.py)
CMS_lumi.lumi_7TeV = "4.8 fb^{-1}"
CMS_lumi.lumi_8TeV = "18.3 fb^{-1}"
CMS_lumi.writeExtraText = 1
CMS_lumi.extraText = "    Preliminary"
CMS_lumi.lumi_sqrtS = "13 TeV" # used with iPeriod = 0, e.g. for simulation-only plots (default is an empty string)

# iPos = 11 # "CMS Preliminary" inside border
iPos = 0 # "CMS Preliminary" above border
if( iPos==0 ): CMS_lumi.relPosX = 0.08

H_ref =  800;
W_ref =  800;
# H_ref =  800;
# W_ref = 1200;
W = W_ref
H  = H_ref

#
# Simple example of macro: plot with CMS name and lumi text
#  (this script does not pretend to work in all configurations)
# iPeriod = 1*(0/1 7 TeV) + 2*(0/1 8 TeV)  + 4*(0/1 13 TeV)
# For instance:
#               iPeriod = 3 means: 7 TeV + 8 TeV
#               iPeriod = 7 means: 7 TeV + 8 TeV + 13 TeV
#               iPeriod = 0 means: free form (uses lumi_sqrtS)
# Initiated by: Gautier Hamel de Monchenault (Saclay)
# Translated in Python by: Joshua Hardenbrook (Princeton)
# Updated by:   Dinko Ferencek (Rutgers)
#

iPeriod = 0

# references for T, B, L, R
T = 0.08*H_ref
B = 0.12*H_ref
L = 0.12*W_ref
R = 0.04*W_ref


def defaultCanvas(cname=""):
    # canvas = rt.TCanvas(cname, cname, W_ref, H_ref)
    canvas = Canvas(name=cname, title=cname, width=W_ref, height=H_ref)
    canvas.SetTopMargin(0.08)
    canvas.SetLeftMargin(0.16)
    canvas.SetRightMargin(0.08)
    canvas.cd()
    return canvas

def splitCanvas(canvas_in, top_pad_size=0.7):
    """Split input canvas into two pads, top and bottom"""
    canvas_in.cd()
    leftmargin  = canvas_in.GetLeftMargin()
    rightmargin = canvas_in.GetRightMargin()
    toppad = Pad(0.0, (1.0-top_pad_size), 1.0, 1.0, color=10, bordersize=5, bordermode=0, name=cname+'_num', title='' )
    toppad.SetLeftMargin(leftmargin)
    toppad.SetRightMargin(rightmargin)
    toppad.SetCanvas(canvas_in)
    toppad.SetNumber(1)
    botpad = Pad(0.0, 0.0, 1.0, (1.0-top_pad_size), color=10, bordersize=5, bordermode=0, name=cname+'_den', title='' )
    botpad.SetLeftMargin(leftmargin)
    botpad.SetRightMargin(rightmargin)
    botpad.SetCanvas(canvas_in)
    botpad.SetNumber(2)
    botpad.SetBottomMargin(0.2)
    return (toppad, botpad)

def createProfile(hist_in, func, name="", title=""):
    """Create 1D histogram val vs x from input 2D histogram, where val is computed from an x-slice of the input histogram.
    func must be a function that takes 1D histogram and returns (val, valError)
    """
    if not hasattr(func, '__call__'): raise TypeError('createProfile','func must be a function that takes 1D histogram and returns (val, valError)')
    hist = Hist1D(hist_in.GetNbinsX(), *hist_in.bounds())
    if name  : hist.SetName  ( name  )
    if title : hist.SetTitle ( title )
    nBinsX = hist.GetNbinsX()
    # for i in xrange(-1, nBinsX+1+1):
    for i in xrange(0, nBinsX+1+1):
        hi = hist_in.ProjectionY("_py_"+str(i), i, i, "")
        (val, valError) = func(hi)
        hist.SetBinContent ( i, val      )
        hist.SetBinError   ( i, valError )
    return hist

def getMeanMeanError(hist_in):
    if hist_in.Integral():
        mean      = hist_in.GetMean()
        meanError = sqrt( mean*(1-mean)/hist_in.Integral() )
    else:
        mean      = 0
        meanError = 0
    return (mean, meanError)

def histToGraph(hist_in, name="", title=""):
    """Convert 1D histogram to graph. Ignores underflow/overflow bins"""
    nBins = hist_in.GetNbinsX()
    graph = Graph( nBins )
    for i in xrange( nBins ):
        graph.SetPoint      ( i+1, hist_in.GetBinCenter(i+1), hist_in.GetBinContent(i+1) )
        graph.SetPointError ( i+1, 0, 0, hist_in.GetBinError(i+1) , hist_in.GetBinError(i+1) )
    return graph


unitNormalize = 1

import argparse
parser = argparse.ArgumentParser()
# parser.add_argument('-f', '--force', action='store_true')
# parser.add_argument('sampleset', type=int)
args = parser.parse_args()
# sampleset = args.sampleset
samples = ['QCD', 'Data', ]

# Open files
file_prefix = '/mnt/hepcms/home/yhshin/data/condor_output/2017-10-30/histos-sys0'
files = OrderedDict()
files['Data']         = root_open(file_prefix+'/histo-JetHT_G1.root', 'read')
files['QCD']          = root_open(file_prefix+'/histo-QCD.root', 'read')

# Descriptive label
label = OrderedDict()
label['Data']   = 'Data (JetHT RunG)'
label['QCD']    = 'QCD MC'

# Color histograms
colors = OrderedDict()
colors['Data']     = 'black'
colors['QCD']      = 'blue'
for s in samples:
    for o in list(files[s].objects()):
        o.color = colors[s]
        # print o.GetName()

# Create dummy decorator objects for each sample
# All objects will copy decorators from this object
decos = OrderedDict()
s = 'Data'          ; h = rt.Hist(1,0,1, title=label[s]) ; h.color = colors[s] ; h.markerstyle = 21; h.markersize = 0.2; h.linesize = 2.0; h.drawstyle = 'e'  ; h.legendstyle = 'pl' ; decos[s] = h ;
s = 'QCD'           ; h = rt.Hist(1,0,1, title=label[s]) ; h.color = colors[s] ; h.markerstyle = 25; h.markersize = 0.2; h.linesize = 2.0; h.drawstyle = 'e'  ; h.legendstyle = 'pl' ; decos[s] = h ;
for k, v in decos.items():
    if k not in samples:
        decos.pop(k)

# Make legend that can be copied
x1_l = 0.92
y1_l = 0.60
dx_l = 0.30
dy_l = 0.18
x0_l = x1_l-dx_l
y0_l = y1_l-dy_l
defaultCanvas()
legend =  rt.Legend(decos.values(), topmargin=0.6, rightmargin=0.45, textfont=42)
legend.SetBorderSize(0)

# Get histogram objects from file
histonames = [
    'sys_log_track_ipXY'
]
histos = OrderedDict()
for s in samples:
    histo_list = list(files[s].objects())
    histo_dict={}
    for h in histo_list:
        if h.InheritsFrom("TH1") and h.GetName() in histonames:
            h.color = colors[s]
            h.SetLineWidth(2)
            h.GetXaxis().SetTitleOffset(1.0)
            # h.GetYaxis().SetTitleOffset(1.0)
            h.GetXaxis().SetNoExponent()
            h.GetYaxis().SetTitleSize(20)
            h.GetYaxis().SetTitleFont(43)
            h.GetYaxis().SetTitleOffset(1.55)
            histo_dict[h.GetName()] = h
            # histo_dict[hist.GetName()] = addOverflowUnderflow(hist)
    histos[s] = histo_dict

# Rebin some histograms
# switch_rebin_jetpt = 1
# if switch_rebin_jetpt:
#     # mbins = [(41,100)]
#     mbins = [(31,35),(36,40),(41,60),(61,80),(81,100)]
#     for s in samples:
#         if s == 'WJetData' or s == 'WJetMC':
#             for hname in ['jet_pt__JTbasic', 'jet_pt__JTemerging', 'jet_pt__JTipcut']:
#                 h = histos[s][hname].merge_bins(mbins)
#                 h.decorate(**histos[s][hname].decorators)
#                 histos[s][hname] = h

############################################################
# Plots
############################################################
canvases = OrderedDict()
toppads = OrderedDict()
botpads = OrderedDict()
legends = OrderedDict()
ratios = OrderedDict()

toppadsize=0.7

if 1:
    hdict = OrderedDict()
    cname = 'sys_log_track_ipXY'
    canvases[cname] = defaultCanvas(cname)
    for i, s in enumerate(samples):
        print i, s
        hdict[s] = histos[s]['sys_log_track_ipXY']
        # hdict[s] = addOverflowUnderflow(histos[s]['sys_log_track_ipXY'])
        # h.RebinX(5)
        # hdict[s] = createProfile( h.Clone(cname+"_"+s), getMeanMeanError )
        hdict[s].decorate(decos[s])
        hdict[s].GetXaxis().SetRangeUser(-3, 1.)
        hdict[s].SetXTitle('log10(ipXY) [cm]')
        hdict[s].SetYTitle('# of tracks')
        if unitNormalize: hdict[s].Scale(1./hdict[s].Integral())
    (ratios[cname], toppads[cname], botpads[cname]) = ratioPlot(hdict['Data'], hdict['QCD'], canvases[cname])
    # Clone and draw legend
    legends[cname] = legend.Clone()
    toppads[cname].cd()
    legends[cname].Draw()
    CMS_lumi.CMS_lumi(toppads[cname], iPeriod, 0)



if 0:
    for i, s in enumerate(samples):
        print s
        printBinContents(histos[s]['cutflow'])

# for c in canvases.values():
#     # c.Update()
#     c.cd()
#     c.Paint()
#     c.ExecuteEvent(rt.kButton1Down, 500, 100)
#     c.Modified()
#     c.ForceUpdate()
#     c.ExecuteEvent(rt.kButton1Up, 500, 101)
#     # canvas.ExecuteEventAxis(rt.kButton1Up, 500, 100)

# raw_input("Press Enter to end")



