from rootpy import ROOT as rt
from rootpy.io import root_open
from process_signal_model import *

fileh = root_open("/mnt/hepcms/home/yhshin/data/condor_output/2018-04-05/histos-test-cut8/mass_X_d_1000_mass_pi_d_1_tau_pi_d_25/histo-mass_X_d_1000_mass_pi_d_1_tau_pi_d_25-mass_X_d_1000_mass_pi_d_1_tau_pi_d_25-test-cut8-0.root")

a = calculate_modeling_uncertainty(fileh)
