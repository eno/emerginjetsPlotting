from rootpy.plotting import Hist, Hist2D, Hist3D, HistStack, Legend, Canvas
from collections import OrderedDict

class outer:
    pass

outer.parameters = OrderedDict()
outer.parameters['mass_X_d']   = [400, 600, 800, 1000, 1250, 1500, 2000]
outer.parameters['mass_pi_d']  = [1, 2, 5, 10]
outer.parameters['tau_pi_d']   = [0.001, 0.1, 1, 2, 5, 25, 45, 60, 100, 150, 225, 300, 500, 1000]
outer.nPar = {}
outer.nPar = {k:len(v) for k,v in outer.parameters.items()}


def sample_name_to_parameters(samplename):
    """Take sample name, e.g. mass_X_d_1000_mass_pi_d_5_tau_pi_d_1 and return parameter outer.parameters as tuple.
    Returns (mass_X_d, mass_pi_d, tau_pi_d)"""
    splitstring = samplename.split('_')
    mass_X_d_string = splitstring[3]
    mass_pi_d_string = splitstring[7]
    tau_pi_d_string = splitstring[11]
    mass_X_d = float(mass_X_d_string)
    mass_pi_d = float(mass_pi_d_string)
    tau_pi_d = float(tau_pi_d_string.replace('p','.'))
    return (mass_X_d, mass_pi_d, tau_pi_d)

def signal_parameter_hist(name=""):
    """Return 3D histogram with alphanumeric labels corresponding to signal outer.parameters"""
    hist = Hist3D(outer.nPar['tau_pi_d'], 0, outer.nPar['tau_pi_d'], outer.nPar['mass_pi_d'], 0, outer.nPar['mass_pi_d'], outer.nPar['mass_X_d'], 0, outer.nPar['mass_X_d'], )
    if name:
        hist.SetName(name)
        hist.SetTitle(Title)
    outer.parameters = signal_parameter_dict()
    for mass_X_d in outer.parameters['mass_X_d']:
        for mass_pi_d in outer.parameters['mass_pi_d']:
            for tau_pi_d in outer.parameters['tau_pi_d']:
                hist.Fill("%g" % tau_pi_d, "%g" % mass_pi_d, "%g" % mass_X_d, 0)
    return hist

def mass_X_d_hist(name=""):
    hist = Hist2D( outer.nPar['tau_pi_d'], 0, outer.nPar['tau_pi_d'], outer.nPar['mass_pi_d'], 0, outer.nPar['mass_pi_d'],)
    hist.GetXaxis().SetTitle('Dark pion decay length [mm]')
    hist.GetYaxis().SetTitle('Dark pion mass [GeV]')
    if name:
        hist.SetName(name)
        hist.SetTitle(Title)
    outer.parameters = signal_parameter_dict()
    for mass_pi_d in outer.parameters['mass_pi_d']:
        for tau_pi_d in outer.parameters['tau_pi_d']:
                hist.Fill("%g" % tau_pi_d, "%g" % mass_pi_d, 0)
    return hist

def mass_pi_d_hist(name="", flip=False):
    """flip flips x and y axes"""
    if not flip:
        hist = Hist2D( outer.nPar['tau_pi_d'], 0, outer.nPar['tau_pi_d'], outer.nPar['mass_X_d'], 0, outer.nPar['mass_X_d'], )
        #hist.GetXaxis().SetTitle('Dark pion decay length [mm]')
        #hist.GetYaxis().SetTitle('Dark mediator mass [GeV]')
        hist.GetXaxis().SetTitle('c#tau_{#pi_{DK}} [mm]')
        hist.GetYaxis().SetTitle('m_{X_{DK}} [GeV]')
        if name:
            hist.SetName(name)
            hist.SetTitle(Title)
        outer.parameters = signal_parameter_dict()
        for mass_X_d in outer.parameters['mass_X_d']:
                for tau_pi_d in outer.parameters['tau_pi_d']:
                    hist.Fill("%g" % tau_pi_d, "%g" % mass_X_d, 0)
    else:
        hist = Hist2D( outer.nPar['mass_X_d'], 0, outer.nPar['mass_X_d'], outer.nPar['tau_pi_d'], 0, outer.nPar['tau_pi_d'], )
        #hist.GetXaxis().SetTitle('Dark mediator mass [GeV]')
        #hist.GetYaxis().SetTitle('Dark pion decay length [mm]')
        hist.GetXaxis().SetTitle('m_{X_{DK}} [GeV]')
        hist.GetYaxis().SetTitle('c#tau_{#pi_{DK}} [mm]')
        if name:
            hist.SetName(name)
            hist.SetTitle(Title)
        outer.parameters = signal_parameter_dict()
        for mass_X_d in outer.parameters['mass_X_d']:
                for tau_pi_d in outer.parameters['tau_pi_d']:
                    hist.Fill("%g" % mass_X_d, "%g" % tau_pi_d, 0)
    return hist

def signal_parameter_dict():
    return outer.parameters

def update_signal_parameter_dict():
    outer.nPar = {}
    outer.nPar = {k:len(v) for k,v in outer.parameters.items()}

# def get_projection(hist_in, bin_mass_X_d=-1, bin_mass_pi_d=-1, bin_tau_pi_d=-1):
#     """Get projection of 3D histogram for specified bin"""
#     if bin_mass_X_d > 0:
#         hist_in.ProjectionX()
#     pass
