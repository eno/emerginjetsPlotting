import pdb
import csv
from collections import OrderedDict

from rootpy import ROOT as rt
from rootpy.io import root_open
from rootpy.plotting import Hist, Hist2D, Hist3D, HistStack, Legend, Canvas
import tdrstyle
tdrstyle.setTDRStyle()
rt.gStyle.SetPalette(1)
# Turn off graphics
# rt.gROOT.SetBatch(True)
from rootutils import saveAllCanvases, updateAllCanvases, deleteAllCanvases

import emjet_helpers as helper
# helper.outer.parameters['mass_X_d'].remove(1250)
# helper.outer.parameters['mass_X_d'].remove(1500)
# helper.outer.parameters['mass_X_d'].remove(2000)
helper.outer.parameters['tau_pi_d'].remove(0.001)
helper.outer.parameters['tau_pi_d'].remove(0.1)
# helper.outer.parameters['tau_pi_d'].remove(1)
# helper.outer.parameters['tau_pi_d'].remove(2)
# helper.outer.parameters['tau_pi_d'].remove(500)
helper.update_signal_parameter_dict()

def defaultCanvas(cname=""):
    # canvas = rt.TCanvas(cname, cname, W_ref, H_ref)
    canvas = Canvas(name=cname, title=cname, width=800, height=600)
    canvas.SetTopMargin(0.08)
    canvas.SetLeftMargin(0.16)
    canvas.SetRightMargin(0.16)
    canvas.cd()
    return canvas

def getAcceptance(s):
    """Calculate acceptance from signal csv dict read from signal file. Return percentage value, e.g. 30 for 30%. Return -1 for error conditions."""
    return float(s['acceptance']) * 100

def getModelingSystematic(s):
    """Calculate magnitude of modeling systematic from signal csv dict read from signal file. Return percentage value, e.g. 30 for 30%. Return -1 for error conditions."""
    if float(s['acceptance']) == 0 : return -1
    else                           : percentShift = abs( float(s['acceptance_shift_ModelingUp' ]) / float(s['acceptance']) )
    if percentShift == 0: percentShift = 0.000001 # FIXME Temporary fix
    return percentShift * 100

def getPileupSystematic(s):
    """Calculate magnitude of pileup systematic from signal csv dict read from signal file. Return percentage value, e.g. 30 for 30%. Return -1 for error conditions."""
    if float(s['acceptance']) == 0 : return -1
    percentShift_up = abs( float(s['acceptance_shift_PileupUp' ]) / float(s['acceptance']) )
    percentShift_dn = abs( float(s['acceptance_shift_PileupDn' ]) / float(s['acceptance']) )
    percentShift = max(percentShift_up, percentShift_dn)
    if percentShift == 0: percentShift = 0.000001 # FIXME Temporary fix
    return percentShift * 100

def getJecSystematic(s):
    """Calculate magnitude of JEC systematic from signal csv dict read from signal file. Return percentage value, e.g. 30 for 30%. Return -1 for error conditions."""
    if float(s['acceptance']) == 0 : return -1
    percentShift_up = abs( float(s['acceptance_shift_JecUp' ]) / float(s['acceptance']) )
    percentShift_dn = abs( float(s['acceptance_shift_JecDn' ]) / float(s['acceptance']) )
    percentShift = max(percentShift_up, percentShift_dn)
    if percentShift == 0: percentShift = 0.000001 # FIXME Temporary fix
    return percentShift * 100

def getTriggerSystematic(s):
    """Calculate magnitude of trigger systematic from signal csv dict read from signal file. Return percentage value, e.g. 30 for 30%. Return -1 for error conditions."""
    if float(s['acceptance']) == 0 : return -1
    else                           : percentShift = abs( float(s['acceptance_shift_TriggerUp' ]) / float(s['acceptance']) )
    if percentShift == 0: percentShift = 0.000001 # FIXME Temporary fix
    return percentShift * 100

def getPdfSystematic(s):
    """Calculate magnitude of pdf systematic from signal csv dict read from signal file. Return percentage value, e.g. 30 for 30%. Return -1 for error conditions."""
    if float(s['acceptance']) == 0 : return -1
    percentShift_up = abs( float(s['acceptance_shift_PdfUp' ]) / float(s['acceptance']) )
    percentShift_dn = abs( float(s['acceptance_shift_PdfDn' ]) / float(s['acceptance']) )
    percentShift = max(percentShift_up, percentShift_dn)
    if percentShift == 0: percentShift = 0.000001 # FIXME Temporary fix
    return percentShift * 100

def getMCStatSystematic(s):
    """Calculate magnitude of MC stat systematic from signal csv dict read from signal file. Return percentage value, e.g. 30 for 30%. Return -1 for error conditions."""
    if float(s['acceptance']) == 0 : return -1
    else                           : percentShift = abs( float(s['acceptance_shift_MCStatUp' ]) / float(s['acceptance']) )
    if percentShift == 0: percentShift = 0.000001 # FIXME Temporary fix
    return percentShift * 100


# def getHistRange(hist_in):
#     """Take histogram and return (min, max) values"""
#     minbin = hist_in.GetMinimumBin(); minval = hist_in.GetBinContent(minbin)
#     maxbin = hist_in.GetMaximumBin(); maxval = hist_in.GetBinContent(maxbin)
#     return (minval, maxval)

signalfile = 'emjet_signals_bestcuts_20180323.csv'
postfix = '20180323'

# Read into list of OrderedDict
with open(signalfile) as csvfile:
    reader = csv.DictReader(csvfile)
    keys = reader.fieldnames
    r = csv.reader(csvfile)
    signal_list = [OrderedDict(zip(keys, row)) for row in r]

########################################################################
# Make 2D histograms and write to ROOT file
########################################################################
savePlots = 1
# Acceptance
if savePlots:
    mass_X_d_list = list( set([float(s['mass_X_d']) for s in signal_list]) )
    mass_X_d_list.sort()
    mass_pi_d_list = list( set([float(s['mass_pi_d']) for s in signal_list]) )
    mass_pi_d_list.sort()
    tau_pi_d_list = list( set([float(s['tau_pi_d']) for s in signal_list]) )
    tau_pi_d_list.sort()
    canvases = OrderedDict()
    mass_pi_d_list = [1.0]
    # Save plots in ROOT and PNG files
    if 1:
        # mass_X_d vs tau_pi_d acceptance plots
        for mass_pi_d in mass_pi_d_list:
            hist = helper.mass_pi_d_hist()
            hist.SetName('mass_pi_d_%g' % mass_pi_d)
            name = hist.GetName()
            signal_list_filtered = [s for s in signal_list if float(s['mass_pi_d'])==mass_pi_d]
            for s in signal_list_filtered:
                mass_X_d   = float(s['mass_X_d'   ])
                tau_pi_d   = float(s['tau_pi_d'   ])
                acceptance = float(s['acceptance' ])
                hist.Fill("%g" % tau_pi_d, "%g" % mass_X_d, acceptance)
            canvases[name] = defaultCanvas(name)
            hist.SetMinimum(0)
            hist.SetMaximum(0.40)
            hist.Draw("colz")
            # canvases[name].SaveAs(".png")
            # hist.Write()
            # Make histograms with cut number as entry
            cuthist = helper.mass_pi_d_hist()
            for s in signal_list_filtered:
                mass_X_d   = float(s['mass_X_d'   ])
                tau_pi_d   = float(s['tau_pi_d'   ])
                acceptance = float(s['acceptance' ])
                cut = int( (s['cutname']).replace('cut', '') )
                cuthist.Fill("%g" % tau_pi_d, "%g" % mass_X_d, cut)
            cuthist.Draw("same text")
    # saveAllCanvases(postfix=postfix, prefix="acceptance_")
    # deleteAllCanvases()

