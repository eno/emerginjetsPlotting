from rootpy import ROOT as rt
from rootpy.io import root_open
from rootpy.plotting import Canvas, Hist1D, Hist2D
from rootpy.plotting.utils import draw
from collections import OrderedDict
from rootutils import saveAllCanvases, updateAllCanvases, deleteAllCanvases
from histoutils import printBinContents
from histoutils import printBinContents, printBinContentsWithLabels
import time

import CMS_lumi, tdrstyle
#set the tdr style
tdrstyle.setTDRStyle()

#change the CMS_lumi variables (see CMS_lumi.py)
CMS_lumi.lumi_7TeV = "4.8 fb^{-1}"
CMS_lumi.lumi_8TeV = "18.3 fb^{-1}"
CMS_lumi.writeExtraText = 1
CMS_lumi.extraText = "Preliminary"
CMS_lumi.lumi_sqrtS = "13 TeV" # used with iPeriod = 0, e.g. for simulation-only plots (default is an empty string)

# iPos = 11 # "CMS Preliminary" inside border
iPos = 0 # "CMS Preliminary" above border
if( iPos==0 ): CMS_lumi.relPosX = 0.08

H_ref =  600;
W_ref =  800;
# H_ref =  800;
# W_ref = 1200;
W = W_ref
H  = H_ref

#
# Simple example of macro: plot with CMS name and lumi text
#  (this script does not pretend to work in all configurations)
# iPeriod = 1*(0/1 7 TeV) + 2*(0/1 8 TeV)  + 4*(0/1 13 TeV)
# For instance:
#               iPeriod = 3 means: 7 TeV + 8 TeV
#               iPeriod = 7 means: 7 TeV + 8 TeV + 13 TeV
#               iPeriod = 0 means: free form (uses lumi_sqrtS)
# Initiated by: Gautier Hamel de Monchenault (Saclay)
# Translated in Python by: Joshua Hardenbrook (Princeton)
# Updated by:   Dinko Ferencek (Rutgers)
#

iPeriod = 0

# references for T, B, L, R
T = 0.08*H_ref
B = 0.12*H_ref
L = 0.12*W_ref
R = 0.04*W_ref


def defaultCanvas(cname=""):
    # canvas = rt.TCanvas(cname, cname, W_ref, H_ref)
    canvas = Canvas(name=cname, title=cname, width=W_ref, height=H_ref)
    canvas.SetLeftMargin(0.16)
    canvas.SetRightMargin(0.08)
    canvas.cd()
    return canvas

def createProfile(hist_in, func, name="", title=""):
    """Create 1D histogram val vs x from input 2D histogram, where val is computed from an x-slice of the input histogram.
    func must be a function that takes 1D histogram and returns (val, valError)
    """
    if not hasattr(func, '__call__'): raise TypeError('createProfile','func must be a function that takes 1D histogram and returns (val, valError)')
    hist = Hist1D(hist_in.bounds())
    if name  : hist.SetName  ( name  )
    if title : hist.SetTitle ( title )
    nBinsX = hist.GetNbinsX()
    for i in xrange(-1, nBinsX+1+1):
        hi = hist_in.ProjectionY("_py_"+str(i), i, i, "e")
        (val, valError) = func(hi)
        hist.SetBinContent ( i, val      )
        hist.SetBinError   ( i, valError )
    return hist


samples = ['QCD', 'ModelA', 'ModelB']

# Open files
files = OrderedDict()
files['ModelA']      = root_open('~/tmp/histos-v0/histo-ModelA.root', 'read')
files['ModelB']      = root_open('~/tmp/histos-v0/histo-ModelB.root', 'read')
files['QCD']         = root_open('~/tmp/histos-v0/histo-QCD_HT2000toInf.root', 'read')
# files['QCD']         = root_open('~/www/2017-02-08/histo-QCD.root', 'read')
# files['ModelA']      = root_open('~/www/2017-02-03/histo-ModelA-v1.root', 'read')


############################################################
# Plots
############################################################
canvases = OrderedDict()

for i, s in enumerate(samples):
    print s
    h = files[s].cutflow
    h.Scale(20)
    printBinContentsWithLabels(h)

# for c in canvases.values():
#     # c.Update()
#     c.cd()
#     c.Paint()
#     c.ExecuteEvent(rt.kButton1Down, 500, 100)
#     c.Modified()
#     c.ForceUpdate()
#     c.ExecuteEvent(rt.kButton1Up, 500, 101)
#     # canvas.ExecuteEventAxis(rt.kButton1Up, 500, 100)

# raw_input("Press Enter to end")



