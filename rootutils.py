import pdb

import sys
import re

import ROOT as rt

def getHistos(ifile):
    """Recursively retrieves all histograms from a TDirectory or TFile.
    """
    histos = []
    keys = ifile.GetListOfKeys()
    for key in keys:
        if key.IsFolder():
            histos_temp = getHistos(key.ReadObj())
            histos.extend(histos_temp)
        else:
            obj = key.ReadObj()
            if obj.IsA().InheritsFrom('TH1'):
                histos.append(obj)
    return histos

def getHistosFromCanvas(icanvas):
    """Recursively retrieves all histograms from a TCanvas.
    """
    histos = []
    objects = icanvas.GetListOfPrimitives()
    for obj in objects:
        if obj.IsA().InheritsFrom('TH1'):
            histos.append(obj)
    return histos

def getEventCount(ifile, eventCountDirectory = '', eventCountHistName = 'eventCountPreFilter'):
    """Get event count from specified histogram (in specified directory) in TFile.
    """
    histos_temp = getObjectsByName(ifile, eventCountHistName, 'TH1')
    histosEventCount = [ hist for hist in histos_temp if hist.GetDirectory().GetName()==eventCountDirectory ]
    assert len(histosEventCount)==1
    eventCount =  histosEventCount[0].GetEntries()
    if eventCount == 0: raise Exception('eventCount histogram has zero entries!', ifile.GetName())
    else: return eventCount

def getObjects(ifile, baseClassName='TObject'):
    """Recursively retrieves all objects inheriting from a certain type from a TDirectory or TFile.
    E.g. To retrieve all histograms: getObjects(ifile, 'TH1')
    """
    objects = []
    keys = ifile.GetListOfKeys()
    for key in keys:
        if key.IsFolder():
            objects_temp = getObjects(key.ReadObj())
            objects.extend(objects_temp)
        else:
            obj = key.ReadObj()
            if obj.IsA().InheritsFrom(baseClassName):
                objects.append(obj)
    return objects

def getObjectsByName(ifile, name, baseClassName='TObject'):
    """Recursively retrieves all objects inheriting from a certain type from a TDirectory or TFile,
    and with the given name.
    E.g. To retrieve all histograms named 'htemp': getObjectsByName(ifile, 'htemp', 'TH1')
    """
    objects = []
    keys = ifile.GetListOfKeys()
    for key in keys:
        obj = key.ReadObj()
        if obj.IsA().InheritsFrom(baseClassName) and obj.GetName() == name:
            objects.append(obj)
        if obj.IsA().InheritsFrom("TDirectory"):
            objects_temp = getObjectsByName(key.ReadObj(), name, baseClassName)
            objects.extend(objects_temp)
    return objects

def getObjectsFromCanvas(pad, baseClassName='TObject'):
    """Retrieves all objects inheriting from a certain type from a TPad
    E.g. To retrieve all histograms: getObjectsFromCanvas(pad, 'TH1')
    """
    objects = []
    tlist = pad.GetListOfPrimitives()
    for obj in tlist:
        if obj.IsA().InheritsFrom(baseClassName):
            objects.append(obj)
    return objects

def getTrees(ifile):
    """Recursively retrieves all trees from a TDirectory or TFile.
    """
    return getObjects(ifile, 'TTree')


def saveAllCanvases(option="png", postfix="", prefix=''):
    canvases = rt.gROOT.GetListOfCanvases()
    for canvas in canvases:
        name = canvas.GetName()
        print 'Saving canvas: %s' % name
        name = name.replace('/', '__')
        if postfix : postStr = '_' + postfix
        else       : postStr = ''
        canvas.Print('%s%s%s.%s' % (prefix, name, postStr, option), option)
        # canvas.Print('.' + option)

def saveAsAllCanvases(option="", postfix="", prefix=''):
    canvases = rt.gROOT.GetListOfCanvases()
    for canvas in canvases:
        name = canvas.GetName()
        print 'Saving canvas: %s' % name
        name = name.replace('/', '__')
        if postfix : postStr = '_' + postfix
        else       : postStr = ''
        canvas.SaveAs('%s%s%s.C' % (prefix, name, postStr, ), option)
        # canvas.Print('.' + option)

def updateAllCanvases(option="", postfix=""):
    canvases = rt.gROOT.GetListOfCanvases()
    for canvas in canvases:
        canvas.cd()
        canvas.Paint()
        canvas.ExecuteEvent(rt.kButton1Down, 500, 100)
        canvas.Modified()
        canvas.ForceUpdate()
        canvas.ExecuteEvent(rt.kButton1Up, 500, 101)
        # canvas.ExecuteEventAxis(rt.kButton1Up, 500, 100)

def deleteAllCanvases(option="png", postfix="", prefix=''):
    canvases = rt.gROOT.GetListOfCanvases()
    for canvas in canvases:
        # canvas.Delete()
        canvas.Close()
