import csv
from collections import OrderedDict
from rootpy import ROOT as rt
from rootpy.io import root_open
from rootpy.plotting import Hist, Hist2D, Hist3D, HistStack, Legend, Canvas, Graph, Pad
from rootpy.plotting.utils import draw
import tdrstyle
tdrstyle.setTDRStyle()
rt.gStyle.SetPalette(1)
batch = 1 # SWITCH
if batch:
    # Turn off graphics
    rt.gROOT.SetBatch(True)
blinded = 1 # SWITCH
from rootutils import saveAllCanvases, updateAllCanvases, deleteAllCanvases
from math import sin
import colorsys

# limitfile = 'emjet_limits_20180131.csv'
# limitfile = 'emjet_best_limits.csv'
limitfile = 'emjet_best_limits_20180402.csv'
# limitfile2 = 'emjet_limits_20180125.csv'
# limitfile2 = 'emjet_limits_20171220.csv'
# limitfile2 = 'emjet_limits_20180118.csv'
# limitfile2 = 'emjet_chosen_limits.csv'
limitfile2 = 'emjet_best_limits_20180323.csv'

import emjet_helpers as helper
# helper.outer.parameters['mass_X_d'].remove(1250)
# helper.outer.parameters['mass_X_d'].remove(1500)
# helper.outer.parameters['mass_X_d'].remove(2000)
helper.outer.parameters['tau_pi_d'].remove(0.001)
helper.outer.parameters['tau_pi_d'].remove(0.1)
# helper.outer.parameters['tau_pi_d'].remove(1)
# helper.outer.parameters['tau_pi_d'].remove(2)
# helper.outer.parameters['tau_pi_d'].remove(500)
helper.update_signal_parameter_dict()

pad_x_split = 0.75
pad_ratio = pad_x_split / (1 - pad_x_split)

yaxis_maximum = {
    400  : 120000 ,
    600  : 120000 ,
    800  : 120000 ,
    1000 : 120000 ,
    1250 : 120000 ,
    1500 : 120000 ,
    2000 : 12000 ,
}
yaxis_minimum = {
    400  : 0.1 ,
    600  : 0.1 ,
    800  : 0.1 ,
    1000 : 0.1 ,
    1250 : 0.1 ,
    1500 : 0.1 ,
    2000 : 0.01 ,
    # 400  : 10  ,
    # 600  : 1   ,
    # 800  : 1   ,
    # 1000 : 0.1 ,
    # 1250 : 0.1 ,
}
xsec_fb = {
    400   : 5506.11   ,
    600   : 523.797   ,
    800   : 85.0014   ,
    1000  : 18.45402  ,
    1250  : 3.47490   ,
    1500  : 0.768744  ,
    2000  : 0.0487065 ,
}


def defaultCanvas(cname=""):
    canvas = Canvas(name=cname, title=cname, width=900, height=600)
    # canvas.SetTopMargin(0.08)
    # canvas.SetLeftMargin(0.16)
    # canvas.SetRightMargin(0.08)
    canvas.SetTopMargin(0.0)
    canvas.SetLeftMargin(0.0)
    canvas.SetRightMargin(0.0)
    # canvas.SetLogy()
    canvas.cd()
    return canvas

def draw_cms_header(canvas=None):
    t = canvas.GetTopMargin()
    l = canvas.GetLeftMargin()
    r = canvas.GetRightMargin()
    if canvas: canvas.cd()
    latex = rt.TLatex()
    latex.SetTextAlign(11) # Left bottom aligned
    latex.DrawLatexNDC(l, (1-t)+0.2*t, '#scale[1.2]{#font[62]{CMS}} #font[52]{#scale[1.0]{Preliminary}}')
    latex.SetTextAlign(31) # Right bottom aligned
    latex.DrawLatexNDC(1-r, (1-t)+0.2*t, '#font[42]{16.1 fb^{-1} (13 TeV)}')

def buildColorPalette(number):
    """Build color palette by dividing the color spectrum into the required number of colors"""
    colors = []
    for i in xrange(number):
        rgb_tuple = colorsys.hls_to_rgb(i/float(number), 0.5, 1.0)
        scaled_rgb_tuple = tuple([256*x for x in rgb_tuple])
        colors.append(scaled_rgb_tuple)
    return colors


# Generated from paletton.com
# *** Primary color:
colors = [None]*20
i = 0
colors [i*5+0] = (127, 42,104)
colors [i*5+1] = (190,127,173)
colors [i*5+2] = (159, 79,137)
colors [i*5+3] = ( 95, 16, 73)
colors [i*5+4] = ( 63,  0, 46)

# *** Secondary color (1):
i += 1
colors [i*5+0] = (161, 54, 70)
colors [i*5+1] = (241,161,173)
colors [i*5+2] = (201,100,116)
colors [i*5+3] = (120, 20, 36)
colors [i*5+4] = ( 80,  0, 13)

# *** Secondary color (2):
i += 1
colors [i*5+0] = ( 82, 43,114)
colors [i*5+1] = (147,118,172)
colors [i*5+2] = (112, 76,143)
colors [i*5+3] = ( 55, 19, 86)
colors [i*5+4] = ( 33,  4, 57)

# *** Complement color:
i += 1
colors [i*5+0] = ( 61, 49,117)
colors [i*5+1] = (134,125,176)
colors [i*5+2] = ( 94, 82,147)
colors [i*5+3] = ( 35, 24, 88)
colors [i*5+4] = ( 17,  7, 59)

# Read into list of OrderedDict
with open(limitfile) as csvfile:
    reader = csv.DictReader(csvfile)
    keys = reader.fieldnames
    r = csv.reader(csvfile)
    signal_list = [OrderedDict(zip(keys, row)) for row in r]
with open(limitfile2) as csvfile:
    reader = csv.DictReader(csvfile)
    keys = reader.fieldnames
    r = csv.reader(csvfile)
    signal_list2 = [OrderedDict(zip(keys, row)) for row in r]

# Testing
if 0: #SWITCH
    hist = Graph(6)
    hist2 = Graph(6)
    for i in range(6):
        hist.SetPoint(i, i, sin(i*3.14/6))
        hist.SetPointError(i, 0., 0., 0.5* sin(i*3.14/6), 0.5* sin(i*3.14/6))
        hist.color = rt.kOrange
        hist.fillstyle = 1001
        # hist.drawstyle = 'e3'
        hist2.SetPoint(i, i, sin(i*3.14/6))
        hist2.SetPointError(i, 0., 0., 0.1* sin(i*3.14/6), 0.1* sin(i*3.14/6))
        hist2.color = rt.kGreen+1
        hist2.fillstyle = 1001
        # hist2.drawstyle = 'e3'
    # hist.Draw("ACE3")
    # hist2.Draw("sameCE3")

########################################################################
# Make 1D graphs and write to file
########################################################################
# Define list of parameters
mass_X_d_list = list( set([float(s['mass_X_d']) for s in signal_list]) )
mass_X_d_list.sort()
mass_pi_d_list = list( set([float(s['mass_pi_d']) for s in signal_list]) )
mass_pi_d_list.sort()
tau_pi_d_list = list( set([float(s['tau_pi_d']) for s in signal_list]) )
tau_pi_d_list.sort()
# mass_X_d_list = [1000]
# mass_pi_d_list = [1, 10]


# 1d limit plots (overlaid mass_pi_d)
if 1: #SWITCH
    # Define dummy histogram and decorators
    hist_dummy = Hist(1, 1, 520)
    hist_dummy.SetLineColor(rt.kMagenta)
    hist_dummy.SetLineWidth(2)
    hist_dummy.GetXaxis().SetTitle('c#tau_{#pi_{d}}  [mm]')
    hist_dummy.GetYaxis().SetTitle('#sigma  [fb]')
    hist_dummy.GetXaxis().SetTitleOffset(1.00)
    hist_dummy.GetYaxis().SetTitleOffset(1.20)
    hist_dummy.legendstyle = 'l'
    decorator_expected = Graph(title='Median expected')
    decorator_sigma1   = Graph(title='#pm 1 #sigma_{experiment}')
    decorator_observed = Graph(title='Observed')
    decorator_expected .linewidth = 3
    decorator_expected .linestyle = 2
    decorator_observed .linewidth = 3
    decorator_sigma1   .linewidth = 2
    decorator_observed .legendstyle = 'l'
    decorator_expected .legendstyle = 'l'
    decorator_sigma1   .legendstyle = 'l'
    decorators = [
        decorator_observed ,
        decorator_expected ,
        decorator_sigma1   ,
    ]
    ntau = len(tau_pi_d_list)
    canvases = OrderedDict()
    leftpads = OrderedDict()
    rightpads = OrderedDict()
    # Save plots in ROOT and PNG files
    for mass_X_d in mass_X_d_list:
        # histos_expected  = []
        # histos_sigma1_up = []
        # histos_sigma1_dn = []
        # histos_observed  = []
        legend_entries   = []
        # Setup plot
        name = 'mass_X_d_%g' % (mass_X_d)
        canvases[name] = defaultCanvas(name)
        leftpads[name] = Pad(0, 0, pad_x_split, 1)
        leftpads[name].SetLogy()
        leftpads[name].SetTopMargin(0.08)
        leftpads[name].SetLeftMargin(0.16)
        leftpads[name].SetRightMargin(0.01)
        rightpads[name] = Pad(pad_x_split, 0, 1, 1)
        rightpads[name].SetTopMargin(0.08)
        rightpads[name].SetLeftMargin(0.01)
        rightpads[name].SetRightMargin(0.00)
        leftpads[name].Draw()
        rightpads[name].Draw()
        leftpads[name].cd()
        hist_dummy_clone = hist_dummy.Clone()
        hist_dummy_clone.SetTitle('m_{X_{d}} = %g GeV' % mass_X_d)
        hist_dummy_clone.SetBinContent(1, xsec_fb[mass_X_d])
        hist_dummy_clone.SetMinimum(yaxis_minimum[mass_X_d])
        hist_dummy_clone.SetMaximum(yaxis_maximum[mass_X_d])
        hist_dummy_clone.Draw("h")
        for imass_pi_d, mass_pi_d in enumerate(mass_pi_d_list):
            ################################################
            # Current limits
            ################################################
            hist_expected  = Graph()
            hist_sigma1_up = Graph()
            hist_sigma1_dn = Graph()
            hist_observed  = Graph()
            hist_expected  .decorate( decorator_expected )
            hist_sigma1_up .decorate( decorator_sigma1   )
            hist_sigma1_dn .decorate( decorator_sigma1   )
            hist_observed  .decorate( decorator_observed )
            hist_expected  .color = colors[4*imass_pi_d+1]
            hist_sigma1_up .color = colors[4*imass_pi_d+1]
            hist_sigma1_dn .color = colors[4*imass_pi_d+1]
            hist_observed  .color = colors[4*imass_pi_d+1]
            hist_expected  .SetName('%s_expected' % name)
            hist_sigma1_up .SetName('%s_sigma1_up'% name)
            hist_sigma1_dn .SetName('%s_sigma1_dn'% name)
            hist_observed  .SetName('%s_observed' % name)
            signal_list_filtered = [s for s in signal_list if float(s['mass_X_d'])==mass_X_d and float(s['mass_pi_d'])==mass_pi_d]
            for itau, s in enumerate(signal_list_filtered):
                tau_pi_d  = float(s['tau_pi_d'])
                expected  = float(s['sigma0'])
                sigma1_up = ( float(s['sigma1']  ) )
                sigma1_dn = ( float(s['sigma-1'] ) )
                observed  = float(s['observed'])
                hist_expected  .SetPoint(itau, tau_pi_d, expected)
                hist_sigma1_up .SetPoint(itau, tau_pi_d, sigma1_up)
                hist_sigma1_dn .SetPoint(itau, tau_pi_d, sigma1_dn)
                hist_observed  .SetPoint(itau, tau_pi_d, observed)
            hist_expected  .linestyle = 1
            hist_expected  .Draw("l same")
            # hist_sigma1_up .Draw("c same")
            # hist_sigma1_dn .Draw("c same")
            # hist_observed  .Draw("l same")
            hist_observed.SetTitle('m_{#pi_{d}} = %g GeV' % (mass_pi_d))
            ################################################
            # Alternate or old limits
            ################################################
            hist_expected_2  = Graph()
            hist_sigma1_up_2 = Graph()
            hist_sigma1_dn_2 = Graph()
            hist_observed_2  = Graph()
            hist_expected_2  .decorate( decorator_expected )
            hist_sigma1_up_2 .decorate( decorator_sigma1   )
            hist_sigma1_dn_2 .decorate( decorator_sigma1   )
            hist_observed_2  .decorate( decorator_observed )
            hist_expected_2  .color = colors[4*imass_pi_d+1]
            hist_sigma1_up_2 .color = colors[4*imass_pi_d+1]
            hist_sigma1_dn_2 .color = colors[4*imass_pi_d+1]
            hist_observed_2  .color = colors[4*imass_pi_d+1]
            hist_expected_2  .SetName('%s_expected_2' % name)
            hist_sigma1_up_2 .SetName('%s_sigma1_up_2'% name)
            hist_sigma1_dn_2 .SetName('%s_sigma1_dn_2'% name)
            hist_observed_2  .SetName('%s_observed_2' % name)
            signal_list_filtered = [s for s in signal_list2 if float(s['mass_X_d'])==mass_X_d and float(s['mass_pi_d'])==mass_pi_d]
            for itau, s in enumerate(signal_list_filtered):
                tau_pi_d  = float(s['tau_pi_d'])
                expected  = float(s['sigma0'])
                sigma1_up = ( float(s['sigma1']  ) )
                sigma1_dn = ( float(s['sigma-1'] ) )
                observed  = float(s['observed'])
                hist_expected_2  .SetPoint(itau, tau_pi_d, expected)
                hist_sigma1_up_2 .SetPoint(itau, tau_pi_d, sigma1_up)
                hist_sigma1_dn_2 .SetPoint(itau, tau_pi_d, sigma1_dn)
                hist_observed_2  .SetPoint(itau, tau_pi_d, observed)
            hist_expected_2  .linestyle = 2
            hist_expected_2  .Draw("l same")
            # hist_sigma1_up_2 .Draw("c same")
            # hist_sigma1_dn_2 .Draw("c same")
            # hist_observed_2  .Draw("l same")
            legentry = hist_observed.Clone()
            legentry.fillcolor = legentry.color[0]
            legentry.linewidth = 0
            legentry.fillstyle = 1001
            legentry.legendstyle = 'f'
            legend_entries  .append( legentry )
        # '#splitline{m_{X_{d}}= %g GeV}{m_{#pi_{d}}= %g GeV}'  % (mass_X_d, mass_pi_d)
        # legend = Legend(decorators, header='#font[42]{95% CL_{s} limits}', leftmargin=0.05, rightmargin=0.5, textfont=42, textsize=0.03, entryheight=0.04)
        rightpads[name].cd()
        legend_s = Legend([hist_dummy_clone], topmargin=0.05, leftmargin=0.0, rightmargin=0.0, textfont=42, textsize=0.03*pad_ratio)
        legend_s.SetBorderSize(0)
        legend_s.Draw()
        legend = Legend([], header='#font[42]{95% CL_{s} limits}',topmargin=0.15, leftmargin=0.0, rightmargin=0.0, entryheight=0.06, entrysep=0.02, textfont=42, textsize=0.03*pad_ratio)
        legend.AddEntry(decorator_observed, 'New')
        legend.AddEntry(decorator_expected, 'Old')
        legend.SetBorderSize(0)
        legend.Draw("same")
        legend_pi_d = Legend(legend_entries, topmargin=0.50, leftmargin=0.0, rightmargin=0.0, entryheight=0.06, entrysep=0.02, textfont=42, textsize=0.03*pad_ratio)
        legend_pi_d.SetBorderSize(0)
        legend_pi_d.Draw("same")
        # latex = rt.TLatex()
        # latex.SetTextSize(0.03*pad_ratio)
        # latex.SetTextAlign(12) # Middle left align
        # latex.SetTextFont(42)
        # latex.DrawLatexNDC(0.05, 0.30,'m_{X_{d}} = %g GeV' % mass_X_d)
        # latex.DrawLatexNDC(0.05, 0.25,'m_{#pi_{d}} = %g GeV' % mass_pi_d)
        # draw_cms_header(canvases[name])
        draw_cms_header(leftpads[name])
    if batch:
        saveAllCanvases(prefix="limits1D_compare_")
        deleteAllCanvases()
