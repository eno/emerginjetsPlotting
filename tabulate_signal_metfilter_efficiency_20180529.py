"""Plot track modeling systematic"""
import argparse
parser = argparse.ArgumentParser()
# parser.add_argument('-f', '--force', action='store_true')
# parser.add_argument('sampleset', type=int)
args = parser.parse_args()
# sampleset = args.sampleset

from rootpy import ROOT as rt
from rootpy.io import root_open

import os, os.path

import csv

from collections import OrderedDict

from emjet_helpers import sample_name_to_parameters # Returns (mass_X_d, mass_pi_d, tau_pi_d)
from emjet_helpers import signal_parameter_hist
from process_signal_model import get_pdf_uncertainty
from process_signal_model import get_trigger_uncertainty
from process_signal_model import get_modeling_uncertainty
from process_signal_model import get_jec_uncertainty
from process_signal_model import get_pileup_uncertainty
from process_signal_model import get_mcstat_uncertainty
from process_signal_model import get_cutflow

def get_filter_efficiency(fileh, name='badMuon', debug=0):
    filterhist = getattr(fileh, 'metFilter_%s' % name)
    efficiency = filterhist.GetMean()
    return efficiency

def get_met_uncertainty(fileh, debug=0):
    denominator = fileh.metunc_den.GetEntries()
    accept = fileh.metunc_raw_central_num.GetEntries() / denominator
    accept_up = fileh.metunc_raw_shifted_up_num.GetEntries() / denominator
    accept_dn = fileh.metunc_raw_shifted_dn_num.GetEntries() / denominator
    if accept != 0:
        shift_up = accept_up - accept
        shift_dn = accept_dn - accept
    else:
        shift_up = 0.0
        shift_dn = 0.0
    return (accept, shift_up, shift_dn)

filternames = [
    # 'HBHENoise',
    # 'HBHENoiseIso',
    # 'EcalDeadCellTP',
    # 'goodVertices',
    # 'eeBadSc',
    # 'beamHalo',
    'badCharged',
    'badMuon',
    # 'all',
]

mainpath = '/mnt/hepcms/home/yhshin/data/condor_output/2018-05-28/histos-met-v0'
# Dictionary of cross sections for each mediator mass
cross_sections_fb = {
    400   : 5506.11   ,
    600   : 523.797   ,
    800   : 85.0014   ,
    1000  : 18.45402  ,
    1250  : 3.47490   ,
    1500  : 0.768744  ,
    2000  : 0.0487065 ,
}

# cutfilepath = '/mnt/hepcms/home/yhshin/workspace/EmJetHistoMaker/cuts/modelset_combined_1121.txt'
# cutfilepath = '/mnt/hepcms/home/yhshin/workspace/EmJetHistoMaker/cuts/modelset_combined_1122.txt'
# cutfilepath = '/mnt/hepcms/home/yhshin/workspace/EmJetHistoMaker/cuts/modelset_0105_combined.txt'
# cutfilepath = 'modelset_best_limits_20180212.txt'

samplelist = [x for x in os.listdir(mainpath) if not os.path.isdir(x)]

signallist = []
for root, dirs, files in os.walk(mainpath, topdown=False):
    if root == mainpath: # Only check one level
        for filename in files: # For each file in mainpath
            if not ( filename[:6] == 'histo-' and filename[-5:] == '.root' ):
                print 'filename invalid. Skip!'
                continue
            print 'Processing %s' % (filename)
            s = OrderedDict()
            samplename = filename[6:-5]
            if samplename[:8] != 'mass_X_d':
                print 'Not a signal sample'
                continue
            s['name'] = samplename
            signal_file_path =  os.path.join(root, filename)
            fileh = root_open(signal_file_path, 'read')
            # Get parameters
            # parameters = sample_name_to_parameters(filename[6:-5])
            # s['mass_X_d']  = parameters[0]
            # s['mass_pi_d'] = parameters[1]
            # s['tau_pi_d']  = parameters[2]
            for f in filternames:
                s[f] = get_filter_efficiency(fileh, f)
            met_unc = get_met_uncertainty(fileh)
            s['acceptance_percent'] = met_unc[0] * 100
            s['acceptance_percent_shift_met_up'] = met_unc[1] / met_unc[0] * 100
            s['acceptance_percent_shift_met_dn'] = met_unc[2] / met_unc[0] * 100
            signallist.append(s)
            fileh.Close()

# signallist.sort(key=lambda k: (k['mass_X_d'], k['mass_pi_d'], k['tau_pi_d'],))



# Write results to CSV file
writeToFile = 1
if writeToFile:
    with open('signal_metfilter_efficiency_20180529.csv', 'w') as csvfile:
        fieldnames = signallist[0].keys()
        writer = csv.DictWriter(csvfile, quotechar='"', fieldnames=fieldnames)
        writer.writeheader()
        for s in signallist:
            writer.writerow(s)

