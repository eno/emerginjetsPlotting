from rootpy import ROOT as rt
from rootpy.io import root_open
from rootpy.plotting import Canvas, Hist1D, Hist2D
from rootpy.plotting.utils import draw
from collections import OrderedDict
from rootutils import saveAllCanvases, updateAllCanvases, deleteAllCanvases
from histoutils import printBinContents
import time

import CMS_lumi, tdrstyle
#set the tdr style
tdrstyle.setTDRStyle()
rt.gStyle.SetPalette(1)

#change the CMS_lumi variables (see CMS_lumi.py)
CMS_lumi.lumi_7TeV = "4.8 fb^{-1}"
CMS_lumi.lumi_8TeV = "18.3 fb^{-1}"
CMS_lumi.writeExtraText = 1
CMS_lumi.extraText = "Preliminary"
CMS_lumi.lumi_sqrtS = "13 TeV" # used with iPeriod = 0, e.g. for simulation-only plots (default is an empty string)

# iPos = 11 # "CMS Preliminary" inside border
iPos = 0 # "CMS Preliminary" above border
if( iPos==0 ): CMS_lumi.relPosX = 0.08

H_ref =  600;
W_ref =  800;
# H_ref =  800;
# W_ref = 1200;
W = W_ref
H  = H_ref

#
# Simple example of macro: plot with CMS name and lumi text
#  (this script does not pretend to work in all configurations)
# iPeriod = 1*(0/1 7 TeV) + 2*(0/1 8 TeV)  + 4*(0/1 13 TeV)
# For instance:
#               iPeriod = 3 means: 7 TeV + 8 TeV
#               iPeriod = 7 means: 7 TeV + 8 TeV + 13 TeV
#               iPeriod = 0 means: free form (uses lumi_sqrtS)
# Initiated by: Gautier Hamel de Monchenault (Saclay)
# Translated in Python by: Joshua Hardenbrook (Princeton)
# Updated by:   Dinko Ferencek (Rutgers)
#

iPeriod = 0

# references for T, B, L, R
T = 0.08*H_ref
B = 0.12*H_ref
L = 0.12*W_ref
R = 0.04*W_ref


def defaultCanvas(cname=""):
    # canvas = rt.TCanvas(cname, cname, W_ref, H_ref)
    canvas = Canvas(name=cname, title=cname, width=W_ref, height=H_ref)
    canvas.SetLeftMargin(0.16)
    canvas.SetRightMargin(0.08)
    canvas.cd()
    return canvas

def createProfile(hist_in, func, name="", title=""):
    """Create 1D histogram val vs x from input 2D histogram, where val is computed from an x-slice of the input histogram.
    func must be a function that takes 1D histogram and returns (val, valError)
    """
    if not hasattr(func, '__call__'): raise TypeError('createProfile','func must be a function that takes 1D histogram and returns (val, valError)')
    hist = Hist1D(hist_in.bounds())
    if name  : hist.SetName  ( name  )
    if title : hist.SetTitle ( title )
    nBinsX = hist.GetNbinsX()
    for i in xrange(-1, nBinsX+1+1):
        hi = hist_in.ProjectionY("_py_"+str(i), i, i, "e")
        (val, valError) = func(hi)
        hist.SetBinContent ( i, val      )
        hist.SetBinError   ( i, valError )
    return hist

unitNormalize = 0

import argparse
parser = argparse.ArgumentParser()
# parser.add_argument('-f', '--force', action='store_true')
args = parser.parse_args()
samples = [0, 91, 102]

# Open files
file_prefix = '/mnt/hepcms/home/yhshin/dev/CMSSW_8_0_26_patch1/src/EmergingJetAnalysis'
files = OrderedDict()
files[0]   = root_open(file_prefix+'/ntuple-decayMode0.root', 'read')
files[91]  = root_open(file_prefix+'/ntuple-decayMode91.root', 'read')
files[102] = root_open(file_prefix+'/ntuple-decayMode102.root', 'read')

# Descriptive label
label = OrderedDict()
label[0]   = 'Decay mode 0'
label[91]  = 'Decay mode 91'
label[102] = 'Decay mode 102'

# Color histograms
colors = OrderedDict()
colors[0]     = 'black'
colors[91]    = 'red'
colors[102]   = 'blue'
for s in samples:
    for o in list(files[s].objects()):
        o.color = colors[s]
        # print o.GetName()

# Create dummy decorator objects for each sample
# All objects will copy decorators from this object
decos = OrderedDict()
s = 0   ; h = rt.Hist(1,0,1, title=label[s]) ; h.color = colors[s] ; h.legendstyle = 'l' ; decos[s] = h ;
s = 91  ; h = rt.Hist(1,0,1, title=label[s]) ; h.color = colors[s] ; h.legendstyle = 'l' ; decos[s] = h ;
s = 102 ; h = rt.Hist(1,0,1, title=label[s]) ; h.color = colors[s] ; h.legendstyle = 'l' ; decos[s] = h ;
for k, v in decos.items():
    if k not in samples:
        decos.pop(k)
# s = 'ModelA' ; h = rt.Hist(1,0,1, title=label[s]) ; h.color = colors[s] ; h.legendstyle = 'l' ; decos[s] = h ;
# s = 'ModelB' ; h = rt.Hist(1,0,1, title=label[s]) ; h.color = colors[s] ; h.legendstyle = 'l' ; decos[s] = h ;

# Make legend that can be copied
x1_l = 0.92
y1_l = 0.60
dx_l = 0.30
dy_l = 0.18
x0_l = x1_l-dx_l
y0_l = y1_l-dy_l
defaultCanvas()
legend =  rt.Legend(decos.values())
legend.SetBorderSize(0)

histos = OrderedDict()
for s in samples:
    histo_list = list(files[s].genParticleAnalyzer.objects())
    histo_dict={}
    for h in histo_list:
        if h.InheritsFrom("TH1"):
            h.color = colors[s]
            h.SetLineWidth(1)
            h.SetFillStyle(0)
            h.GetXaxis().SetTitleOffset(1.0)
            h.GetYaxis().SetTitleOffset(1.0)
            h.GetXaxis().SetNoExponent()
            histo_dict[h.GetName()] = h
            # histo_dict[hist.GetName()] = addOverflowUnderflow(hist)
    histos[s] = histo_dict


############################################################
# Plots
############################################################
canvases = OrderedDict()

if 1:
    hdict = OrderedDict()
    cname = 'pt'
    canvases[cname] = defaultCanvas(cname)
    for i, s in enumerate(samples):
        print i, s
        h = histos[s]['pt_Daughter_DarkRho_']
        h.SetXTitle('p_{T} [GeV]')
        hdict[s] = h.Clone(cname+"_"+str(s))
        if unitNormalize: hdict[s].Scale(1./hdict[s].Integral())
        # hdict[s].Draw("colz")
    draw(hdict.values(), logx=0)
    CMS_lumi.CMS_lumi(canvases[cname], iPeriod, 0)
    legend.Clone().Draw()

