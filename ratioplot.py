from rootpy import ROOT as rt
from rootpy.plotting.utils import draw
# from ROOT import Canvas, Color, Axis, Hist, Pad
# from ROOT import kBlack, kBlue, kRed

titleSize = 40
labelSize = 30
# yTitleOffset = 1.3
yTitleOffset = 1.0
xTitleOffset = 3.0
rightMargin = 1.0


def createH1():
    h1 = rt.TH1F("h1", ("Two gaussian plots and their ratio; x title; h1 and h2"
                " histograms"), 100, -5, 5)
    h1.SetLineColor(rt.kBlue+1)
    h1.SetLineWidth(2)
    h1.FillRandom("gaus")
    h1.GetYaxis().SetTitleSize(titleSize)
    h1.GetYaxis().SetTitleFont(43)
    h1.GetYaxis().SetTitleOffset(yTitleOffset)
    h1.SetStats(0)
    return h1


def createH2():
    h2 = rt.TH1F("h2", "h2", 100, -5, 5)
    h2.FillRandom("gaus")
    h2.SetLineColor(rt.kRed)
    h2.SetLineWidth(2)
    return h2


def createRatio(h1, h2):
    h3 = h1.Clone("h3")
    h3.SetLineColor(rt.kBlack)
    h3.SetMarkerColor(rt.kBlack)
    h3.SetMarkerStyle(21)
    h3.SetTitle("")
    # h3.SetMinimum(0.8)
    # h3.SetMaximum(1.35)
    # Set up plot for markers and errors
    h3.Sumw2()
    h3.SetStats(0)
    h3.Divide(h2)

    # Adjust y-axis settings
    y = h3.GetYaxis()
    y.SetTitle("Data/MC")
    y.SetNdivisions(505)
    y.SetTitleSize(titleSize)
    y.SetTitleFont(43)
    y.SetTitleOffset(yTitleOffset)
    y.SetLabelFont(43)
    y.SetLabelSize(labelSize)

    # Adjust x-axis settings
    x = h3.GetXaxis()
    x.SetTitleSize(titleSize)
    x.SetTitleFont(43)
    x.SetTitleOffset(4.0)
    x.SetLabelFont(43)
    x.SetLabelSize(labelSize)

    return h3


def createCanvasPads(canvas_in=None):
    if not canvas_in : c = rt.TCanvas("c", "canvas", 800, 800)
    else             : c = canvas_in
    # Upper histogram plot is pad1
    pad1 = rt.TPad("pad1", "pad1", 0, 0.3, 1, 1.0)
    pad1.SetTopMargin(0.08)
    # pad1.SetBottomMargin(0)  # joins upper and lower plot
    pad1.SetBottomMargin(0.05)
    pad1.SetRightMargin(rightMargin)
    pad1.SetGridx()
    pad1.Draw()
    # Lower ratio plot is pad2
    c.cd()  # returns to main canvas before defining pad2
    pad2 = rt.TPad("pad2", "pad2", 0, 0.00, 1, 0.3)
    # pad2.SetTopMargin(0)  # joins upper and lower plot
    pad2.SetTopMargin(0.0)
    pad2.SetRightMargin(rightMargin)
    pad2.SetBottomMargin(0.45)
    pad2.SetGridx()
    pad2.Draw()

    return c, pad1, pad2

def ratioPlot(hdata, hmc, canvas_in=None, xlimits=None):
    # Standardize formatting
    hdata.GetXaxis().SetLabelSize(0)
    hdata.GetXaxis().SetTitleSize(0)
    hdata.GetYaxis().SetLabelSize(labelSize)
    hdata.GetYaxis().SetLabelFont(43)
    hdata.GetYaxis().SetTitleSize(titleSize)
    hdata.GetYaxis().SetTitleFont(43)
    hdata.GetYaxis().SetTitleOffset(yTitleOffset)

    # create required parts
    h3 = createRatio(hdata, hmc)
    c, pad1, pad2 = createCanvasPads(canvas_in)

    # draw everything
    pad1.cd()
    # hdata.GetYaxis().SetLabelSize(0.0)
    # hdata.GetXaxis().SetRangeUser(-1, 20.0)
    # x = hdata.GetXaxis()
    # print x.GetXmin(), x.GetXmax()
    # if xlimits: toppad_axis = draw([hdata, hmc], pad1) #, xaxis= xaxis) #, xlimits=[-1, 20])
    toppad_axis = draw([hdata, hmc], pad1, xlimits=xlimits) #, xaxis= xaxis) #, xlimits=[-1, 20])
    # pad1.cd()
    # axis = rt.TGaxis(0.2, 0.3, 0.2, 0.9, 0.0, 1.0, 510, "")
    # axis.SetLabelFont(43)
    # axis.SetLabelSize(15)
    # axis.Draw()
    # hdata.Draw()
    # hmc.Draw("same")
    # to avoid clipping the bottom zero, redraw a small axis
    pad2.cd()
    botpad_axis = draw(h3, pad2, xlimits=xlimits)
    return (h3, pad1, pad2, toppad_axis, botpad_axis)

