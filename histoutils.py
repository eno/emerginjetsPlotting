import sys
import random
import ROOT as rt
import math
from array import array
# rt.gROOT.ProcessLine('.x $HOME/rootlogon.C')
# import CMS_lumi, tdrstyle
# tdrstyle.setTDRStyle()

import warnings

def randomPostfix(nDigits):
    num = random.randint(1, 10**nDigits -1)
    pstr = '{:0' + str(nDigits) + '}'
    return pstr.format(num)

def transformHistogram(hist_in, func_in, name=""):
    nBins = ( hist_in.GetNbinsX()+2 ) * ( hist_in.GetNbinsY()+2 ) * ( hist_in.GetNbinsZ()+2 )
    hist = hist_in
    if name: hist.SetName(name)
    for i in range(nBins):
        # print  i+1, hist_in.GetBinContent(i+1)
        hist.SetBinContent( i+1, func_in( hist_in.GetBinContent(i+1) ) )
    return hist

def random1DHistogram():
    """Returns unique TH1D filled with \"gaus\". Use for testing."""
    postfix = randomPostfix(3)
    hist = rt.TH1D("hist"+postfix, "hist"+postfix, 20, -10, 10)
    hist.FillRandom("gaus", 5000)
    return hist

def random2DHistogram():
    """Returns unique TH2D filled with 2D Gaussian. Use for testing."""
    postfix = randomPostfix(3)
    f = rt.TF2("2dgaus", "TMath::Gaus(x)*TMath::Gaus(y)", -10, 10, -10, 10)
    hist = rt.TH2F("hist"+postfix, "hist"+postfix, 20, -10, 10, 20, -10, 10)
    hist.FillRandom("2dgaus", 250000)
    return hist

def getMaxMin(*arguments, **keywords):
    """Get true maximum/minimum of provided histograms by iterating over bins."""
    hist = arguments[0]
    max_temp = hist.GetBinContent(1)
    min_temp = hist.GetBinContent(1)
    for hist in arguments:
        NbinsX = hist.GetNbinsX()
        NbinsY = hist.GetNbinsY()
        NbinsZ = hist.GetNbinsZ()
        bins = [(x+1, y+1, z+1) for x in range(NbinsX) for y in range(NbinsY) for z in range(NbinsZ)]
        for b in bins:
            max_temp = max( max_temp, hist.GetBinContent(*b) )
            min_temp = min( min_temp, hist.GetBinContent(*b) )
    return (max_temp, min_temp)

def getMaxMinPositive(*arguments, **keywords):
    """Get true maximum/minimum of provided histograms by iterating over non-zero and positive bins."""
    hist = arguments[0]
    max_temp = hist.GetBinContent(1)
    min_temp = hist.GetBinContent(hist.GetMaximumBin())
    for hist in arguments:
        NbinsX = hist.GetNbinsX()
        NbinsY = hist.GetNbinsY()
        NbinsZ = hist.GetNbinsZ()
        bins = [(x+1, y+1, z+1) for x in range(NbinsX) for y in range(NbinsY) for z in range(NbinsZ)]
        for b in bins:
            binContent = hist.GetBinContent(*b)
            max_temp = max( max_temp, binContent )
            if binContent > 0: min_temp = min( min_temp, binContent )
    return (max_temp, min_temp)

def printBinContents(hist):
    print "bin\tcontent"
    for i in xrange(hist.GetNbinsX()+2):
        print "%d\t%f" % (i, hist.GetBinContent(i))

def printBinContentsWithLabels(hist):
    print "bin\tcontent"
    for i in xrange(hist.GetNbinsX()+2):
        binlabel = hist.GetXaxis().GetBinLabel(i)
        print "%d\t%f\t%s" % (i, hist.GetBinContent(i), binlabel)


def plotRatios(numerators, denominator,
        title="", xTitle="", yTitle="", yTitle2="S/B",
        minMargin=0.5, maxMargin=1.5,
        ratioMinMargin=0.5, ratioMaxMargin=1.5,
        topPlotLogY=False, bottomPlotLogY=False):
    # //*************************************************
    # // Variables
    # topPlotLogY = False;      # // 0 = no log; 1= log
    # bottomPlotLogY = False;      # // 0 = no log; 1= log
    # yTitle2 = "S/B"; # // bottom plot y axis title

    histColors = [rt.kBlack, rt.kRed, rt.kBlue, rt.kGreen+2, rt.kMagenta][1:]

    histDenominatorColor = rt.kBlack;

    # // END of Variables
    # //*************************************************

    # Dictionaries to return
    canvases = {}
    histos = {}
    numeratorHistograms = []
    ratioHistograms = []

    # numerator   = histos[numeratorKey]
    # denominator = histos[denominatorKey]

    postfix = randomPostfix(3)
    canvasName = denominator.GetName() + '_ratio' + postfix
    c1 = rt.TCanvas(canvasName, canvasName, 0,0,1200,800);
    c1.Range(0,0,1,1);
    canvases[''] = c1

    denominatorHistogram = denominator.Clone()
    histos['denominator'] = denominatorHistogram
    for i, numerator in enumerate(numerators):
        numeratorHistograms.append( numerator.Clone() );
        histos['numerator'+str(i)] = numeratorHistograms[i]
        # // Create ratio histograms
        # ratioHistogram;
        ratioHistograms.append( numeratorHistograms[i].Clone() )
        ratioHistograms[i].Divide(denominatorHistogram)
        histos['ratio'+str(i)] = ratioHistograms[i]

    # //*************************************************
    # // Bottom plot
    c1_1 = rt.TPad(canvasName+"_bot", canvasName+"_bot",0.01,0.01,0.99,0.32);
    c1_1.Draw();
    c1_1.cd();
    c1_1.SetTopMargin(0.01);
    c1_1.SetBottomMargin(0.3);
    c1_1.SetRightMargin(0.1);
    c1_1.SetFillStyle(0);

    c1_1.SetLogy(bottomPlotLogY);
    canvases['bottom'] = c1_1


    first = True
    (ymax, ymin) = getMaxMin(*ratioHistograms)
    # print (ymax,ymin)
    ymin *= minMargin
    ymax *= maxMargin
    if bottomPlotLogY and ymin<=0:
        warnings.warn(__name__+"Attempting to set y-axis range below or equal to zero while in log scale. Setting y-axis min to 1e-5")
        ymin=1e-5
    for i, ratioHistogram in enumerate(ratioHistograms):
        if first : ratioHistogram.Draw("hist");
        else     : ratioHistogram.Draw("hist same");
        ratioHistogram.SetLineWidth(1);
        ratioHistogram.SetLineColor(histColors[i]);
        ratioHistogram.SetMarkerColor(histColors[i]);
        ratioHistogram.GetYaxis().SetNdivisions(5);
        ratioHistogram.SetTitle(";"+xTitle+";"+yTitle2);
        ratioHistogram.GetXaxis().SetTitleSize(0.14);
        ratioHistogram.GetXaxis().SetLabelSize(0.14);
        ratioHistogram.GetYaxis().SetLabelSize(0.11);
        ratioHistogram.GetYaxis().SetTitleSize(0.14);
        ratioHistogram.GetYaxis().SetTitleOffset(0.28);
        ratioHistogram.GetYaxis().SetRangeUser(ymin, ymax);
        first = False
    # // End bottom plot
    # //*************************************************


    # //*************************************************
    # // Top Plot
    c1.cd();
    c1_2 = rt.TPad(canvasName+"_top", canvasName+"_top",0.01,0.33,0.99,0.99);
    c1_2.Draw();
    c1_2.cd();
    c1_2.SetTopMargin(0.1);
    c1_2.SetBottomMargin(0.01);
    c1_2.SetRightMargin(0.1);
    c1_2.SetFillStyle(0);
    canvases['top'] = c1_2

    (ymax, ymin) = getMaxMin(denominatorHistogram, *numeratorHistograms)
    ymin *= minMargin
    ymax *= maxMargin
    if topPlotLogY and ymin<=0:
        warnings.warn(__name__+"Attempting to set y-axis range below or equal to zero while in log scale. Setting y-axis min to 1e-5")
        ymin=1e-5
    denominatorHistogram.SetLineWidth(2);
    denominatorHistogram.SetLineColor(histDenominatorColor);
    denominatorHistogram.SetMarkerColor(histDenominatorColor);
    denominatorHistogram.SetLabelSize(0.0);
    denominatorHistogram.GetXaxis().SetTitleSize(0.00);
    denominatorHistogram.GetYaxis().SetLabelSize(0.07);
    denominatorHistogram.GetYaxis().SetTitleSize(0.08);
    denominatorHistogram.GetYaxis().SetTitleOffset(0.76);
    denominatorHistogram.SetTitle(title+";;"+yTitle);
    denominatorHistogram.GetYaxis().SetRangeUser(ymin, ymax);
    denominatorHistogram.Draw("hist");

    for i, numeratorHistogram in enumerate(numeratorHistograms):
        numeratorHistogram.SetLineWidth(2);
        numeratorHistogram.SetLineColor(histColors[i]);
        numeratorHistogram.SetMarkerColor(histColors[i]);
        numeratorHistogram.GetYaxis().SetRangeUser(ymin, ymax);
        numeratorHistogram.Draw("hist same");

    c1_2.SetLogy(topPlotLogY);
    # // End bottom plot
    # //*************************************************

    # rt.SetOwnership(c1, False)
    # rt.SetOwnership(c1_1, False)
    # rt.SetOwnership(c1_2, False)
    return (canvases, histos)

def plotRatiosGeneric(numerators, denominator, yAxisTitle="Data/MC", canvas=None, style=""):
    """Split current pad into two pads, plot ratio in bottom plot and return (ratios, toppad, bottompad).
    If style=\"d\", copy style from denominator"""
    if not hasattr(numerators, '__iter__'):
        # Probably only provided a single numerator
        numerators = [numerators]
    if not canvas:
        canvas = rt.gPad

    # Calculate scaling factor for histograms
    # canvas_width  = canvas.XtoPixel(canvas.GetX2());
    canvas_height = canvas.YtoPixel(canvas.GetY1());
    # charheight = textsize*canvas_width;
    # if (canvas_width < canvas_height) : charheight = textsize*canvas_width;
    # else                              : charheight = textsize*canvas_height;

    # yTitleSize = numerators[0].GetYaxis().GetTitleSize() * canvas_width
    # yTitleFont = numerators[0].GetYaxis().GetTitleFont() * canvas_width
    # yTitleOffset = numerators[0].GetYaxis().GetTitleOffset() * canvas_width
    # yLabelSize = numerators[0].GetYaxis().GetLabelSize() * canvas_width
    # yLabelFont = numerators[0].GetYaxis().GetLabelFont() * canvas_width
    # print yTitleSize , yTitleFont , yTitleOffset , yLabelSize , yLabelFont
    cname = canvas.GetName()
    ratios = []
    canvas.cd()
    toppad    = rt.TPad(cname+"_top", cname+"_bottom", 0, 0.3, 1, 1.0)
    toppad.SetLeftMargin(0.08)
    toppad.SetBottomMargin(0)
    toppad.SetLogy(canvas.GetLogy())
    toppad.SetLogx(canvas.GetLogx())
    toppad.Draw()
    toppad.cd()
    toppad_height = toppad.YtoPixel(toppad.GetY1());
    # axis = rt.TGaxis( -5, 20, -5, 220, 20,220,510,"");
    # axis.SetLabelFont(43); # Absolute font size in pixel (precision 3)
    # axis.SetLabelSize(15);
    # axis.Draw();
    canvas.cd()
    bottompad = rt.TPad(cname+"_top", cname+"_bottom", 0, 0.05, 1, 0.3)
    bottompad.SetLeftMargin(0.08)
    bottompad.SetTopMargin(0)
    bottompad.SetBottomMargin(0.30)
    bottompad.SetLogy(canvas.GetLogy())
    bottompad.SetLogx(canvas.GetLogx())
    bottompad.Draw()
    bottompad_height = bottompad.YtoPixel(bottompad.GetY1());
    for num in numerators:
        if style=="d": ratio = denominator.Clone()
        else: ratio = num.Clone()
        ratio.Divide(num, denominator, 1.0, 1.0, )
        ratios.append(ratio)
        # num.GetYaxis().SetLabelSize(0)
    ratio.GetYaxis().SetTitle(yAxisTitle+"  ");
    ratio.GetYaxis().SetNdivisions(505);
    scaleSize(ratio, float(toppad_height)/bottompad_height)
    return (ratios, toppad, bottompad)

################################################################################
# Simple 1D transformations
# Could be replaced with generic transform(hist_in, func)
# where func takes binContent, binError and returns (newBinContent, newBinError)
#
def resetBinContent(hist_in):
    hist = hist_in.Clone()
    for i in xrange(hist.GetNbinsX()+2):
        x = hist.GetBinContent(i)
        hist.SetBinContent(i, 0)
        hist.SetBinError(i, 0)
    hist.SetEntries(0)
    return hist

def squareRoot(hist_in):
    hist = hist_in.Clone()
    for i in xrange(hist.GetNbinsX()+2):
        x = hist.GetBinContent(i)
        hist.SetBinContent(i, math.sqrt(x))
    return hist

def complement(hist_in):
    hist = hist_in.Clone()
    for i in xrange(hist.GetNbinsX()+2):
        x = hist.GetBinContent(i)
        hist.SetBinContent(i, 1-x)
    return hist

#
################################################################################

################################################################################
# Simple 1D utils
def binWidthNormalize(hist_in):
    hist = resetBinContent( hist_in )
    for i in xrange(hist.GetNbinsX()+2):
        x = hist.GetBinContent(i)
        e = hist.GetBinError(i)
        b = hist.GetXaxis().GetBinWidth(i)
        hist.SetBinContent(i, x/b)
        hist.SetBinError(i, e/b)
    return hist

def addOverflowUnderflow(hist_in):
    """Returns a copy of the input histogram with extra bins for underflow/overflow.
    Modified from https://root.cern.ch/phpBB3/viewtopic.php?t=6764"""
    hist = resetBinContent( hist_in )
    nx   = hist_in.GetNbinsX();
    # Set xbins
    xbins= [];
    underflowbin_loweredge = hist_in.GetBinLowEdge(1) - hist_in.GetBinWidth(1)
    xbins.append(underflowbin_loweredge)
    # Run through bins 1 to nx+1
    for i in xrange(nx+1):
        xbins.append(hist_in.GetBinLowEdge(i+1))
    overflowbin_upperedge  = hist_in.GetBinLowEdge(nx+1) + hist_in.GetBinWidth(nx)
    xbins.append(overflowbin_upperedge)
    hist.SetBins(nx+2, array('d', xbins));
    # Fill the new histogram including the extra bin for overflows
    for i in xrange(nx+2):
        hist.SetBinContent(i+1, hist_in.GetBinContent(i));
        hist.SetBinError(i+1, hist_in.GetBinError(i));
    # Restore the number of entries
    hist.SetEntries(hist_in.GetEntries());
    return hist;

#
################################################################################

def createProfile(hist_in, func, name="", title=""):
    """Create 1D histogram val vs x from input 2D histogram, where val is computed from an x-slice of the input histogram.
    func must be a function that takes 1D histogram and returns (val, valError)
    """
    if not hasattr(func, '__call__'): raise TypeError('createProfile','func must be a function that takes 1D histogram and returns (val, valError)')
    hist = hist_in.ProjectionX("_px"+randomPostfix(5), 1, 0) # Hack to get empty 1d histogram with identical x-axis attributes as hist_in
    if name  : hist.SetName  ( name  )
    if title : hist.SetTitle ( title )
    nBinsX = hist.GetNbinsX()
    for i in xrange(-1, nBinsX+1+1):
        hi = hist_in.ProjectionY("_py_"+str(i), i, i, "e")
        (val, valError) = func(hi)
        hist.SetBinContent ( i, val      )
        hist.SetBinError   ( i, valError )
    return hist

def getCumulativeHistogram(hist, reverse=False, normalize=False):
    hist_temp= hist.Clone(hist.GetName()+'_cumulative')
    nbins = hist_temp.GetNbinsX()
    if not reverse:
        for i in range(nbins): hist_temp.SetBinContent(i+1, hist.Integral(1,i+1))
    else          :
        for i in range(nbins): hist_temp.SetBinContent(i+1, hist.Integral(1, nbins) - hist.Integral(1,i+1))
    if normalize: hist_temp.Scale(1/hist.Integral(1, nbins))
    return hist_temp

def makeGraphFromHistos(histX, histY, reverse=False):
    nBins = histX.GetNbinsX()
    nBinsY = histY.GetNbinsX()
    if nBins != nBinsY: print 'makeGraphFromHistos: Warning - Binning mismatch between X and Y histograms'
    graph = rt.TGraph(nBins)
    for i in range(nBins):
        if not reverse:
            graph.SetPoint(i+1,       histX.GetBinContent(i+1), histY.GetBinContent(i+1))
            print (i+1,       histX.GetBinContent(i+1), histY.GetBinContent(i+1))
        else:
            graph.SetPoint(nBins-i+1, histX.GetBinContent(i+1), histY.GetBinContent(i+1))
            print (nBins-i+1, histX.GetBinContent(i+1), histY.GetBinContent(i+1))
    return graph

def getAbsoluteValueHistogram(hist):
    """Takes a 1d histogram with 2n bins from -limit to +limit
    and produces a 1d histogram with n bins from 0. to +range,
    where the new x-axis corresponds to absolute value of the original x-axis.
    """
    nBin = hist.GetNbinsX()
    limit = hist.GetBinLowEdge(nBin+1)
    limit_neg = hist.GetBinLowEdge(1)
    if nBin % 2 != 0 or limit!=-limit_neg: print "getAbsoluteValueHistogram: Input histogram is not symmetric!"
    # if nBin % 2 != 0 or abs(limit+limit_neg)<0.1: print "getAbsoluteValueHistogram: Input histogram is not symmetric!"

    hist_temp = hist.Clone()
    hist_temp.Scale(0.)
    hist_temp.SetBins(nBin/2, 0., limit)
    for i in range(nBin/2):
        hist_temp.SetBinContent(i, hist.GetBinContent(nBin/2+i) + hist.GetBinContent(nBin/2-i))
    return hist_temp

def scaleSize(hist_in, factor):
    hist_in.GetYaxis().SetTitleSize   ( hist_in.GetYaxis().GetTitleSize  ()*factor );
    # hist_in.GetYaxis().SetTitleOffset ( hist_in.GetYaxis().GetTitleOffset()*factor );
    hist_in.GetYaxis().SetLabelSize   ( hist_in.GetYaxis().GetLabelSize  ()*factor );
    hist_in.GetXaxis().SetTitleSize   ( hist_in.GetXaxis().GetTitleSize  ()*factor );
    # hist_in.GetXaxis().SetTitleOffset ( hist_in.GetXaxis().GetTitleOffset()*factor );
    hist_in.GetXaxis().SetLabelSize   ( hist_in.GetXaxis().GetLabelSize  ()*factor );
