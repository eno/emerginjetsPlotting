"""Test all cuts on a signal model"""
from rootpy import ROOT as rt
from rootpy.io import root_open
signal_region_bin = 15

files = [
    '/mnt/hepcms/home/yhshin/data/condor_output/2017-01-16/histos-acc0-cut1/mass_X_d_400_mass_pi_d_10_tau_pi_d_1/histo-mass_X_d_400_mass_pi_d_10_tau_pi_d_1-mass_X_d_400_mass_pi_d_10_tau_pi_d_1-acc0-cut1-0.root',
    '/mnt/hepcms/home/yhshin/data/condor_output/2017-01-16/histos-acc0-cut2/mass_X_d_400_mass_pi_d_10_tau_pi_d_1/histo-mass_X_d_400_mass_pi_d_10_tau_pi_d_1-mass_X_d_400_mass_pi_d_10_tau_pi_d_1-acc0-cut2-0.root',
    '/mnt/hepcms/home/yhshin/data/condor_output/2017-01-16/histos-acc0-cut3/mass_X_d_400_mass_pi_d_10_tau_pi_d_1/histo-mass_X_d_400_mass_pi_d_10_tau_pi_d_1-mass_X_d_400_mass_pi_d_10_tau_pi_d_1-acc0-cut3-0.root',
    '/mnt/hepcms/home/yhshin/data/condor_output/2017-01-16/histos-acc0-cut4/mass_X_d_400_mass_pi_d_10_tau_pi_d_1/histo-mass_X_d_400_mass_pi_d_10_tau_pi_d_1-mass_X_d_400_mass_pi_d_10_tau_pi_d_1-acc0-cut4-0.root',
    '/mnt/hepcms/home/yhshin/data/condor_output/2017-01-16/histos-acc0-cut5/mass_X_d_400_mass_pi_d_10_tau_pi_d_1/histo-mass_X_d_400_mass_pi_d_10_tau_pi_d_1-mass_X_d_400_mass_pi_d_10_tau_pi_d_1-acc0-cut5-0.root',
    '/mnt/hepcms/home/yhshin/data/condor_output/2017-01-16/histos-acc0-cut6/mass_X_d_400_mass_pi_d_10_tau_pi_d_1/histo-mass_X_d_400_mass_pi_d_10_tau_pi_d_1-mass_X_d_400_mass_pi_d_10_tau_pi_d_1-acc0-cut6-0.root',
    '/mnt/hepcms/home/yhshin/data/condor_output/2017-01-16/histos-acc0-cut7/mass_X_d_400_mass_pi_d_10_tau_pi_d_1/histo-mass_X_d_400_mass_pi_d_10_tau_pi_d_1-mass_X_d_400_mass_pi_d_10_tau_pi_d_1-acc0-cut7-0.root',
    '/mnt/hepcms/home/yhshin/data/condor_output/2017-01-16/histos-acc0-cut8/mass_X_d_400_mass_pi_d_10_tau_pi_d_1/histo-mass_X_d_400_mass_pi_d_10_tau_pi_d_1-mass_X_d_400_mass_pi_d_10_tau_pi_d_1-acc0-cut8-0.root',
]

for (i, f) in enumerate(files):
    fileh = root_open(f)
    accept = fileh.cutflow.GetBinContent(signal_region_bin) / fileh.cutflow.GetBinContent(1)
    print i, accept

