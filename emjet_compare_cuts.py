import csv
import os
from collections import OrderedDict
from rootpy import ROOT as rt
from rootpy.io import root_open
from rootpy.plotting import Hist, Hist2D, Hist3D, HistStack, Legend, Canvas, Graph, Pad
from rootpy.plotting.utils import draw
import tdrstyle
tdrstyle.setTDRStyle()
rt.gStyle.SetPalette(1)
batch = 1 # SWITCH
if batch:
    # Turn off graphics
    rt.gROOT.SetBatch(True)
blinded = 1 # SWITCH
from rootutils import saveAllCanvases, updateAllCanvases, deleteAllCanvases
from math import sin
import colorsys

# limitfile = 'emjet_limits_20180131.csv'
# limitfile = 'emjet_limits_allcuts_20180205.csv'
# limitfile2 = 'emjet_limits_20180125.csv'
# limitfile2 = 'emjet_limits_20171220.csv'
# limitfile2 = 'emjet_limits_20180118.csv'
# limitfile = 'emjet_limits_allcuts_20180214.csv'
# limitfile = os.environ['LIMITFILE']
limitfile = 'emjet_limits_allcuts_20180215.csv'

# Read into list of OrderedDict
with open(limitfile) as csvfile:
    reader = csv.DictReader(csvfile)
    keys = reader.fieldnames
    r = csv.reader(csvfile)
    signal_list = [OrderedDict(zip(keys, row)) for row in r]

chosen_signal_names = [
    'mass_X_d_400_mass_pi_d_1_tau_pi_d_150',
    'mass_X_d_400_mass_pi_d_1_tau_pi_d_225',
    'mass_X_d_400_mass_pi_d_1_tau_pi_d_300',
    'mass_X_d_400_mass_pi_d_1_tau_pi_d_500',
    'mass_X_d_400_mass_pi_d_2_tau_pi_d_150',
    'mass_X_d_400_mass_pi_d_2_tau_pi_d_500',
    'mass_X_d_400_mass_pi_d_5_tau_pi_d_300',
    'mass_X_d_400_mass_pi_d_5_tau_pi_d_500',
    'mass_X_d_400_mass_pi_d_10_tau_pi_d_300',
    'mass_X_d_400_mass_pi_d_10_tau_pi_d_500',
    'mass_X_d_600_mass_pi_d_1_tau_pi_d_150',
    'mass_X_d_600_mass_pi_d_1_tau_pi_d_225',
    'mass_X_d_600_mass_pi_d_1_tau_pi_d_300',
    'mass_X_d_600_mass_pi_d_1_tau_pi_d_500',
    'mass_X_d_600_mass_pi_d_2_tau_pi_d_225',
    'mass_X_d_600_mass_pi_d_2_tau_pi_d_300',
    'mass_X_d_600_mass_pi_d_2_tau_pi_d_500',
    'mass_X_d_600_mass_pi_d_5_tau_pi_d_300',
    'mass_X_d_600_mass_pi_d_5_tau_pi_d_500',
    'mass_X_d_600_mass_pi_d_10_tau_pi_d_500',
    'mass_X_d_800_mass_pi_d_1_tau_pi_d_150',
    'mass_X_d_800_mass_pi_d_1_tau_pi_d_225',
    'mass_X_d_800_mass_pi_d_1_tau_pi_d_300',
    'mass_X_d_800_mass_pi_d_1_tau_pi_d_500',
    'mass_X_d_800_mass_pi_d_2_tau_pi_d_225',
    'mass_X_d_800_mass_pi_d_2_tau_pi_d_300',
    'mass_X_d_800_mass_pi_d_2_tau_pi_d_500',
    'mass_X_d_800_mass_pi_d_5_tau_pi_d_300',
    'mass_X_d_800_mass_pi_d_5_tau_pi_d_500',
    'mass_X_d_800_mass_pi_d_10_tau_pi_d_500',
    'mass_X_d_1000_mass_pi_d_1_tau_pi_d_150',
    'mass_X_d_1000_mass_pi_d_1_tau_pi_d_225',
    'mass_X_d_1000_mass_pi_d_1_tau_pi_d_300',
    'mass_X_d_1000_mass_pi_d_1_tau_pi_d_500',
    'mass_X_d_1000_mass_pi_d_2_tau_pi_d_225',
    'mass_X_d_1000_mass_pi_d_2_tau_pi_d_300',
    'mass_X_d_1000_mass_pi_d_2_tau_pi_d_500',
    'mass_X_d_1000_mass_pi_d_5_tau_pi_d_300',
    'mass_X_d_1000_mass_pi_d_5_tau_pi_d_500',
    'mass_X_d_1000_mass_pi_d_10_tau_pi_d_500',
    'mass_X_d_1250_mass_pi_d_1_tau_pi_d_150',
    'mass_X_d_1250_mass_pi_d_1_tau_pi_d_225',
    'mass_X_d_1250_mass_pi_d_1_tau_pi_d_300',
    'mass_X_d_1250_mass_pi_d_1_tau_pi_d_500',
    'mass_X_d_1250_mass_pi_d_2_tau_pi_d_225',
    'mass_X_d_1250_mass_pi_d_2_tau_pi_d_300',
    'mass_X_d_1250_mass_pi_d_2_tau_pi_d_500',
    'mass_X_d_1250_mass_pi_d_5_tau_pi_d_300',
    'mass_X_d_1250_mass_pi_d_5_tau_pi_d_500',
    'mass_X_d_1250_mass_pi_d_10_tau_pi_d_500',
]

chosen_signal_dict_cut3 = OrderedDict()
chosen_signal_dict_cut4 = OrderedDict()
for s in signal_list:
    if s['name'] in chosen_signal_names:
        if s['cutname'] == 'cut3': chosen_signal_dict_cut3.append(s)
        if s['cutname'] == 'cut4': chosen_signal_dict_cut4.append(s)

for name in chosen_signal_names:
    pass


########################################################################
# Do something
########################################################################
# output_master_signals_file = os.environ['SIGFILE_BEST']
# with open(output_master_signals_file, 'w') as csvfile:
#     writer = csv.DictWriter(csvfile, quotechar='"', fieldnames=master_signal_list_filtered[0].keys())
#     writer.writeheader()
#     for m in master_signal_list_filtered:
#         writer.writerow(m)


