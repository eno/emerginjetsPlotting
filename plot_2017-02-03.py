from rootpy import ROOT as rt
from rootpy.io import root_open
from rootpy.plotting import Canvas
from rootpy.plotting.utils import draw
from collections import OrderedDict
from rootutils import saveAllCanvases, updateAllCanvases, deleteAllCanvases
import time

import CMS_lumi, tdrstyle
#set the tdr style
tdrstyle.setTDRStyle()

#change the CMS_lumi variables (see CMS_lumi.py)
CMS_lumi.lumi_7TeV = "4.8 fb^{-1}"
CMS_lumi.lumi_8TeV = "18.3 fb^{-1}"
CMS_lumi.writeExtraText = 1
CMS_lumi.extraText = "Preliminary"
CMS_lumi.lumi_sqrtS = "13 TeV" # used with iPeriod = 0, e.g. for simulation-only plots (default is an empty string)

# iPos = 11 # "CMS Preliminary" inside border
iPos = 0 # "CMS Preliminary" above border
if( iPos==0 ): CMS_lumi.relPosX = 0.08

H_ref =  600;
W_ref =  800;
# H_ref =  800;
# W_ref = 1200;
W = W_ref
H  = H_ref

#
# Simple example of macro: plot with CMS name and lumi text
#  (this script does not pretend to work in all configurations)
# iPeriod = 1*(0/1 7 TeV) + 2*(0/1 8 TeV)  + 4*(0/1 13 TeV)
# For instance:
#               iPeriod = 3 means: 7 TeV + 8 TeV
#               iPeriod = 7 means: 7 TeV + 8 TeV + 13 TeV
#               iPeriod = 0 means: free form (uses lumi_sqrtS)
# Initiated by: Gautier Hamel de Monchenault (Saclay)
# Translated in Python by: Joshua Hardenbrook (Princeton)
# Updated by:   Dinko Ferencek (Rutgers)
#

iPeriod = 0

# references for T, B, L, R
T = 0.08*H_ref
B = 0.12*H_ref
L = 0.12*W_ref
R = 0.04*W_ref


def defaultCanvas(cname=""):
    canvas = rt.TCanvas(cname, cname, W_ref, H_ref)
    canvas.SetLeftMargin(0.16)
    canvas.SetRightMargin(0.08)
    canvas.cd()
    return canvas

samples = ['WJetData', 'WJetMC', 'QCD']

# Open files
files = OrderedDict()
files['WJetData'] = root_open('~/www/2017-02-03/histo-WJetSkimMuon-v1.root', 'read')
files['WJetMC']   = root_open('~/www/2017-02-03/histo-WJetMC-v1.root', 'read')
files['QCD']      = root_open('~/www/2017-02-03/histo-QCD-v1.root', 'read')
# files['ModelA']      = root_open('~/www/2017-02-03/histo-ModelA-v1.root', 'read')

# Descriptive label
label = OrderedDict()
label['WJetData'] = 'W+Jet Data'
label['WJetMC']   = 'W+Jet MC'
label['QCD']      = 'QCD MC'

# Color histograms
colors = OrderedDict()
colors['WJetData'] = 'blue'
colors['WJetMC']   = 'red'
colors['QCD']      = 'black'
for s in samples:
    for o in list(files[s].objects()):
        o.color = colors[s]
        # print o.GetName()

# Create dummy decorator objects for each sample
# All objects will copy decorators from this object
decos = OrderedDict()
s = 'WJetData'; h = rt.Hist(1,0,1, title=label[s]); h.color = colors[s]; h.legendstyle = 'l'; decos[s] = h;
s = 'WJetMC'  ; h = rt.Hist(1,0,1, title=label[s]); h.color = colors[s]; h.legendstyle = 'l'; decos[s] = h;
s = 'QCD'     ; h = rt.Hist(1,0,1, title=label[s]); h.color = colors[s]; h.legendstyle = 'l'; decos[s] = h;

# Make legend that can be copied
x1_l = 0.92
y1_l = 0.60
dx_l = 0.30
dy_l = 0.18
x0_l = x1_l-dx_l
y0_l = y1_l-dy_l
defaultCanvas()
legend =  rt.Legend(decos.values())
legend.SetBorderSize(0)



histos = OrderedDict()
for s in samples:
    histo_list = list(files[s].objects())
    histo_dict={}
    for h in histo_list:
        h.color = colors[s]
        h.GetXaxis().SetTitleOffset(1.0)
        h.GetYaxis().SetTitleOffset(1.0)
        h.GetXaxis().SetNoExponent()
        histo_dict[h.GetName()] = h
        # histo_dict[hist.GetName()] = addOverflowUnderflow(hist)
    histos[s] = histo_dict

# Rebin some histograms
switch_rebin_jetpt = 1
if switch_rebin_jetpt:
    # mbins = [(41,100)]
    mbins = [(31,35),(36,40),(41,60),(61,80),(81,100)]
    for s in samples:
        if s == 'WJetData' or s == 'WJetMC':
            for hname in ['jet_pt__JTegammacut', 'jet_pt__JTemerging', 'jet_pt__JTipcut']:
                h = histos[s][hname].merge_bins(mbins)
                h.decorate(**histos[s][hname].decorators)
                histos[s][hname] = h

############################################################
# Plots
############################################################
if 0:
    canvases = OrderedDict()
    cname = 'fakerate_alphaMaxCut_vs_pt'
    canvases[cname] = defaultCanvas(cname)
    fakerate_alphaMax = OrderedDict()
    for s in samples:
        h = histos[s]['jet_pt__JTemerging']
        h.Divide( histos[s]['jet_pt__JTegammacut'] )
        fakerate_alphaMax[s] = h
    draw(fakerate_alphaMax.values(), xtitle='Jet p_{T} [GeV]', ytitle='Fake rate (#alpha_{max} < 0.04)', ylimits=[-0.005, 0.02])
    CMS_lumi.CMS_lumi(canvases[cname], iPeriod, 0)
    legend.Clone().Draw()

if 0:
    canvases = OrderedDict()
    cname = 'fakerate_maxIpCut_vs_pt'
    canvases[cname] = defaultCanvas(cname)
    fakerate_alphaMax = OrderedDict()
    for s in samples:
        h = histos[s]['jet_pt__JTipcut']
        h.Divide( histos[s]['jet_pt__JTegammacut'] )
        fakerate_alphaMax[s] = h
    draw(fakerate_alphaMax.values(), xtitle='Jet p_{T} [GeV]', ytitle='Fake rate (#alpha_{max} < 0.04, IP^{2D}_{max} > 0.4 cm)', ylimits=[-0.005, 0.02])
    CMS_lumi.CMS_lumi(canvases[cname], iPeriod, 0)
    legend.Clone().Draw()

# for c in canvases.values():
#     # c.Update()
#     c.cd()
#     c.Paint()
#     c.ExecuteEvent(rt.kButton1Down, 500, 100)
#     c.Modified()
#     c.ForceUpdate()
#     c.ExecuteEvent(rt.kButton1Up, 500, 101)
#     # canvas.ExecuteEventAxis(rt.kButton1Up, 500, 100)

# raw_input("Press Enter to end")



