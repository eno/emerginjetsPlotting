"""Plot track modeling systematic"""
import argparse
parser = argparse.ArgumentParser()
# parser.add_argument('-f', '--force', action='store_true')
# parser.add_argument('sampleset', type=int)
args = parser.parse_args()
# sampleset = args.sampleset

from rootpy import ROOT as rt
from rootpy.plotting import Hist, Hist2D, Hist3D, HistStack, Legend, Canvas
from rootpy.tree import Tree

import os, os.path

import csv

from pdfsystematic import calculate_pdf_uncertainty
from emjet_helpers import sample_name_to_parameters # Returns (mass_X_d, mass_pi_d, tau_pi_d)
from emjet_helpers import signal_parameter_hist

# mainpath = '/mnt/hepcms/home/yhshin/data/condor_output/2017-11-04/histos-test/'
# mainpath = '/mnt/hepcms/home/yhshin/data/condor_output/2017-11-07/histos-pdftest0/'
mainpath = '/mnt/hepcms/home/yhshin/data/condor_output/2017-11-08/histos-pdftest1/'

samplelist = [x for x in os.listdir(mainpath) if os.path.isdir(x)]
tfile = rt.TFile("pdf_systematics.root", "RECREATE")
hist_up   = rt.TH1F("shift_up", "shift_up", 100, -1., 1.)
hist_down = rt.TH1F("shift_down", "shift_down", 100, -1., 1.)
hist3d_pdf_up = signal_parameter_hist(); hist3d_pdf_up.SetName("pdf_up"); hist3d_pdf_up.SetTitle("pdf_up")
hist3d_pdf_down = signal_parameter_hist(); hist3d_pdf_down.SetName("pdf_down"); hist3d_pdf_down.SetTitle("pdf_down")
# hist3d_pdf_up = Hist3D(25, 0, 25, 25, 0, 25, 25, 0, 25, name='pdf_up')
# hist3d_pdf_down = Hist3D(25, 0, 25, 25, 0, 25, 25, 0, 25, name='pdf_down')
tree = Tree("test")
tree.create_branches(
    {
        'mass_X_d'       : 'I',
        'mass_pi_d'      : 'I',
        'tau_pi_d'       : 'F',
        'pdf_shift_up'   : 'F',
        'pdf_shift_down' : 'F',
    }
)

pdf_uncs = []
for root, dirs, files in os.walk(mainpath, topdown=False):
    if root == mainpath: # Only check one level
        for dirname in dirs: # For each directory in mainpath
            dircontents = os.listdir( os.path.join(root, dirname) )
            if 'mass_X_d' in dirname:
                (shift_up, shift_down) = calculate_pdf_uncertainty( os.path.join(root, dirname, dircontents[0]) ) # Calculate PDF uncertainty from first file
                (mass_X_d, mass_pi_d, tau_pi_d) = sample_name_to_parameters(dirname)
                print mass_X_d, mass_pi_d, tau_pi_d, shift_up, shift_down
                pdf_uncs.append( (mass_X_d, mass_pi_d, tau_pi_d, shift_up, shift_down) )
                hist_up.Fill(shift_up)
                hist_down.Fill(shift_down)
                hist3d_pdf_up.Fill("%g" % tau_pi_d, "%g" % mass_pi_d, "%g" % mass_X_d, shift_up)
                hist3d_pdf_down.Fill("%g" % tau_pi_d, "%g" % mass_pi_d, "%g" % mass_X_d, shift_down)
                tree.mass_X_d       = mass_X_d
                tree.mass_pi_d      = mass_pi_d
                tree.tau_pi_d       = tau_pi_d
                tree.pdf_shift_up   = shift_up
                tree.pdf_shift_down = shift_down
                tree.Fill()

pdf_uncs.sort()

tfile.cd()
hist_up.Write()
hist_down.Write()
hist3d_pdf_up.Write()
hist3d_pdf_down.Write()
tree.Write()
# hist3d_pdf_up.Write()
# print pdf_uncs

