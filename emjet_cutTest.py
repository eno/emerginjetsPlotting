"""Tabulate shifts to ipXY and 3dSig used in MC modeling uncertainty"""
import argparse
parser = argparse.ArgumentParser()
# parser.add_argument('-f', '--force', action='store_true')
# parser.add_argument('sampleset', type=int)
args = parser.parse_args()
# sampleset = args.sampleset

from rootpy import ROOT as rt
from rootpy.io import root_open

import os, os.path
import subprocess
import csv

from collections import OrderedDict

from emjet_helpers import sample_name_to_parameters # Returns (mass_X_d, mass_pi_d, tau_pi_d)
from emjet_helpers import signal_parameter_hist
from process_signal_model import get_pdf_uncertainty
from process_signal_model import get_modeling_uncertainty
from process_signal_model import get_modeling_shifts
from process_signal_model import get_cutflow

searchpath = '/mnt/hepcms/home/yhshin/data/condor_output/2017-12-08/cuttest/histos-cuttest0*'
filelist = subprocess.check_output("find %s -name '*.root'" % searchpath, shell=True).split()

signallist = []
for filepath in filelist:
    print 'Processing %s' % (filepath)
    s = OrderedDict()
    # Get modeling shifts
    fileh = root_open(filepath, 'read')
    (cutname, sys_ipXyShift, sys_3dSigShift) = get_modeling_shifts(fileh)
    s['cutname' ]    = cutname
    s['ipXyShift' ]  = sys_ipXyShift
    s['3dSigShift' ] = sys_3dSigShift
    signallist.append( s )

# Write results to CSV file
with open('emjet_cutTest.txt', 'w') as csvfile:
    fieldnames = signallist[0].keys()
    writer = csv.DictWriter(csvfile, quotechar='"', fieldnames=fieldnames)
    writer.writeheader()
    for s in signallist:
        writer.writerow(s)

