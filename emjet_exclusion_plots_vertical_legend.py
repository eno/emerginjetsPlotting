import os
import csv
import array
from collections import OrderedDict
from rootpy import ROOT as rt
from rootpy.io import root_open
from rootpy.plotting import Hist, Hist2D, Hist3D, HistStack, Legend, Canvas, Graph, Graph2D, Pad
from rootpy.plotting.utils import draw
import tdrstyle
tdrstyle.setTDRStyle()
rt.gStyle.SetPalette(1)
batch = 0 # SWITCH
if batch:
    # Turn off graphics
    rt.gROOT.SetBatch(True)
savePng = 1 # Save png format?
savePdf = 1 # Save pdf format?
blinded = 1 # SWITCH
from rootutils import saveAllCanvases, updateAllCanvases, deleteAllCanvases
from histoutils import randomPostfix, transformHistogram
from math import sin, log10
import colorsys

limitfile = os.environ['LIMITFILE_BEST']
postfix = ''
dir = '2018-06-14'
if dir:
    if not os.path.isdir(dir):
        os.mkdir(dir)
    prefix = dir + '/'
else:
    prefix = ''

import emjet_helpers as helper
# helper.outer.parameters['mass_X_d'].remove(1250)
# helper.outer.parameters['mass_X_d'].remove(1500)
# helper.outer.parameters['mass_X_d'].remove(2000)
helper.outer.parameters['tau_pi_d'].remove(0.001)
helper.outer.parameters['tau_pi_d'].remove(0.1)
# helper.outer.parameters['tau_pi_d'].remove(1)
# helper.outer.parameters['tau_pi_d'].remove(2)
# helper.outer.parameters['tau_pi_d'].remove(500)
helper.update_signal_parameter_dict()

def getExpected(s):
    return float(s['sigma0'])

def getSigma1Up(s):
    return float(s['sigma1'])

def getSigma1Down(s):
    return float(s['sigma-1'])

def getSigma2Up(s):
    return float(s['sigma2'])

def getSigma2Down(s):
    return float(s['sigma-2'])

def getObserved(s):
    return float(s['observed'])

def getObservedError(s):
    return float(s['observed_err'])

def getTheoryXsec(s):
    return float(s['xsec_fb'])

pad_x_split = 0.8
pad_ratio = pad_x_split / (1 - pad_x_split)

yaxis_maximum = {
    400  : 120000 ,
    600  : 120000 ,
    800  : 120000 ,
    1000 : 120000 ,
    1250 : 120000 ,
    1500 : 120000 ,
    2000 : 120000 ,
}
yaxis_minimum = {
    400  : 0.1 ,
    600  : 0.1 ,
    800  : 0.1 ,
    1000 : 0.1 ,
    1250 : 0.1 ,
    1500 : 0.1 ,
    2000 : 0.1 ,
    # 400  : 10  ,
    # 600  : 1   ,
    # 800  : 1   ,
    # 1000 : 0.1 ,
    # 1250 : 0.1 ,
}
xsec_fb = {
    400   : 5506.11   ,
    600   : 523.797   ,
    800   : 85.0014   ,
    1000  : 18.45402  ,
    1250  : 3.47490   ,
    1500  : 0.768744  ,
    2000  : 0.0487065 ,
}

# leftMargin = 0.12
leftMargin = 0.14
# rightMargin = 0.16
rightMargin = 0.15

def defaultCanvas(cname=""):
    # canvas = Canvas(name=cname, title=cname, width=900, height=600)
    canvas = Canvas(name=cname, title=cname, width=1200, height=1200)
    # canvas.SetTopMargin(0.08)
    # canvas.SetLeftMargin(leftMargin)
    # canvas.SetRightMargin(rightMargin)
    # canvas.SetBottomMargin(0.16)
    canvas.SetTopMargin(0.0)
    canvas.SetLeftMargin(0.0)
    canvas.SetRightMargin(0.0)
    # canvas.SetLogy()
    canvas.cd()
    return canvas

def draw_cms_header(canvas=None, prelim=True):
    t = canvas.GetTopMargin()
    l = canvas.GetLeftMargin()
    r = canvas.GetRightMargin()
    if canvas: canvas.cd()
    latex = rt.TLatex()
    latex.SetTextAlign(11) # Left bottom aligned
    if prelim: latex.DrawLatexNDC(l, (1-t)+0.2*t, '#scale[1.2]{#font[62]{CMS}} #font[52]{#scale[1.0]{Preliminary}}')
    else     : latex.DrawLatexNDC(l, (1-t)+0.2*t, '#scale[1.2]{#font[62]{CMS}}')
    latex.SetTextAlign(31) # Right bottom aligned
    latex.DrawLatexNDC(1-r, (1-t)+0.2*t, '#font[42]{16.1 fb^{-1} (13 TeV)}')

def buildColorPalette(number):
    """Build color palette by dividing the color spectrum into the required number of colors"""
    colors = []
    for i in xrange(number):
        rgb_tuple = colorsys.hls_to_rgb(i/float(number), 0.5, 1.0)
        scaled_rgb_tuple = tuple([256*x for x in rgb_tuple])
        colors.append(scaled_rgb_tuple)
    return colors


# Generated from paletton.com
# *** Primary color:
colors = [None]*20
i = 0
colors [i*5+0] = (127, 42,104)
colors [i*5+1] = (190,127,173)
colors [i*5+2] = (159, 79,137)
colors [i*5+3] = ( 95, 16, 73)
colors [i*5+4] = ( 63,  0, 46)

# *** Secondary color (1):
i += 1
colors [i*5+0] = (161, 54, 70)
colors [i*5+1] = (241,161,173)
colors [i*5+2] = (201,100,116)
colors [i*5+3] = (120, 20, 36)
colors [i*5+4] = ( 80,  0, 13)

# *** Secondary color (2):
i += 1
colors [i*5+0] = ( 82, 43,114)
colors [i*5+1] = (147,118,172)
colors [i*5+2] = (112, 76,143)
colors [i*5+3] = ( 55, 19, 86)
colors [i*5+4] = ( 33,  4, 57)

# *** Complement color:
i += 1
colors [i*5+0] = ( 61, 49,117)
colors [i*5+1] = (134,125,176)
colors [i*5+2] = ( 94, 82,147)
colors [i*5+3] = ( 35, 24, 88)
colors [i*5+4] = ( 17,  7, 59)

# Read into list of OrderedDict
with open(limitfile) as csvfile:
    reader = csv.DictReader(csvfile)
    keys = reader.fieldnames
    r = csv.reader(csvfile)
    signal_list = [OrderedDict(zip(keys, row)) for row in r]
    signal_list.sort(key=lambda k: (float(k['mass_X_d']), float(k['mass_pi_d']), float(k['tau_pi_d'],)))

writeToFile = 0
out_csvfile = 'test_limits.csv'
if writeToFile:
    with open(out_csvfile, 'w') as csvfile:
        fieldnames = signal_list[0].keys()
        writer = csv.DictWriter(csvfile, quotechar='"', fieldnames=fieldnames)
        writer.writeheader()
        for s in signal_list:
            writer.writerow(s)

# Testing
if 0: #SWITCH
    hist = Graph(6)
    hist2 = Graph(6)
    for i in range(6):
        hist.SetPoint(i, i, sin(i*3.14/6))
        hist.SetPointError(i, 0., 0., 0.5* sin(i*3.14/6), 0.5* sin(i*3.14/6))
        hist.color = rt.kOrange
        hist.fillstyle = 1001
        # hist.drawstyle = 'e3'
        hist2.SetPoint(i, i, sin(i*3.14/6))
        hist2.SetPointError(i, 0., 0., 0.1* sin(i*3.14/6), 0.1* sin(i*3.14/6))
        hist2.color = rt.kGreen+1
        hist2.fillstyle = 1001
        # hist2.drawstyle = 'e3'
    # hist.Draw("ACE3")
    # hist2.Draw("sameCE3")

########################################################################
# Make 2D graphs
########################################################################
# Define list of parameters
mass_X_d_list = list( set([float(s['mass_X_d']) for s in signal_list]) )
mass_X_d_list.sort()
mass_pi_d_list = list( set([float(s['mass_pi_d']) for s in signal_list]) )
mass_pi_d_list.sort()
tau_pi_d_list = list( set([float(s['tau_pi_d']) for s in signal_list]) )
tau_pi_d_list.sort()
# mass_X_d_list = [1000]
# mass_pi_d_list = [1]
canvases = OrderedDict()
leftpads = OrderedDict()
rightpads = OrderedDict()
graphs = OrderedDict()

def createGraph(signal_list_in, func=getExpected):
    """Create TGraph2D from input list of signals.
    x: mass_X_d
    y: log10(tau_pi_d)
    z: func(s)"""
    graph = rt.TGraph2D(1)
    postfix = randomPostfix(3)
    name = func.__name__ + postfix
    graph.SetName(name)
    for itau, s in enumerate(signal_list_in):
        mass_X_d  = float(s['mass_X_d'])
        tau_pi_d  = float(s['tau_pi_d'])
        value = func(s)
        graph.SetPoint(itau, mass_X_d, log10(tau_pi_d), (value))
    return graph

def findExclusionCurve(graph_in, cutoff=1):
    """Find contour of input TGraph2D at z=cutoff.
    Return list of TGraph2D objects"""
    isBatch = rt.gROOT.IsBatch()
    rt.gROOT.SetBatch(True)
    canvas = defaultCanvas("contours")
    hist = graph_in.GetHistogram()
    contour_values = array.array('d', [cutoff, 10])
    hist.SetContour(2, contour_values)
    hist.Draw("cont list")
    rt.gPad.Update()
    contours = rt.gROOT.GetListOfSpecials().FindObject("contours")
    contour_list = contours.At(0)
    n_graphs = contour_list.GetSize()
    # if n_graphs!= 1: print graph_in.GetTitle()
    graphs_out = []
    for c in contour_list:
        clone = c.Clone()
        clone.SetLineWidth(3)
    graphs_out.append(clone)
    rt.gROOT.SetBatch(isBatch)
    canvas.IsA().Destructor(canvas)
    return graphs_out

# Create and clone contours
contoursExpected     = OrderedDict()
contoursSigma1Up     = OrderedDict()
contoursSigma1Down   = OrderedDict()
contoursSigma2Up     = OrderedDict()
contoursSigma2Down   = OrderedDict()
contoursObserved     = OrderedDict()
contoursObservedUp   = OrderedDict()
contoursObservedDown = OrderedDict()
histExpected         = OrderedDict()
yaxis                = OrderedDict()
for mass_pi_d in mass_pi_d_list:
    signal_list_filtered = [s for s in signal_list if float(s['mass_pi_d'])==mass_pi_d]
    # canvas = defaultCanvas("contours")
    # graphExpectedExclusion   = createGraph(signal_list_filtered, lambda s: getExpected (s) / getTheoryXsec(s) )
    graphExpectedExclusion   = createGraph(signal_list_filtered, lambda s: log10( getTheoryXsec(s) / getExpected   (s) ) )
    graphSigma1UpExclusion   = createGraph(signal_list_filtered, lambda s: log10( getTheoryXsec(s) / getSigma1Up   (s) ) )
    graphSigma1DownExclusion = createGraph(signal_list_filtered, lambda s: log10( getTheoryXsec(s) / getSigma1Down (s) ) )
    graphSigma2UpExclusion   = createGraph(signal_list_filtered, lambda s: log10( getTheoryXsec(s) / getSigma2Up   (s) ) )
    graphSigma2DownExclusion = createGraph(signal_list_filtered, lambda s: log10( getTheoryXsec(s) / getSigma2Down (s) ) )
    graphObservedExclusion   = createGraph(signal_list_filtered, lambda s: log10( getTheoryXsec(s) / getObserved   (s) ) )
    graphObservedUpExclusion   = createGraph(signal_list_filtered, lambda s: log10( getTheoryXsec(s) / ( getObserved(s) + getObservedError(s) ) ) )
    graphObservedDownExclusion = createGraph(signal_list_filtered, lambda s: log10( getTheoryXsec(s) / ( getObserved(s) - getObservedError(s) ) ) )
    contoursExpected   [mass_pi_d] = findExclusionCurve(graphExpectedExclusion   , cutoff=1e-10)
    contoursSigma1Up   [mass_pi_d] = findExclusionCurve(graphSigma1UpExclusion   , cutoff=1e-10)
    contoursSigma1Down [mass_pi_d] = findExclusionCurve(graphSigma1DownExclusion , cutoff=1e-10)
    contoursSigma2Up   [mass_pi_d] = findExclusionCurve(graphSigma2UpExclusion   , cutoff=1e-10)
    contoursSigma2Down [mass_pi_d] = findExclusionCurve(graphSigma2DownExclusion , cutoff=1e-10)
    contoursObserved   [mass_pi_d] = findExclusionCurve(graphObservedExclusion   , cutoff=1e-10)


# Draw plots
for mass_pi_d in mass_pi_d_list:
    # continue
    signal_list_filtered = [s for s in signal_list if float(s['mass_pi_d'])==mass_pi_d]
    name = 'limit_exclusion_mass_pi_d_%g' % (mass_pi_d)
    canvases[name] = defaultCanvas(name)
    # leftpads[name] = Pad(0, 0, pad_x_split, 1)
    leftpads[name] = Pad(0, 0, 1, pad_x_split)
    # leftpads[name].SetTopMargin(0.08)
    # leftpads[name].SetLeftMargin(leftMargin)
    # leftpads[name].SetRightMargin(rightMargin)
    # leftpads[name].SetBottomMargin(0.16)
    leftpads[name].SetTopMargin(0.08)
    leftpads[name].SetLeftMargin(leftMargin)
    leftpads[name].SetRightMargin(rightMargin)
    leftpads[name].SetBottomMargin(0.16)
    leftpads[name].SetLogz()
    rightpads[name] = Pad(0, pad_x_split, 1, 1)
    rightpads[name].SetTopMargin(0.08)
    rightpads[name].SetLeftMargin(leftMargin)
    rightpads[name].SetRightMargin(rightMargin)
    rightpads[name].SetBottomMargin(0.01)
    # rightpads[name].SetLeftMargin(0.12)
    leftpads[name].Draw()
    rightpads[name].Draw()
    leftpads[name].cd()
    isBatch = rt.gROOT.IsBatch()
    rt.gROOT.SetBatch(True)
    # graphExpected   = createGraph(signal_list_filtered, getExpected )
    # graphExpected   .Draw("colz")
    # hist = graphExpected   .GetHistogram()
    graphLogExpected   = createGraph(signal_list_filtered, lambda s: log10( getExpected (s) ) )
    # graphLogObserved   = createGraph(signal_list_filtered, lambda s: log10( getObserved (s) ) )
    # graphLogExpected   .Draw("colz")
    hist = graphLogExpected.GetHistogram().Clone()
    histExpected [mass_pi_d] = transformHistogram( hist, lambda z: pow(10, z) )
    # histExpected [mass_pi_d] = hist
    rt.gROOT.SetBatch(isBatch)
    histExpected [mass_pi_d].Draw("colz")
    # graphExpected.Draw("colz")
    # graphLogExpected.Draw("col same")
    histExpected [mass_pi_d].GetXaxis().SetLabelOffset(0.016)
    histExpected [mass_pi_d].GetXaxis().SetLabelSize(0.04);
    histExpected [mass_pi_d].GetXaxis().SetTitleOffset(1.00)
    histExpected [mass_pi_d].GetXaxis().SetTitle("m_{X_{d}}  [GeV]")
    histExpected [mass_pi_d].GetYaxis().SetLabelSize(0.0)
    histExpected [mass_pi_d].GetYaxis().SetTickSize(0.0)
    histExpected [mass_pi_d].GetYaxis().SetTitleOffset(0.95)
    histExpected [mass_pi_d].GetYaxis().SetTitle("c #tau_{#pi_{d}}  [mm]")
    histExpected [mass_pi_d].GetZaxis().SetLabelSize(0.04)
    histExpected [mass_pi_d].GetZaxis().SetTitleSize(0.05)
    histExpected [mass_pi_d].GetZaxis().SetTitle("95% C.L. upper limit on cross section  [fb]")
    histExpected [mass_pi_d].GetZaxis().SetRangeUser(5e-1, 3e3)
    for c in contoursExpected   [mass_pi_d] : c.SetLineColor(rt.kRed); c.SetLineStyle(1); c.Draw("c same")
    for c in contoursSigma1Up   [mass_pi_d] : c.SetLineColor(rt.kRed); c.SetLineStyle(2); c.Draw("c same")
    for c in contoursSigma1Down [mass_pi_d] : c.SetLineColor(rt.kRed); c.SetLineStyle(2); c.Draw("c same")
    for c in contoursObserved   [mass_pi_d] : c.SetLineColor(rt.kBlack); c.SetLineStyle(1); c.Draw("c same")
    yaxis[mass_pi_d] = rt.TGaxis(400, 0, 400, 3, 1, 1000, 50510, "GW");
    yaxis[mass_pi_d].SetName("axis");
    yaxis[mass_pi_d].SetLabelFont(42);
    yaxis[mass_pi_d].SetLabelSize(0.04);
    yaxis[mass_pi_d].SetLabelOffset(0.020)
    yaxis[mass_pi_d].SetGridLength(1.00-leftMargin-rightMargin);
    yaxis[mass_pi_d].Draw();
    # Draw legend in right pad
    rightpads[name].cd()
    # legend = Legend(decorators, header='#font[42]{95% CL_{s} limits}',topmargin=0.15, leftmargin=0.0, rightmargin=0.0, textfont=42, textsize=0.03*pad_ratio)
    legend_expected = contoursExpected   [mass_pi_d][0].Clone(); legend_expected.SetTitle('Exp. limit'); legend_expected.legendstyle = 'l'
    legend_sigma1Up = contoursSigma1Up   [mass_pi_d][0].Clone(); legend_sigma1Up.SetTitle('Exp. #pm 1#sigma'); legend_sigma1Up.legendstyle = 'l'
    legend_observed = contoursObserved   [mass_pi_d][0].Clone(); legend_observed.SetTitle('Obs. limit'); legend_observed.legendstyle = 'l'
    legend_entries = [legend_expected, legend_sigma1Up, legend_observed]
    legend = Legend(legend_entries, header='#font[42]{m_{#pi_{d}} = %g GeV}' % mass_pi_d, topmargin=0.01, leftmargin=0.01, rightmargin=0.01, entrysep=0.20, textfont=42, textsize=0.18)
    legend.SetBorderSize(1)
    legend.SetFillStyle(1001)
    legend.SetLineWidth(4)
    legend.Draw()
    # Draw vertical legend in left pad
    leftpads[name].cd()
    t = leftpads[name].GetTopMargin()
    l = leftpads[name].GetLeftMargin()
    r = leftpads[name].GetRightMargin()
    legend_vertical = Legend(legend_entries, header='#font[42]{m_{#pi_{d}} = %g GeV}' % mass_pi_d, topmargin=0, leftmargin=0, rightmargin=0, entrysep=0.01, textfont=42, textsize=0.06)
    legend_vertical.SetBorderSize(1)
    legend_vertical.SetFillStyle(1001)
    legend_vertical.SetLineWidth(4)
    # legend_vertical.Draw()
    # latex = rt.TLatex()
    # latex.SetTextSize(0.03*pad_ratio)
    # latex.SetTextAlign(12) # Middle left align
    # latex.SetTextFont(42)
    # latex.DrawLatexNDC(0.05, 0.25,'m_{#pi_{d}} = %g GeV' % mass_pi_d)
    draw_cms_header(leftpads[name], prelim=False)
    rt.gPad.Update()
    # palette = histExpected [mass_pi_d].GetListOfFunctions().FindObject("palette");
    # palette.Paint()
    if batch:
        if savePng: saveAllCanvases(option="png", postfix=postfix, prefix=prefix)
        if savePdf: saveAllCanvases(option="pdf", postfix=postfix, prefix=prefix)
        deleteAllCanvases()

# Draw overlaid limit exclusion plot
if 0:
    name = "limit_exclusion_overlaid"
    canvases[name] = defaultCanvas(name)
    graphExpected   = createGraph(signal_list_filtered, getExpected )
    graphExpected.Draw("axis")
    graphExpected.GetYaxis().SetLabelSize(0.0)
    graphExpected.GetYaxis().SetTickSize(0.0)
    for mass_pi_d in [1, 10]:
        for c in contoursExpected   [mass_pi_d] : c.SetLineColor(rt.kRed); c.SetLineStyle(1); c.Draw("c same")
        for c in contoursSigma1Up   [mass_pi_d] : c.SetLineColor(rt.kRed); c.SetLineStyle(2); c.Draw("c same")
        for c in contoursSigma1Down [mass_pi_d] : c.SetLineColor(rt.kRed); c.SetLineStyle(2); c.Draw("c same")
    axis = rt.TGaxis(400, 0, 400, 3, 1, 1000, 50510, "GW");
    axis.SetName("axis");
    axis.SetLabelFont(42);
    axis.SetLabelSize(0.05);
    axis.SetGridLength(1.00-0.08-0.16);
    axis.Draw();

# Old testing code
# for mass_pi_d in mass_pi_d_list:
#     continue
#     signal_list_filtered = [s for s in signal_list if float(s['mass_pi_d'])==mass_pi_d]
#     name = 'mass_pi_d_%g' % (mass_pi_d)
#     canvases[name] = defaultCanvas(name)
#     # canvases[name].SetLogy()
#     canvases[name].SetLogz()
#     graph = rt.TGraph2D(1)
#     # graph.SetNpx(10000)
#     # graph.SetNpy(10000)
#     for itau, s in enumerate(signal_list_filtered):
#         mass_X_d  = float(s['mass_X_d'])
#         tau_pi_d  = float(s['tau_pi_d'])
#         expected  = float(s['sigma0'])
#         print(itau, mass_X_d, tau_pi_d, expected)
#         graph.SetPoint(itau, mass_X_d, log10(tau_pi_d), expected)
#     # graph.Draw("colz")
#     # graph.Draw("cont4 z")
#     hist = graph.GetHistogram()
#     # hist.Draw("colz")
#     contours = array.array('d', [5, 10])
#     hist.SetContour(2, contours)
#     hist.Draw("cont list")
#     rt.gPad.Update()
#     # hist.GetZaxis().SetRangeUser(0, 1e3)
#     hist.GetYaxis().SetLabelSize(0.0)
#     hist.GetYaxis().SetTickSize(0.0)
#     # axis = rt.TGaxis(400, 0, 400, 3, 1, 1000, 50510, "GW");
#     # axis.SetName("axis");
#     # axis.SetLabelFont(42);
#     # axis.SetLabelSize(0.05);
#     # axis.SetGridLength(1.00-0.08-0.16);
#     # axis.Draw();
#     x1, x2, y1, y2 = rt.Double(0), rt.Double(0), rt.Double(0), rt.Double(0)
#     contours = rt.gROOT.GetListOfSpecials().FindObject("contours")
#     contour_list = contours.At(0)
#     n_graphs = contour_list.GetSize()
#     contour_graph = contour_list.First().Clone()
#     contour_graph.Draw("c")

