import csv
from collections import OrderedDict
from rootpy import ROOT as rt
from rootpy.io import root_open
from rootpy.plotting import Hist, Hist2D, Hist3D, HistStack, Legend, Canvas, Graph, Pad
from rootpy.plotting.utils import draw
import tdrstyle
tdrstyle.setTDRStyle()
rt.gStyle.SetPalette(1)
batch = 1 # SWITCH
if batch:
    # Turn off graphics
    rt.gROOT.SetBatch(True)
blinded = 1 # SWITCH
from rootutils import saveAllCanvases, updateAllCanvases, deleteAllCanvases
from math import sin
import colorsys

# limitfile = 'emjet_limits_20180131.csv'
limitfile = 'emjet_limits_allcuts_20180205.csv'
# limitfile2 = 'emjet_limits_20180125.csv'
# limitfile2 = 'emjet_limits_20171220.csv'
# limitfile2 = 'emjet_limits_20180118.csv'

# Read into list of OrderedDict
with open(limitfile) as csvfile:
    reader = csv.DictReader(csvfile)
    keys = reader.fieldnames
    r = csv.reader(csvfile)
    signal_list = [OrderedDict(zip(keys, row)) for row in r]

########################################################################
# Do something
########################################################################
# Define list of parameters
mass_X_d_list = list( set([float(s['mass_X_d']) for s in signal_list]) )
mass_X_d_list.sort()
mass_pi_d_list = list( set([float(s['mass_pi_d']) for s in signal_list]) )
mass_pi_d_list.sort()
tau_pi_d_list = list( set([float(s['tau_pi_d']) for s in signal_list]) )
tau_pi_d_list.sort()
# mass_X_d_list = [1000]
# mass_pi_d_list = [1, 10]

# for mass_X_d in mass_X_d_list:
#     for mass_pi_d in mass_pi_d_list:
#         for tau_pi_d in tau_pi_d_list:
cutfilepath = '/mnt/hepcms/home/yhshin/workspace/EmJetHistoMaker/cuts/modelset_0105_combined.txt'
with open(cutfilepath) as csvfile:
    r = csv.reader(csvfile, skipinitialspace=True)
    cutdict = {row[0]: row[1] for row in r}

chosen_signal_list = []
for i, s in enumerate(signal_list):
    if s['cutname'] == cutdict[s['name']]:
        chosen_limit = s
        chosen_signal_list.append(chosen_limit)

with open('emjet_chosen_limits.csv', 'w') as ofile:
        writer = csv.DictWriter(ofile, fieldnames=chosen_signal_list[0].keys())
        writer.writeheader()
        for s in chosen_signal_list:
            writer.writerow(s)


