from rootpy import ROOT as rt
from rootpy.io import root_open
from rootpy.plotting import Canvas, Graph, Hist1D, Hist2D, Pad, Legend
from rootpy.plotting.utils import draw
import colorsys

def buildColorPalette(number):
    """Build color palette by dividing the color spectrum into the required number of colors"""
    colors = []
    for i in xrange(number):
        rgb_tuple = colorsys.hls_to_rgb(i/float(number), 0.5, 1.0)
        scaled_rgb_tuple = tuple([256*x for x in rgb_tuple])
        colors.append(scaled_rgb_tuple)
    return colors

from collections import OrderedDict
filenames = [
    'histo-QCD_HT1000to1500.root',
    'histo-QCD_HT1500to2000.root',
    'histo-QCD_HT2000toInf.root',
    'histo-QCD_HT500to700.root',
    'histo-QCD_HT700to1000.root',
    'histo-mass_X_d_1000_mass_pi_d_10_tau_pi_d_0p001.root',
    'histo-mass_X_d_1000_mass_pi_d_10_tau_pi_d_0p1.root',
    'histo-mass_X_d_1000_mass_pi_d_10_tau_pi_d_1.root',
    'histo-mass_X_d_1000_mass_pi_d_10_tau_pi_d_100.root',
    'histo-mass_X_d_1000_mass_pi_d_10_tau_pi_d_150.root',
    'histo-mass_X_d_1000_mass_pi_d_10_tau_pi_d_225.root',
    'histo-mass_X_d_1000_mass_pi_d_10_tau_pi_d_25.root',
    'histo-mass_X_d_1000_mass_pi_d_10_tau_pi_d_300.root',
    'histo-mass_X_d_1000_mass_pi_d_10_tau_pi_d_45.root',
    'histo-mass_X_d_1000_mass_pi_d_10_tau_pi_d_5.root',
    'histo-mass_X_d_1000_mass_pi_d_10_tau_pi_d_60.root',
]
colorlist = buildColorPalette(len(filenames))

file_prefix = '/mnt/hepcms/home/yhshin/data/condor_output/2017-11-16/histos-pileup0/'

def defaultCanvas(cname=""):
    # canvas = rt.TCanvas(cname, cname, W_ref, H_ref)
    canvas = Canvas(name=cname, title=cname, width=800, height=800)
    canvas.SetTopMargin(0.08)
    canvas.SetLeftMargin(0.16)
    canvas.SetRightMargin(0.08)
    canvas.cd()
    return canvas

def makeMcBaseHisto():
    """Make histogram corresponding to PDF in 2016_25ns_Moriond17MC_PoissonOOTPU
    Values from http://cmslxr.fnal.gov/dxr/CMSSW/source/SimGeneral/MixingModule/python/mix_2016_25ns_Moriond17MC_PoissonOOTPU_cfi.py"""
    probFunctionVariable = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74]
    probValue = [1.78653e-05 ,2.56602e-05 ,5.27857e-05 ,8.88954e-05 ,0.000109362 ,0.000140973 ,0.000240998 ,0.00071209 ,0.00130121 ,0.00245255 ,0.00502589 ,0.00919534 ,0.0146697 ,0.0204126 ,0.0267586 ,0.0337697 ,0.0401478 ,0.0450159 ,0.0490577 ,0.0524855 ,0.0548159 ,0.0559937 ,0.0554468 ,0.0537687 ,0.0512055 ,0.0476713 ,0.0435312 ,0.0393107 ,0.0349812 ,0.0307413 ,0.0272425 ,0.0237115 ,0.0208329 ,0.0182459 ,0.0160712 ,0.0142498 ,0.012804 ,0.011571 ,0.010547 ,0.00959489 ,0.00891718 ,0.00829292 ,0.0076195 ,0.0069806 ,0.0062025 ,0.00546581 ,0.00484127 ,0.00407168 ,0.00337681 ,0.00269893 ,0.00212473 ,0.00160208 ,0.00117884 ,0.000859662 ,0.000569085 ,0.000365431 ,0.000243565 ,0.00015688 ,9.88128e-05 ,6.53783e-05 ,3.73924e-05 ,2.61382e-05 ,2.0307e-05 ,1.73032e-05 ,1.435e-05 ,1.36486e-05 ,1.35555e-05 ,1.37491e-05 ,1.34255e-05 ,1.33987e-05 ,1.34061e-05 ,1.34211e-05 ,1.34177e-05 ,1.32959e-05 ,1.33287e-05]
    h = rt.Hist(100, 0, 100, name="nTrueInt", title="nTrueInt")
    for i in probFunctionVariable:
        bini = h.FindBin(i)
        h.SetBinContent(bini, probValue[i])
    h.color = 'black'
    return h

can = defaultCanvas("c1")
filelist = [root_open(file_prefix+f) for f in filenames]
histolist = [f.nTrueInt for f in filelist]
for i, h in enumerate(histolist):
    h.Scale(1./h.Integral())
    h.SetTitle(filenames[i])
    h.color = colorlist[i]
    h.legendstyle = 'ple'
draw(histolist)
mchisto = makeMcBaseHisto()
mchisto.Draw("l hist same")
legend =  rt.Legend(histolist, leftmargin=0.05, topmargin=0.05, rightmargin=0.05, entryheight=0.025, textfont=42, textsize=0.02)
legend.SetBorderSize(0)
legend.Draw()

ofile = rt.TFile("mcPileupPdf-2017-11-16.root", "NEW")
mchisto.Write()
ofile.Close()

