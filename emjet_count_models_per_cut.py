import pdb
import csv
from collections import OrderedDict

signalfile = 'emjet_signals_bestcuts_20180215.csv'
postfix = ''

cuts = [
    'cut1',
    'cut2',
    'cut3',
    'cut4',
    'cut5',
    'cut6',
    'cut7',
    'cut8',
]

# Read into list of OrderedDict
with open(signalfile) as csvfile:
    reader = csv.DictReader(csvfile)
    keys = reader.fieldnames
    r = csv.reader(csvfile)
    signal_list = [OrderedDict(zip(keys, row)) for row in r]

for cut in cuts:
    signal_count = 0
    for s in signal_list:
        if s['cutname'] == cut:
            signal_count += 1
    print 'Number of models for %s: %d' % (cut, signal_count)
