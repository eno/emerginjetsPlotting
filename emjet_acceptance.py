import pdb
import csv
from collections import OrderedDict
import os

from rootpy import ROOT as rt
from rootpy.io import root_open
from rootpy.plotting import Hist, Hist2D, Hist3D, HistStack, Legend, Canvas
import tdrstyle
tdrstyle.setTDRStyle()
rt.gStyle.SetPalette(1)
# Turn off graphics
rt.gROOT.SetBatch(1)
from rootutils import saveAllCanvases, saveAsAllCanvases, updateAllCanvases, deleteAllCanvases

import emjet_helpers as helper
# helper.outer.parameters['mass_X_d'].remove(1250)
# helper.outer.parameters['mass_X_d'].remove(1500)
# helper.outer.parameters['mass_X_d'].remove(2000)
helper.outer.parameters['tau_pi_d'].remove(0.001)
helper.outer.parameters['tau_pi_d'].remove(0.1)
# helper.outer.parameters['tau_pi_d'].remove(1)
# helper.outer.parameters['tau_pi_d'].remove(2)
# helper.outer.parameters['tau_pi_d'].remove(500)
helper.update_signal_parameter_dict()

def defaultCanvas(cname=""):
    # canvas = rt.TCanvas(cname, cname, W_ref, H_ref)
    canvas = Canvas(name=cname, title=cname, width=800, height=600)
    canvas.SetTopMargin(0.1)
    canvas.SetLeftMargin(0.16)
    canvas.SetRightMargin(0.20)
    canvas.cd()
    return canvas

def draw_cms_header(canvas=None, zoom=1.0, prelim_text="Preliminary", add_text="", lumi_text="16.1 fb^{-1}"):
    """prelim_text is right justified in italic"""
    b = 0.2 * zoom # Amount of space below text
    t = canvas.GetTopMargin()
    l = canvas.GetLeftMargin()
    r = canvas.GetRightMargin()
    if canvas: canvas.cd()
    latex = rt.TLatex()
    latex.SetTextAlign(11) # Left bottom aligned
    cms_size = zoom * 1.2
    prelim_size = zoom * 1.0
    add_size = zoom * 1.0
    lumi_size = zoom * 1.0
    if not add_text:
        left_latex = latex.DrawLatexNDC(l, (1-t)+b*t, '#scale[%.1f]{#font[61]{CMS}} #kern[-0.03]{#font[52]{#scale[%.1f]{%s}}}' % (cms_size, prelim_size, prelim_text))
    else:
        left_latex = latex.DrawLatexNDC(l, (1-0.8*t)+b*t, '#scale[%.1f]{#font[61]{CMS}} #kern[-0.03]{#font[52]{#scale[%.1f]{%s}}} #scale[%.1f]{#font[42]{%s}}' % (cms_size, prelim_size, prelim_text, add_size, add_text, ))
    latex.SetTextAlign(31) # Right bottom aligned
    right_latex = latex.DrawLatexNDC(1-r, (1-0.8*t)+b*t, '#scale[%.1f]{#font[42]{%s (13 TeV)}}' % (lumi_size, lumi_text))
    return (left_latex, right_latex)

def getAcceptance(s):
    """Calculate acceptance from signal csv dict read from signal file. Return percentage value, e.g. 30 for 30%. Return -1 for error conditions."""
    return float(s['acceptance']) * 100

def getModelingSystematic(s):
    """Calculate magnitude of modeling systematic from signal csv dict read from signal file. Return percentage value, e.g. 30 for 30%. Return -1 for error conditions."""
    if float(s['acceptance']) == 0 : return -1
    else                           : percentShift = abs( float(s['acceptance_shift_ModelingUp' ]) / float(s['acceptance']) )
    if percentShift == 0: percentShift = 0.000001 # FIXME Temporary fix
    return percentShift * 100

def getPileupSystematic(s):
    """Calculate magnitude of pileup systematic from signal csv dict read from signal file. Return percentage value, e.g. 30 for 30%. Return -1 for error conditions."""
    if float(s['acceptance']) == 0 : return -1
    percentShift_up = abs( float(s['acceptance_shift_PileupUp' ]) / float(s['acceptance']) )
    percentShift_dn = abs( float(s['acceptance_shift_PileupDn' ]) / float(s['acceptance']) )
    percentShift = max(percentShift_up, percentShift_dn)
    if percentShift == 0: percentShift = 0.000001 # FIXME Temporary fix
    return percentShift * 100

def getJecSystematic(s):
    """Calculate magnitude of JEC systematic from signal csv dict read from signal file. Return percentage value, e.g. 30 for 30%. Return -1 for error conditions."""
    if float(s['acceptance']) == 0 : return -1
    percentShift_up = abs( float(s['acceptance_shift_JecUp' ]) / float(s['acceptance']) )
    percentShift_dn = abs( float(s['acceptance_shift_JecDn' ]) / float(s['acceptance']) )
    percentShift = max(percentShift_up, percentShift_dn)
    if percentShift == 0: percentShift = 0.000001 # FIXME Temporary fix
    return percentShift * 100

def getTriggerSystematic(s):
    """Calculate magnitude of trigger systematic from signal csv dict read from signal file. Return percentage value, e.g. 30 for 30%. Return -1 for error conditions."""
    if float(s['acceptance']) == 0 : return -1
    else                           : percentShift = abs( float(s['acceptance_shift_TriggerUp' ]) / float(s['acceptance']) )
    if percentShift == 0: percentShift = 0.000001 # FIXME Temporary fix
    return percentShift * 100

def getPdfSystematic(s):
    """Calculate magnitude of pdf systematic from signal csv dict read from signal file. Return percentage value, e.g. 30 for 30%. Return -1 for error conditions."""
    if float(s['acceptance']) == 0 : return -1
    percentShift_up = abs( float(s['acceptance_shift_PdfUp' ]) / float(s['acceptance']) )
    percentShift_dn = abs( float(s['acceptance_shift_PdfDn' ]) / float(s['acceptance']) )
    percentShift = max(percentShift_up, percentShift_dn)
    if percentShift == 0: percentShift = 0.000001 # FIXME Temporary fix
    return percentShift * 100

def getMCStatSystematic(s):
    """Calculate magnitude of MC stat systematic from signal csv dict read from signal file. Return percentage value, e.g. 30 for 30%. Return -1 for error conditions."""
    if float(s['acceptance']) == 0 : return -1
    else                           : percentShift = abs( float(s['acceptance_shift_MCStatUp' ]) / float(s['acceptance']) )
    if percentShift == 0: percentShift = 0.000001 # FIXME Temporary fix
    return percentShift * 100


# def getHistRange(hist_in):
#     """Take histogram and return (min, max) values"""
#     minbin = hist_in.GetMinimumBin(); minval = hist_in.GetBinContent(minbin)
#     maxbin = hist_in.GetMaximumBin(); maxval = hist_in.GetBinContent(maxbin)
#     return (minval, maxval)

signalfile = 'emjet_signals_combined_20180613.csv'
#signalfile = 'emjet_signals_bestcuts_20180509_combined.csv'
#signalfile = 'emjet_signals_met_20180613.csv'
postfix = ''
dir = '2019-02-01-HEPData'
if dir:
    if not os.path.isdir(dir):
        os.mkdir(dir)
    prefix = dir + '/'
else:
    prefix = ''
#save_format = "pdf"
# save_format = "png"
save_format = "root"
prelim_text_input = "Simulation"
# prelim_text_input = "Simulation Preliminary"; postfix = 'pas'


# Read into list of OrderedDict
with open(signalfile) as csvfile:
    reader = csv.DictReader(csvfile)
    keys = reader.fieldnames
    r = csv.reader(csvfile)
    signal_list = [OrderedDict(zip(keys, row)) for row in r]

########################################################################
# Write plain acceptance table
########################################################################
if 0:
    acceptance_table = []
    with open(signalfile) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            acceptance_table_entry = OrderedDict()
            acceptance_table_entry[r'$m_{X_{d}}$ [GeV]']     = '%g' % float(row['mass_X_d'])
            acceptance_table_entry[r'$m_{\pi_{d}}$ [GeV]']   = '%g' % float(row['mass_pi_d'])
            acceptance_table_entry[r'$\tau_{\pi_{d}}$ [mm]'] = '%g' % float(row['tau_pi_d'])
            if (float(row['acceptance']) * 100) < 1.0 : acceptance_format = '%.2f'
            else                                      : acceptance_format = '%.1f'
            acceptance_table_entry[r'Acceptance [%]']        = acceptance_format % (float(row['acceptance']) * 100)
            acceptance_table.append(acceptance_table_entry)
    with open('emjet_acceptance_table.txt', 'w') as ofile:
        writer = csv.DictWriter(ofile, fieldnames=acceptance_table[0].keys())
        writer.writeheader()
        for a in acceptance_table:
            writer.writerow(a)

########################################################################
# Compact acceptance table, with lifetime written to a single row
########################################################################
if 1:
    # Construct table with empty fields
    compact_acc_table = []
    masses_X_d = [400, 600, 800, 1000, 1250, 1500, 2000]
    masses     = [1, 2, 5, 10,]
    lifetimes  = [1, 2, 5, 25, 45, 60, 100, 150, 225, 300, 500, 1000]
    entry = OrderedDict()
    entry['mass_X_d']     = ""
    entry['mass_pi_d']    = ""
    for lifetime in lifetimes:
        entry[lifetime] = ""
    for mass_X_d in masses_X_d:
        for mass in masses:
            e = entry.copy()
            e['mass_X_d'] = mass_X_d
            e['mass_pi_d'] = mass
            compact_acc_table.append(e)
    # Fill table
    with open(signalfile) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            cut = (row['cutname']).replace('cut', '')
            mass_X_d   = float(row['mass_X_d'])
            mass_pi_d  = float(row['mass_pi_d'])
            tau_pi_d   = float(row['tau_pi_d'])
            acceptance = (float(row['acceptance']) * 100)
            if (float(row['acceptance']) * 100) < 1.0 : acceptance_format = '%.2f'
            else                                      : acceptance_format = '%.1f'
            for e in compact_acc_table:
                if e['mass_X_d'] == mass_X_d and e['mass_pi_d'] == mass_pi_d:
                    e[tau_pi_d] = (acceptance_format + ' (%s)') % (acceptance, cut)
    with open('emjet_compact_acceptance_table.txt', 'w') as ofile:
        writer = csv.DictWriter(ofile, fieldnames=entry.keys())
        writer.writeheader()
        for a in compact_acc_table:
            writer.writerow(a)

########################################################################
# Find range of each systematic
########################################################################
labels = ['Acceptance', 'Modeling', 'Pileup', 'JEC', 'Trigger', 'PDF', 'MC statistical']
funcs  = [getAcceptance, getModelingSystematic, getPileupSystematic, getJecSystematic, getTriggerSystematic, getPdfSystematic, getMCStatSystematic]
##labels = ['Acceptance', 'Pileup', 'JEC', 'Trigger', 'PDF', 'MC statistical']
##funcs  = [getAcceptance, getPileupSystematic, getJecSystematic, getTriggerSystematic, getPdfSystematic, getMCStatSystematic]
# labels = ['Acceptance', 'Pileup', 'JEC', 'Trigger', 'PDF', 'MC statistical']
# funcs  = [getAcceptance, getPileupSystematic, getJecSystematic, getTriggerSystematic, getPdfSystematic, getMCStatSystematic]
with open('emjet_range_table.txt', 'w') as ofile:
    for i, label in enumerate(labels):
        func = funcs[i]
        maxval = func( max(signal_list, key=func) )
        minval = func( min(signal_list, key=func) )
        ofile.write( '%s [%%]: %f, %f\n' % (label, minval, maxval) )

selected_signal_names = []
#with open('emjet_excluded_models_1.txt') as ifile:
with open('emjet_excluded_models.txt') as ifile:
    for line in ifile:
        name = line.rstrip()
        selected_signal_names.append(name)
signal_list_filtered = [s for s in signal_list if s['name'] in selected_signal_names]

labels = ['Acceptance', 'Modeling', 'Pileup', 'JEC', 'Trigger', 'PDF', 'MC statistical']
funcs  = [getAcceptance, getModelingSystematic, getPileupSystematic, getJecSystematic, getTriggerSystematic, getPdfSystematic, getMCStatSystematic]
##labels = ['Acceptance', 'Pileup', 'JEC', 'Trigger', 'PDF', 'MC statistical']
##funcs  = [getAcceptance, getPileupSystematic, getJecSystematic, getTriggerSystematic, getPdfSystematic, getMCStatSystematic]
# labels = ['Acceptance', 'Pileup', 'JEC', 'Trigger', 'PDF', 'MC statistical']
# funcs  = [getAcceptance, getPileupSystematic, getJecSystematic, getTriggerSystematic, getPdfSystematic, getMCStatSystematic]
with open('emjet_range_table_filtered.txt', 'w') as ofile:
    for i, label in enumerate(labels):
        func = funcs[i]
        maxval = func( max(signal_list_filtered, key=func) )
        minval = func( min(signal_list_filtered, key=func) )
        ofile.write( '%s [%%]: %f, %f\n' % (label, minval, maxval) )

########################################################################
# Make 2D histograms and write to ROOT file
########################################################################
savePlots = 1
drawCutNumber = 1 # Draw cut number in acceptance plots
cutNumberDict = {}
cutNumberDict['cut1'] = 1
cutNumberDict['cut2'] = 2
cutNumberDict['cut3'] = -1
cutNumberDict['cut4'] = 3
cutNumberDict['cut5'] = 4
cutNumberDict['cut6'] = 5
cutNumberDict['cut7'] = 6
cutNumberDict['cut8'] = 7
# Acceptance
if savePlots:
    fh = rt.File('emjet_acceptance_plots.root', 'RECREATE')
    mass_X_d_list = list( set([float(s['mass_X_d']) for s in signal_list]) )
    mass_X_d_list.sort()
    mass_pi_d_list = list( set([float(s['mass_pi_d']) for s in signal_list]) )
    mass_pi_d_list.sort()
    tau_pi_d_list = list( set([float(s['tau_pi_d']) for s in signal_list]) )
    tau_pi_d_list.sort()
    canvases = OrderedDict()
    # Save plots in ROOT and PNG files
    if 0:
        # mass_pi_d vs tau_pi_d acceptance plots
        for mass_X_d in mass_X_d_list:
            hist = helper.mass_X_d_hist()
            hist.SetName('mass_X_d_%g' % mass_X_d)
            name = hist.GetName()
            signal_list_filtered = [s for s in signal_list if float(s['mass_X_d'])==mass_X_d]
            for s in signal_list_filtered:
                mass_pi_d  = float(s['mass_pi_d'  ])
                tau_pi_d   = float(s['tau_pi_d'   ])
                acceptance = float(s['acceptance' ])
                hist.Fill("%g" % tau_pi_d, "%g" % mass_pi_d, acceptance)
            canvases[name] = defaultCanvas(name)
            hist.SetMinimum(0)
            hist.SetMaximum(0.40)
            hist.Draw("colz")
            # canvases[name].SaveAs(".png")
            hist.Write()
    if 1:
        # mass_X_d vs tau_pi_d acceptance plots
        for mass_pi_d in mass_pi_d_list:
            hist = helper.mass_pi_d_hist()
            hist.SetName('mass_pi_d_%g' % mass_pi_d)
            name = hist.GetName()
            signal_list_filtered = [s for s in signal_list if float(s['mass_pi_d'])==mass_pi_d]
            for s in signal_list_filtered:
                mass_X_d   = float(s['mass_X_d'   ])
                tau_pi_d   = float(s['tau_pi_d'   ])
                acceptance = float(s['acceptance' ])
                hist.Fill("%g" % tau_pi_d, "%g" % mass_X_d, acceptance)
            canvases[name] = defaultCanvas(name)
            hist.SetMinimum(0)
            hist.SetMaximum(0.40)
            hist.Draw("colz")
            if drawCutNumber:
                cuthist = helper.mass_pi_d_hist()
                for s in signal_list_filtered:
                    mass_X_d   = float(s['mass_X_d'   ])
                    tau_pi_d   = float(s['tau_pi_d'   ])
                    acceptance = float(s['acceptance' ])
                    # cut = int( (s['cutname']).replace('cut', '') )
                    cut = cutNumberDict[s['cutname']]
                    cuthist.Fill("%g" % tau_pi_d, "%g" % mass_X_d, cut)
                cuthist.Draw("same text")
            # canvases[name].SaveAs(".png")
            hist.Write()
    saveAllCanvases(postfix=postfix, option=save_format, prefix=prefix+"acceptance_")
    deleteAllCanvases()
    if 1:
        # tau_pi_d vs mass_X_d acceptance plots
        for mass_pi_d in mass_pi_d_list:
            hist = helper.mass_pi_d_hist(flip=True)
            hist.SetName('mass_pi_d_%g' % mass_pi_d)
            name = hist.GetName()
            hist.SetName('hist_mass_pi_d_%g' % mass_pi_d)
            signal_list_filtered = [s for s in signal_list if float(s['mass_pi_d'])==mass_pi_d]
            for s in signal_list_filtered:
                mass_X_d   = float(s['mass_X_d'   ])
                tau_pi_d   = float(s['tau_pi_d'   ])
                acceptance = float(s['acceptance' ])
                hist.Fill("%g" % mass_X_d, "%g" % tau_pi_d, acceptance)
            canvases[name] = defaultCanvas(name)
            #canvases[name].SetRightMargin(0.2)#0.24
            #hist.GetZaxis().SetTitle("Signal acceptance (m_{#pi_{DK}} = %g GeV)" % (mass_pi_d))
            hist.GetZaxis().SetTitle("A")
            hist.GetZaxis().CenterTitle()
            #hist.GetZaxis().SetTitleFont(42)
            #hist.GetZaxis().SetTitleSize(0.06)
            hist.GetXaxis().SetTitleOffset(1.)
            hist.GetYaxis().SetTitleOffset(1.1)
            hist.GetZaxis().SetTitleOffset(1.)#1.25
            hist.SetMinimum(0)
            hist.SetMaximum(0.40)
            hist.Draw("colz")
            hist.GetZaxis().SetLabelSize(0.033)
            canvases[name].Modified()
            if drawCutNumber:
                cuthist = helper.mass_pi_d_hist(flip=True)
                for s in signal_list_filtered:
                    mass_X_d   = float(s['mass_X_d'   ])
                    tau_pi_d   = float(s['tau_pi_d'   ])
                    acceptance = float(s['acceptance' ])
                    # cut = int( (s['cutname']).replace('cut', '') )
                    cut = cutNumberDict[s['cutname']]
                    cuthist.SetMarkerSize(1.4)
                    #cuthist.SetBarOffset(0.2)
                    #ci=rt.TColor.GetColor("#222b3a");
                    #cuthist.SetMarkerColor(ci)
                    cuthist.Fill("%g" % mass_X_d, "%g" % tau_pi_d, cut)
                cuthist.Draw("same text")

##                cuthist1 = helper.mass_pi_d_hist(flip=True)
##                for s in signal_list_filtered:
##                    mass_X_d   = float(s['mass_X_d'   ])
##                    tau_pi_d   = float(s['tau_pi_d'   ])
##                    acceptance = float(s['acceptance' ])
##                    # cut = int( (s['cutname']).replace('cut', '') )
##                    cut = cutNumberDict[s['cutname']]
##                    cuthist1.SetMarkerSize(2)
##                    cuthist1.SetMarkerColor(10)
##                    cuthist1.Fill("%g" % mass_X_d, "%g" % tau_pi_d, cut)
##                cuthist1.Draw("same text")

            # canvases[name].SaveAs(".png")
            hist.Write()
            #(left_text, right_text) = draw_cms_header(canvas=canvases[name], zoom=1.0, prelim_text=prelim_text_input, add_text="", lumi_text="")
            (left_text, right_text) = draw_cms_header(canvas=canvases[name], zoom=0.8, prelim_text=prelim_text_input, add_text="(m_{#pi_{DK}}= %g GeV)" % (mass_pi_d), lumi_text="")
    saveAllCanvases(postfix=postfix, option=save_format, prefix=prefix+"acceptance_flip_")
    saveAsAllCanvases(postfix=postfix, prefix=prefix+"acceptance_flip_")
    deleteAllCanvases()

# Modeling uncertainty
#if 0:
if savePlots:
    mass_X_d_list = list( set([float(s['mass_X_d']) for s in signal_list]) )
    mass_X_d_list.sort()
    mass_pi_d_list = list( set([float(s['mass_pi_d']) for s in signal_list]) )
    mass_pi_d_list.sort()
    tau_pi_d_list = list( set([float(s['tau_pi_d']) for s in signal_list]) )
    tau_pi_d_list.sort()
    canvases = OrderedDict()
    # mass_X_d vs tau_pi_d modeling uncertainty plots
    for mass_pi_d in mass_pi_d_list:
        hist = helper.mass_pi_d_hist()
        hist.SetName('mass_pi_d_%g' % mass_pi_d)
        name = hist.GetName()
        signal_list_filtered = [s for s in signal_list if float(s['mass_pi_d'])==mass_pi_d]
        for s in signal_list_filtered:
            mass_X_d   = float(s['mass_X_d'   ])
            tau_pi_d   = float(s['tau_pi_d'   ])
            # if float(s['acceptance'])==0: continue
            if float(s['acceptance']) == 0 : percentShift = 0
            else                           : percentShift = float(s['acceptance_shift_ModelingUp' ]) / float(s['acceptance'])
            if percentShift == 0: percentShift = 0.000001 # FIXME Temporary fix
            hist.Fill("%g" % tau_pi_d, "%g" % mass_X_d, percentShift)
        canvases[name] = defaultCanvas(name)
        # canvases[name].SetLogz()
        hist.SetMinimum(0)
        hist.SetMaximum(0.20)
        hist.Draw("colz")
        # canvases[name].SaveAs(".png")
    saveAllCanvases(postfix=postfix, option=save_format, prefix=prefix+"acceptance_Modeling_")
    deleteAllCanvases()

# Pileup reweighting uncertainty
if savePlots:
    mass_X_d_list = list( set([float(s['mass_X_d']) for s in signal_list]) )
    mass_X_d_list.sort()
    mass_pi_d_list = list( set([float(s['mass_pi_d']) for s in signal_list]) )
    mass_pi_d_list.sort()
    tau_pi_d_list = list( set([float(s['tau_pi_d']) for s in signal_list]) )
    tau_pi_d_list.sort()
    canvases = OrderedDict()
    # mass_X_d vs tau_pi_d Jec uncertainty plots
    for mass_pi_d in mass_pi_d_list:
        hist = helper.mass_pi_d_hist()
        hist.SetName('mass_pi_d_%g' % mass_pi_d)
        name = hist.GetName()
        signal_list_filtered = [s for s in signal_list if float(s['mass_pi_d'])==mass_pi_d]
        for s in signal_list_filtered:
            mass_X_d   = float(s['mass_X_d'   ])
            tau_pi_d   = float(s['tau_pi_d'   ])
            magnitude = getPileupSystematic(s)/100
            if magnitude>0:
                hist.Fill("%g" % tau_pi_d, "%g" % mass_X_d, magnitude)
        canvases[name] = defaultCanvas(name)
        hist.SetMinimum(0)
        hist.SetMaximum(0.20)
        hist.Draw("colz")
        # canvases[name].SaveAs(".png")
    saveAllCanvases(postfix=postfix, option=save_format, prefix=prefix+"acceptance_Pileup_")
    deleteAllCanvases()

# Trigger uncertainty
if savePlots:
    mass_X_d_list = list( set([float(s['mass_X_d']) for s in signal_list]) )
    mass_X_d_list.sort()
    mass_pi_d_list = list( set([float(s['mass_pi_d']) for s in signal_list]) )
    mass_pi_d_list.sort()
    tau_pi_d_list = list( set([float(s['tau_pi_d']) for s in signal_list]) )
    tau_pi_d_list.sort()
    canvases = OrderedDict()
    # mass_X_d vs tau_pi_d trigger uncertainty plots
    for mass_pi_d in mass_pi_d_list:
        hist = helper.mass_pi_d_hist()
        hist.SetName('mass_pi_d_%g' % mass_pi_d)
        name = hist.GetName()
        signal_list_filtered = [s for s in signal_list if float(s['mass_pi_d'])==mass_pi_d]
        for s in signal_list_filtered:
            mass_X_d   = float(s['mass_X_d'   ])
            tau_pi_d   = float(s['tau_pi_d'   ])
            if float(s['acceptance'])==0:
                # print 'Acceptance == 0'
                continue
            percentShift = abs( float(s['acceptance_shift_TriggerUp' ]) / float(s['acceptance']) )
            if percentShift == 0: percentShift = 0.000001 # FIXME Temporary fix
            # if mass_X_d == 400:
            #     print float(s['acceptance_shift_TriggerUp' ]) , float(s['acceptance'])
            #     print "%g" % tau_pi_d, "%g" % mass_X_d, percentShift
            hist.Fill("%g" % tau_pi_d, "%g" % mass_X_d, percentShift)
        canvases[name] = defaultCanvas(name)
        hist.SetMinimum(0)
        hist.SetMaximum(0.20)
        hist.Draw("colz")
        # canvases[name].SaveAs(".png")
    saveAllCanvases(postfix=postfix, option=save_format, prefix=prefix+"acceptance_Trigger_")
    deleteAllCanvases()

# JEC uncertainty
if savePlots:
    mass_X_d_list = list( set([float(s['mass_X_d']) for s in signal_list]) )
    mass_X_d_list.sort()
    mass_pi_d_list = list( set([float(s['mass_pi_d']) for s in signal_list]) )
    mass_pi_d_list.sort()
    tau_pi_d_list = list( set([float(s['tau_pi_d']) for s in signal_list]) )
    tau_pi_d_list.sort()
    canvases = OrderedDict()
    # mass_X_d vs tau_pi_d Jec uncertainty plots
    for mass_pi_d in mass_pi_d_list:
        hist = helper.mass_pi_d_hist()
        hist.SetName('mass_pi_d_%g' % mass_pi_d)
        name = hist.GetName()
        signal_list_filtered = [s for s in signal_list if float(s['mass_pi_d'])==mass_pi_d]
        for s in signal_list_filtered:
            mass_X_d   = float(s['mass_X_d'   ])
            tau_pi_d   = float(s['tau_pi_d'   ])
            if float(s['acceptance'])==0: continue
            percentShift_up = float(s['acceptance_shift_JecUp' ]) / float(s['acceptance'])
            percentShift_dn = float(s['acceptance_shift_JecDn' ]) / float(s['acceptance'])
            percentShift = max(percentShift_up, abs(percentShift_dn))
            if percentShift == 0: percentShift = 0.000001 # FIXME Temporary fix
            hist.Fill("%g" % tau_pi_d, "%g" % mass_X_d, percentShift)
        canvases[name] = defaultCanvas(name)
        hist.SetMinimum(0)
        hist.SetMaximum(0.20)
        hist.Draw("colz")
        # canvases[name].SaveAs(".png")
    saveAllCanvases(postfix=postfix, option=save_format, prefix=prefix+"acceptance_Jec_")
    deleteAllCanvases()

# PDF uncertainty
if savePlots:
    mass_X_d_list = list( set([float(s['mass_X_d']) for s in signal_list]) )
    mass_X_d_list.sort()
    mass_pi_d_list = list( set([float(s['mass_pi_d']) for s in signal_list]) )
    mass_pi_d_list.sort()
    tau_pi_d_list = list( set([float(s['tau_pi_d']) for s in signal_list]) )
    tau_pi_d_list.sort()
    canvases = OrderedDict()
    # mass_X_d vs tau_pi_d Jec uncertainty plots
    for mass_pi_d in mass_pi_d_list:
        hist = helper.mass_pi_d_hist()
        hist.SetName('mass_pi_d_%g' % mass_pi_d)
        name = hist.GetName()
        signal_list_filtered = [s for s in signal_list if float(s['mass_pi_d'])==mass_pi_d]
        for s in signal_list_filtered:
            mass_X_d   = float(s['mass_X_d'   ])
            tau_pi_d   = float(s['tau_pi_d'   ])
            magnitude = getPdfSystematic(s)/100
            if magnitude>0:
                hist.Fill("%g" % tau_pi_d, "%g" % mass_X_d, magnitude)
        canvases[name] = defaultCanvas(name)
        hist.SetMinimum(0)
        hist.SetMaximum(0.20)
        hist.Draw("colz")
        # canvases[name].SaveAs(".png")
    saveAllCanvases(postfix=postfix, option=save_format, prefix=prefix+"acceptance_Pdf_")
    deleteAllCanvases()

# MC stat uncertainty
if savePlots:
    mass_X_d_list = list( set([float(s['mass_X_d']) for s in signal_list]) )
    mass_X_d_list.sort()
    mass_pi_d_list = list( set([float(s['mass_pi_d']) for s in signal_list]) )
    mass_pi_d_list.sort()
    tau_pi_d_list = list( set([float(s['tau_pi_d']) for s in signal_list]) )
    tau_pi_d_list.sort()
    canvases = OrderedDict()
    # mass_X_d vs tau_pi_d Jec uncertainty plots
    for mass_pi_d in mass_pi_d_list:
        hist = helper.mass_pi_d_hist()
        hist.SetName('mass_pi_d_%g' % mass_pi_d)
        name = hist.GetName()
        signal_list_filtered = [s for s in signal_list if float(s['mass_pi_d'])==mass_pi_d]
        for s in signal_list_filtered:
            mass_X_d   = float(s['mass_X_d'   ])
            tau_pi_d   = float(s['tau_pi_d'   ])
            if float(s['acceptance'])==0: continue
            percentShift_up = float(s['acceptance_shift_MCStatUp' ]) / float(s['acceptance'])
            percentShift_dn = float(s['acceptance_shift_MCStatDn' ]) / float(s['acceptance'])
            percentShift = max(percentShift_up, abs(percentShift_dn))
            if percentShift == 0: percentShift = 0.000001 # FIXME Temporary fix
            hist.Fill("%g" % tau_pi_d, "%g" % mass_X_d, percentShift)
        canvases[name] = defaultCanvas(name)
        hist.SetMinimum(0)
        hist.SetMaximum(1.00)
        hist.Draw("colz")
        # canvases[name].SaveAs(".png")
    saveAllCanvases(postfix=postfix, option=save_format, prefix=prefix+"acceptance_MCStat_")
    deleteAllCanvases()

# HLT efficiency
plotHltEfficiency = 0
if plotHltEfficiency:
    mass_X_d_list = list( set([float(s['mass_X_d']) for s in signal_list]) )
    mass_X_d_list.sort()
    mass_pi_d_list = list( set([float(s['mass_pi_d']) for s in signal_list]) )
    mass_pi_d_list.sort()
    tau_pi_d_list = list( set([float(s['tau_pi_d']) for s in signal_list]) )
    tau_pi_d_list.sort()
    canvases = OrderedDict()
    # mass_X_d vs tau_pi_d Jec uncertainty plots
    for mass_pi_d in mass_pi_d_list:
        hist = helper.mass_pi_d_hist()
        hist.SetName('mass_pi_d_%g' % mass_pi_d)
        name = hist.GetName()
        signal_list_filtered = [s for s in signal_list if float(s['mass_pi_d'])==mass_pi_d]
        for s in signal_list_filtered:
            mass_X_d   = float(s['mass_X_d'   ])
            tau_pi_d   = float(s['tau_pi_d'   ])
            hltefficiency = float(s['"HLT_PFHT900 == 1"']) / float(s['"nocut"'])
            # print hltefficiency
            # if float(s['acceptance'])==0: continue
            # percentShift_up = float(s['acceptance_shift_MCStatUp' ]) / float(s['acceptance'])
            # percentShift_dn = float(s['acceptance_shift_MCStatDn' ]) / float(s['acceptance'])
            # percentShift = max(percentShift_up, abs(percentShift_dn))
            # if percentShift == 0: percentShift = 0.000001 # FIXME Temporary fix
            hist.Fill("%g" % tau_pi_d, "%g" % mass_X_d, hltefficiency)
        canvases[name] = defaultCanvas(name)
        hist.SetMinimum(0)
        hist.SetMaximum(1.00)
        hist.Draw("colz")
        # canvases[name].SaveAs(".png")
    saveAllCanvases(postfix=postfix, option=save_format, prefix=prefix+"hltEfficiency_")
    deleteAllCanvases()

# Change in acceptance after changing cutflow
plotAcceptanceChange = 0
if plotAcceptanceChange:
    mass_X_d_list = list( set([float(s['mass_X_d']) for s in signal_list]) )
    mass_X_d_list.sort()
    mass_pi_d_list = list( set([float(s['mass_pi_d']) for s in signal_list]) )
    mass_pi_d_list.sort()
    tau_pi_d_list = list( set([float(s['tau_pi_d']) for s in signal_list]) )
    tau_pi_d_list.sort()
    canvases = OrderedDict()
    # mass_X_d vs tau_pi_d Jec uncertainty plots
    for mass_pi_d in mass_pi_d_list:
        hist = helper.mass_pi_d_hist()
        hist.SetName('mass_pi_d_%g' % mass_pi_d)
        name = hist.GetName()
        signal_list_filtered = [s for s in signal_list if float(s['mass_pi_d'])==mass_pi_d]
        for s in signal_list_filtered:
            mass_X_d   = float(s['mass_X_d'   ])
            tau_pi_d   = float(s['tau_pi_d'   ])
            acceptanceChange = float(s['"nEmerging >= nEmerging_min"']) / float(s['"nEmerging == nEmerging_min"']) - 1
            cut = int( (s['cutname']).replace('cut', '') )
            if acceptanceChange > 0.3:
                print acceptanceChange
            # if float(s['acceptance'])==0: continue
            # percentShift_up = float(s['acceptance_shift_MCStatUp' ]) / float(s['acceptance'])
            # percentShift_dn = float(s['acceptance_shift_MCStatDn' ]) / float(s['acceptance'])
            # percentShift = max(percentShift_up, abs(percentShift_dn))
            # if percentShift == 0: percentShift = 0.000001 # FIXME Temporary fix
            if cut != 4:
                hist.Fill("%g" % tau_pi_d, "%g" % mass_X_d, acceptanceChange)
        canvases[name] = defaultCanvas(name)
        hist.SetMinimum(0)
        # hist.SetMaximum(1.00)
        hist.Draw("colz")
        if drawCutNumber:
            cuthist = helper.mass_pi_d_hist()
            for s in signal_list_filtered:
                mass_X_d   = float(s['mass_X_d'   ])
                tau_pi_d   = float(s['tau_pi_d'   ])
                acceptance = float(s['acceptance' ])
                cut = int( (s['cutname']).replace('cut', '') )
                cuthist.Fill("%g" % tau_pi_d, "%g" % mass_X_d, cut)
            cuthist.Draw("same text")
        # canvases[name].SaveAs(".png")
    saveAllCanvases(postfix=postfix, option=save_format, prefix=prefix+"acceptanceChange_")
    deleteAllCanvases()

# Count number of signal models per cut
cuts = [
    'cut1',
    'cut2',
    'cut3',
    'cut4',
    'cut5',
    'cut6',
    'cut7',
    'cut8',
]
doSignalCount = 1
if doSignalCount:
    signalcountfile = open('emjet_signal_count.txt', 'w')
    signal_count = OrderedDict()
    for cut in cuts:
        signal_count[cut] = 0
    for s in signal_list:
        cutname = s['cutname']
        for cut in cuts:
            if cut==cutname:
                signal_count[cut] += 1
    # print signal_count
    for cut in cuts:
        signalcountfile.write("%s, %g\n" % (cut, signal_count[cut]))
    signalcountfile.close()

# Filter samples with large stat. uncertainties and print info to file
debug = 0
if debug:
    debugfile = open('emjet_acceptance_debug.txt', 'w')
    debugfile.write('# Models with large stat. uncertainty\n')
    # debugfile.write('name, cutname, err_stat\n')
    for s in signal_list:
        dump = False
        mass_X_d   = float(s['mass_X_d'   ])
        mass_pi_d  = float(s['mass_pi_d'  ])
        tau_pi_d   = float(s['tau_pi_d'   ])
        if float(s['acceptance'])==0:
            percentShift = 0
            dump = True
        else:
            percentShift_up = float(s['acceptance_shift_MCStatUp' ]) / float(s['acceptance'])
            percentShift_dn = float(s['acceptance_shift_MCStatDn' ]) / float(s['acceptance'])
            percentShift = max(percentShift_up, abs(percentShift_dn))
            if percentShift > 0.15: dump = True
        if dump:
            infodump = [ s['name'], s['cutname'], s['"nocut"'], s['acceptance'], '%f' % percentShift ]
            debugfile.write(", ".join(infodump) + "\n")
    debugfile.close()
