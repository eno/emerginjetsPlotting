blinded = 1
import csv
from collections import OrderedDict

from rootpy import ROOT as rt
from rootpy.io import root_open
from rootpy.plotting import Hist, Hist2D, Hist3D, HistStack, Legend, Canvas
import tdrstyle
tdrstyle.setTDRStyle()
rt.gStyle.SetPalette(1)
# Turn off graphics
rt.gROOT.SetBatch(True)
from rootutils import saveAllCanvases, updateAllCanvases, deleteAllCanvases

import emjet_helpers as helper
helper.outer.parameters['mass_X_d'].remove(1250)
helper.outer.parameters['mass_X_d'].remove(1500)
helper.outer.parameters['mass_X_d'].remove(2000)
helper.outer.parameters['tau_pi_d'].remove(0.001)
helper.outer.parameters['tau_pi_d'].remove(0.1)
helper.outer.parameters['tau_pi_d'].remove(1)
helper.outer.parameters['tau_pi_d'].remove(2)
helper.outer.parameters['tau_pi_d'].remove(500)
helper.update_signal_parameter_dict()

def defaultCanvas(cname=""):
    # canvas = rt.TCanvas(cname, cname, W_ref, H_ref)
    canvas = Canvas(name=cname, title=cname, width=800, height=600)
    canvas.SetTopMargin(0.08)
    canvas.SetLeftMargin(0.16)
    canvas.SetRightMargin(0.16)
    canvas.cd()
    return canvas

########################################################################
# Table of event yields
########################################################################
# Format: cutname, Expected, Observed
backgrounds = OrderedDict()
with open('emjet_background.csv') as csvfile, open('emjet_results_table.txt', 'w') as ofile:
    r = csv.reader(csvfile)
    next(r) # Ignore first line
    writer = csv.DictWriter(ofile, ['Cut set', 'Expected', 'Observed'])
    writer.writeheader()
    for row in r:
        cutname              = row[0]
        bg_predicted         = float(row[1])
        bg_predicted_error   = float(row[2])
        observed             = float(row[3])
        backgrounds[cutname] = (bg_predicted, bg_predicted_error, observed)
        entry = OrderedDict()
        entry['Cut set'] = cutname.replace('cut', '')
        entry['Expected'] = '%.1f $\pm$ %.1f' % (bg_predicted, bg_predicted_error)
        if blinded:
            entry['Observed'] = 'Blinded'
        else:
            entry['Observed'] = '%g' % observed
        writer.writerow(entry)
