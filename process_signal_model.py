from rootpy import ROOT as rt
from math import sqrt
import numpy as np
# signal_bin = 16
# signal_bin = 17

def get_pdf_uncertainty(fileh, signal_bin=16, debug=0):
    if debug:
        print fileh.cutflow.GetBinContent(signal_bin) , fileh.cutflow.GetBinContent(1)
        print fileh.cutflow__PdfUp.GetBinContent(signal_bin) , fileh.cutflow__PdfUp.GetBinContent(1)
        print fileh.cutflow__PdfDn.GetBinContent(signal_bin) , fileh.cutflow__PdfDn.GetBinContent(1)

    accept = fileh.cutflow.GetBinContent(signal_bin) / fileh.cutflow.GetBinContent(1)
    accept_up = fileh.cutflow__PdfUp.GetBinContent(signal_bin) / fileh.cutflow__PdfUp.GetBinContent(1)
    accept_dn = fileh.cutflow__PdfDn.GetBinContent(signal_bin) / fileh.cutflow__PdfDn.GetBinContent(1)
    if accept != 0:
        shift_up = accept_up - accept
        shift_dn = accept_dn - accept
    else:
        shift_up = 0.0
        shift_dn = 0.0
    if debug:
        print 'accept    :' , accept
        print 'accept_up :' , accept_up
        print 'accept_dn :' , accept_dn
    sorted_shift = [shift_up, shift_dn]
    sorted_shift.sort(reverse=True) # Sort from high to low
    return (accept, sorted_shift[0], sorted_shift[1])

def get_trigger_uncertainty(fileh, signal_bin=16, debug=0):
    accept = fileh.cutflow.GetBinContent(signal_bin) / fileh.cutflow.GetBinContent(1)
    accept_up = fileh.cutflow__TriggerUp.GetBinContent(signal_bin) / fileh.cutflow.GetBinContent(1)
    shift_up = accept_up - accept
    return (accept, shift_up, 0)

def get_mcstat_uncertainty(fileh, signal_bin=16, debug=0):
    accept = fileh.cutflow.GetBinContent(signal_bin) / fileh.cutflow.GetBinContent(1)
    accept_error = sqrt( accept * (1-accept) / fileh.cutflow.GetBinContent(1) )
    return (accept, accept_error, -accept_error)

def get_modeling_uncertainty(fileh, signal_bin=16, debug=0):
    accept = fileh.cutflow.GetBinContent(signal_bin) / fileh.cutflow.GetBinContent(1)
    accept_up = fileh.cutflow__ModelingUp.GetBinContent(signal_bin) / fileh.cutflow__ModelingUp.GetBinContent(1)
    shift_up = abs(accept_up - accept)
    return (accept, shift_up, 0)

def get_modeling_toy_uncertainty(fileh, signal_bin=16, debug=0):
    accept = fileh.cutflow.GetBinContent(signal_bin) / fileh.cutflow.GetBinContent(1)
    denominator = fileh.sys_acc_den_modeling.GetBinContent(1)
    numerators = []
    for i in range(100):
        numerator = fileh.sys_acc_num_modeling.GetBinContent(i+1)
        numerators.append(numerator)
    numerators_arr = np.array(numerators)
    mean = np.mean(numerators_arr)
    std  = np.std(numerators_arr)
    shift_up = accept * (std/mean)
    return (accept, shift_up, 0)

def get_modeling_shifts(fileh, signal_bin=16, debug=0):
    cutname        = fileh.cutname.GetTitle()
    sys_ipXyShift  = fileh.sys_IpXyShift.GetVal()
    sys_3dSigShift = fileh.sys_3dSigShift.GetVal()
    return (cutname, sys_ipXyShift, sys_3dSigShift)

def get_jec_uncertainty(fileh, signal_bin=16, debug=0):
    accept = fileh.cutflow.GetBinContent(signal_bin) / fileh.cutflow.GetBinContent(1)
    accept_up = fileh.cutflow__PtUp.GetBinContent(signal_bin) / fileh.cutflow__PtUp.GetBinContent(1)
    accept_dn = fileh.cutflow__PtDn.GetBinContent(signal_bin) / fileh.cutflow__PtDn.GetBinContent(1)
    if accept != 0:
        shift_up = accept_up - accept
        shift_dn = accept_dn - accept
    else:
        shift_up = 0.0
        shift_dn = 0.0
    if debug:
        print 'accept    :' , accept
        print 'accept_up :' , accept_up
        print 'accept_dn :' , accept_dn
    sorted_shift = [shift_up, shift_dn]
    sorted_shift.sort(reverse=True) # Sort from high to low
    return (accept, sorted_shift[0], sorted_shift[1])

def get_met_uncertainty(fileh, signal_bin=16, debug=0):
    accept = fileh.cutflow.GetBinContent(signal_bin) / fileh.cutflow.GetBinContent(1)
    accept_up = fileh.cutflow__MetUp.GetBinContent(signal_bin) / fileh.cutflow__MetUp.GetBinContent(1)
    accept_dn = fileh.cutflow__MetDn.GetBinContent(signal_bin) / fileh.cutflow__MetDn.GetBinContent(1)
    if accept != 0:
        shift_up = accept_up - accept
        shift_dn = accept_dn - accept
    else:
        shift_up = 0.0
        shift_dn = 0.0
    if debug:
        print 'accept    :' , accept
        print 'accept_up :' , accept_up
        print 'accept_dn :' , accept_dn
    sorted_shift = [shift_up, shift_dn]
    sorted_shift.sort(reverse=True) # Sort from high to low
    return (accept, sorted_shift[0], sorted_shift[1])

def get_pileup_uncertainty(fileh, signal_bin=16, debug=0):
    accept = fileh.cutflow.GetBinContent(signal_bin) / fileh.cutflow.GetBinContent(1)
    accept_up = fileh.cutflow__PileupUp.GetBinContent(signal_bin) / fileh.cutflow__PileupUp.GetBinContent(1)
    accept_dn = fileh.cutflow__PileupDn.GetBinContent(signal_bin) / fileh.cutflow__PileupDn.GetBinContent(1)
    if accept != 0:
        shift_up = accept_up - accept
        shift_dn = accept_dn - accept
    else:
        shift_up = 0.0
        shift_dn = 0.0
    if debug:
        print 'accept    :' , accept
        print 'accept_up :' , accept_up
        print 'accept_dn :' , accept_dn
    sorted_shift = [shift_up, shift_dn]
    sorted_shift.sort(reverse=True) # Sort from high to low
    return (accept, sorted_shift[0], sorted_shift[1])

def calculate_modeling_uncertainty(fileh, signal_bin=16, debug=0):
    accept = fileh.cutflow.GetBinContent(signal_bin) / fileh.cutflow.GetBinContent(1)
    num_hist = fileh.sys_acc_num_modeling
    den_hist = fileh.sys_acc_den_modeling
    hist = rt.Hist(200, -10., 10.)
    arr = np.array([])
    for i in range(100):
        accept_modelingUp = num_hist.GetBinContent(i+1) / den_hist.GetBinContent(i+1)
        print  accept_modelingUp / accept
        arr = np.append(arr, [accept_modelingUp])
        # hist.Fill(accept_ratio)
    return arr
    # return (accept, shift_up, 0)

def get_cutflow(fileh, signal_bin=16, debug=0):
    hist = fileh.cutflow.clone()
    nbins = hist.GetNbinsX()
    axis = hist.GetXaxis()
    cutflow = []
    for i in xrange(1, nbins+1):
        label = axis.GetBinLabel(i).rstrip()
        nEvents = hist.GetBinContent(i)
        cutflow.append((label, nEvents))
    return cutflow

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('-debug', type=int, default=0)
    parser.add_argument('signal_filename', type=str)
    args = parser.parse_args()
    signal_filename = args.signal_filename
    print get_pdf_uncertainty(signal_filename, args.debug)
    # print shift_up, shift_dn


