import csv
import os
from collections import OrderedDict
from rootpy import ROOT as rt
from rootpy.io import root_open
from rootpy.plotting import Hist, Hist2D, Hist3D, HistStack, Legend, Canvas, Graph, Pad
from rootpy.plotting.utils import draw
import tdrstyle
tdrstyle.setTDRStyle()
rt.gStyle.SetPalette(1)
batch = 1 # SWITCH
if batch:
    # Turn off graphics
    rt.gROOT.SetBatch(True)
blinded = 1 # SWITCH
from rootutils import saveAllCanvases, updateAllCanvases, deleteAllCanvases
from math import sin
import colorsys

# limitfile = 'emjet_limits_20180131.csv'
# limitfile = 'emjet_limits_allcuts_20180205.csv'
# limitfile2 = 'emjet_limits_20180125.csv'
# limitfile2 = 'emjet_limits_20171220.csv'
# limitfile2 = 'emjet_limits_20180118.csv'
# limitfile = 'emjet_limits_allcuts_20180214.csv'
limitfile = os.environ['LIMITFILE']

# Read into list of OrderedDict
with open(limitfile) as csvfile:
    reader = csv.DictReader(csvfile)
    keys = reader.fieldnames
    r = csv.reader(csvfile)
    signal_list = [OrderedDict(zip(keys, row)) for row in r]

########################################################################
# Do something
########################################################################
# Define list of parameters
mass_X_d_list = list( set([float(s['mass_X_d']) for s in signal_list]) )
mass_X_d_list.sort()
mass_pi_d_list = list( set([float(s['mass_pi_d']) for s in signal_list]) )
mass_pi_d_list.sort()
tau_pi_d_list = list( set([float(s['tau_pi_d']) for s in signal_list]) )
tau_pi_d_list.sort()
# mass_X_d_list = [1000]
# mass_pi_d_list = [1, 10]

# for mass_X_d in mass_X_d_list:
#     for mass_pi_d in mass_pi_d_list:
#         for tau_pi_d in tau_pi_d_list:
best_signal_list = []
processed_signal_names = []
for i, s in enumerate(signal_list):
    if s['name'] not in processed_signal_names:
        signal_list_filtered = [t for t in signal_list if t['name']==s['name']]
        signal_list_sorted = sorted(signal_list_filtered, key=lambda s: float(s['sigma0']))
        # if len(signal_list_sorted) > 1:
        #     print signal_list_sorted
        best_limit = signal_list_sorted[0]
        best_signal_list.append(best_limit)
        processed_signal_names.append(s['name'])

limitfile_best = os.environ['LIMITFILE_BEST']
with open(limitfile_best, 'w') as ofile:
    writer = csv.DictWriter(ofile, fieldnames=best_signal_list[0].keys())
    writer.writeheader()
    for s in best_signal_list:
        writer.writerow(s)

# Write cut file
# best_signal_list_cutonly = []
# for s in best_signal_list:
#     s_cutonly = [ (k, s[k]) for k in ['name', 'cutname'] ]
#     s_cutonly = OrderedDict(s_cutonly)
#     best_signal_list_cutonly.append(s_cutonly)
cutdict = OrderedDict()
cutmap = os.environ['CUTMAP']
with open(cutmap, 'w') as ofile:
    for s in best_signal_list:
        ofile.write('%s, %s\n' % (s['name'], s['cutname']))
        cutdict[s['name']] = s['cutname']

# Filter master signal file based on best limits
master_signal_list_filtered = []
sigfile = os.environ['SIGFILE']
with open(sigfile) as csvfile:
    reader = csv.DictReader(csvfile)
    keys = reader.fieldnames
    r = csv.reader(csvfile)
    master_signal_list = [OrderedDict(zip(keys, row)) for row in r]
for m in master_signal_list:
    name    = m['name']
    cutname = m['cutname']
    if name in cutdict.keys():
        if cutdict[name] == cutname:
            master_signal_list_filtered.append(m)
    else:
        print 'Could not find limit for signal: %s' % (name)
        pass # Skip signals with no limits
output_master_signals_file = os.environ['SIGFILE_BEST']
with open(output_master_signals_file, 'w') as csvfile:
    writer = csv.DictWriter(csvfile, quotechar='"', fieldnames=master_signal_list_filtered[0].keys())
    writer.writeheader()
    for m in master_signal_list_filtered:
        writer.writerow(m)


